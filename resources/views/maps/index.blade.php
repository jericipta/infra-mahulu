@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
    <ul class="breadcrumb breadcrumb-top">
        <li> Users List</li>
        <li><a href="/users">Users</a></li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="block full row">
            <div class="block-title">
                <div class="block-options pull-right">
                    <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
                </div>
                <h2><strong>Statistik</strong> Pengeluaran</h2>
            </div>
            <div class="col-md-12">
                <div style="width: 700px; height: 500px;">
                    {!! Mapper::render() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
@endsection