@extends('infra.layouts.dashboard')

@section('template_title')
	{{ $user->name }}'s Profile
@endsection

@section('template_fastload_css')

	#map-canvas{
		min-height: 300px;
		height: 100%;
		width: 100%;
	}

@endsection

@section('header')
	<small>
		{{ trans('profile.showProfileTitle',['username' => $user->name]) }}
	</small>
@endsection

@section('breadcrumbs')

	<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
		<a itemprop="item" href="{{url('/')}}">
			<span itemprop="name">
				{{ trans('titles.app') }}
			</span>
		</a>
		<i class="material-icons">chevron_right</i>
		<meta itemprop="position" content="1" />
	</li>

	<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="active">
		<a itemprop="item" href="{{ url('/profile/'.Auth::user()->name) }}" class="hidden">
			<span itemprop="name">
				{{ trans('titles.profile') }}
			</span>
		</a>
		{{ trans('titles.profile') }}
		<meta itemprop="position" content="2" />
	</li>

@endsection

@section('content')

	{{--@include('cards.user-profile-card')--}}
	<div class="row">
		<div class="col-lg-12">
			<div class="block">
				<div class="block-title">
					<h2><i class="fa fa-file-o"></i> User Profile</h2>
				</div>
				<div class="block-section text-center">
					<a href="javascript:void(0)">
						<img src="{{asset('ProUI/img/placeholders/avatars/avatar4@2x.jpg')}}" alt="avatar" class="img-circle">
					</a>
					<h3>
						<strong>{{ $user->first_name." ".$user->last_name }}</strong><br><small></small>
					</h3>
				</div>
				<table class="table table-borderless table-striped table-vcenter">
					<tbody>
					<tr>
						<td class="text-right" style="width: 50%;"><strong>NIP</strong></td>
						<td>{{ $user->profile->nip }}</td>
					</tr>
					<tr>
						<td class="text-right" style="width: 50%;"><strong>Username</strong></td>
						<td>{{ $user->name }}</td>
					</tr>
					<tr>
						<td class="text-right" style="width: 50%;"><strong>No. Telp</strong></td>
						<td>{{ $user->profile->no_telepon }}</td>
					</tr>
					<tr>
						<td class="text-right"><strong>Birthdate</strong></td>
						<td>{{ $user->profile->tanggal_lahir }}</td>
					</tr>
					<tr>
						<td class="text-right"><strong>Bagian</strong></td>
						<td>{{$user->profile->bagian}}</td>
					</tr>
					<tr>
						<td class="text-right"><strong>Jabatan</strong></td>
						<td>{{$user->profile->jabatan}}</td>
					</tr>
					<tr>
						<td class="text-right"><strong>Language</strong></td>
						<td>English (UK)</td>
					</tr>
					<tr>
						<td class="text-right"><strong>Registrations</strong></td>
						<td><span class="label label-primary">Newsletter</span></td>
					</tr>
					<tr>
						<td class="text-right"><strong>Status</strong></td>
						<td><span class="label label-success"><i class="fa fa-check"></i> Active</span></td>
					</tr>
					</tbody>
				</table>
				<!-- END Customer Info -->
			</div>
			</div>
		</div>
	</div>

@endsection

@section('footer_scripts')

	@include('scripts.google-maps-geocode-and-map')

@endsection
