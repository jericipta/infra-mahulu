@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
    <ul class="breadcrumb breadcrumb-top">
        <li> Role List</li>
        <li><a href="/users">Roles</a></li>
    </ul>
@endsection

@section('content')
    <div class="row">
       
        <div class="col-md-5">
            <div class="block">
                {!! Form::open(array('action' => 'RoleController@store', 'method' => 'POST', 'role' => 'form', 'class' => 'form-bordered')) !!}
                <div class="form-group">
                    <label for="role_name">Nama Role</label>
                    <input type="text" id="role_name" name="role_name" class="form-control" placeholder="Nama Role">
                </div>
                <div class="form-group">
                    <label for="role-list">Slug Role</label>
                    <input type="text" id="role-list" name="role-list" class="form-control" placeholder="Slug Role">
                </div>
                <div class="form-group">
                    <label for="descript">Description</label>
                    <textarea id="example-textarea-input" name="descript" rows="9" class="form-control" placeholder=""></textarea>
                </div>
                <div class="form-group">
                    <label for="level">Level</label>
                    <select id="level" name="level">
                        <option value="" disabled>--Pilih Level--</option>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="5">5</option>
                    </select>
                </div>
                <div class="form-group form-actions">
                    <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Save</button>
                    <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-md-7 pull-right">
            <div class="block">
                <div class="table-responsive">
                    <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th style="font-size: 14px;">ID</th>
                            <th style="font-size: 14px;">Nama</th>
                            <th style="font-size: 14px;">Slug</th>
                            <th style="font-size: 14px;">Description</th>
                            <th style="font-size: 14px;">Level</th>
                            <th style="font-size: 14px;">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($role as $val)
                            <tr>
                                <td style="font-size: 12px;"><span class="idRole">{{$val->id}}</span></td>
                                <td style="font-size: 12px;">{{$val->name}}</td>
                                <td style="font-size: 12px;">{{$val->slug}}</td>
                                <td style="font-size: 12px;">{{$val->description}}</td>
                                <td style="font-size: 12px;">{{$val->level}}</td>
                                <td class="text-center" style="font-size: 12px;">
                                    <a href="#" data-toggle="modal" data-target="#myModal" class="btnEdit">
                                        <i class="fa fa-pencil"></i></a> | 
                                    <a href="#" data-toggle="modal" data-target="#modal-delete" class="modal-delete" data-id="{{$val->id}}">
                                        <i class="fa fa-trash"></i></a> 
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    {!! Form::open(array('action' => 'RoleController@update', 'method' => 'POST', 'role' => 'form')) !!}
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Update Role</h4>
            </div>
            <div class="modal-body">
                <label class="col-md-3 control-label" for="example-text-input">Nama Role</label>
                <input type="text" id="editRoleName" name="role_name" class="form-control" placeholder="Nama Role">
                <input type="hidden" id="idRoleedit" name="idRoleedit">
                <span class="help-block"></span>
                <label class="col-md-3 control-label" for="example-text-input">Slug Role</label>
                <input type="text" id="slug-list-edit" name="slug-list-edit" class="form-control" placeholder="Slug Role">
                <label class="col-md-3 control-label" for="example-textarea-input">description</label>
                <textarea id="edit-desc" name="descript" rows="9" class="form-control" placeholder=""></textarea>
                <label class="col-md-3 control-label" for="example-text-input">Level</label>
                <select id="edit-level" name="level">
                    <option value="" disabled>--Pilih Level--</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="5">5</option>
                </select>
            </div>
            <div class="modal-footer">
                {!! Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
                {!! Form::button('<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Save', array('class' => 'bbtn btn-sm btn-primary pull-right btn-flat', 'type' => 'submit', 'id' => 'confirm' )) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<div id="modal-delete" class="modal fade" role="dialog">
    {!! Form::open(array('action' => 'RoleController@delete', 'method' => 'POST', 'role' => 'form')) !!}
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirm</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id2" name="id">
                <span class="help-block"></span>
            </div>
            <div class="modal-footer">
                {!! Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
                {!! Form::button('<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Confirm Delete', array('class' => 'btn btn-danger pull-right btn-flat', 'type' => 'submit', 'id' => 'confirm' )) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection
@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $("body").on("click",".btnEdit",function(){
            var id = $(this).parent("td").prev("td").prev("td").prev("td").prev("td").prev("td").text();
            var nama = $(this).parent("td").prev("td").prev("td").prev("td").prev("td").text();
            var slug = $(this).parent("td").prev("td").prev("td").prev("td").text();
            var desc = $(this).parent("td").prev("td").prev("td").text();
            var lvl = $(this).parent("td").prev("td").text();

            $('#editRoleName').val(nama);
            $('#slug-list-edit').val(slug);
            $('#edit-desc').val(desc);
            $('#edit-level').val(lvl);
            $('#idRoleedit').val(id);
        });
        $('#bnt-update').click(function () {
//            $.ajax({
//                url : "update/role",
//                type : "POST",
//                data : {
//                    'id' : id,
//                    'nama' : nama,
//                    'slug' : slug,
//                    'desc' : desc,
//                    'lvl' : lvl
//                },
//                success: function(response) {
                    alert("Role Updated");
//                    window.reload();
//                }
//            });
////            alert('edan');
        })

        $("body").on("click",".modal-delete",function(){
            $('#id2').val($(this).attr('data-id'));
        });

    </script>
@endsection