@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
    <ul class="breadcrumb breadcrumb-top">
        <li>Statistik</li>
        <li><a href="/chart">Chart</a></li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="block full row">
        <div class="block-title">
            <div class="block-options pull-right">
                <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
            </div>
            <h2><strong>Statistik</strong> Pengeluaran</h2>
        </div>
            <div class="col-md-7">
                <select>
                    <option>Daerah/Kota</option>
                </select>
                <center>Jumlah Anggaran 2016</center>
                <div id="canvas-holder" style="width:100%;height: 100%">
                    <canvas id="myChart" width="300" height="150"></canvas>
                </div>
            </div>
            <div class="col-md-5 pull-right">
                    <select>
                        <option>2016</option>
                    </select>
                    <center>Jumlah Anggaran 2016</center>
                    <div id="canvas-holder" style="width:100%;height: 100%">
                        <canvas id="chart-area"></canvas>
                    </div>
            </div>
    </div>
    </div>
@endsection
@section('footer_scripts')
    <script src="{{asset('js/chart/Chart.bundle.js')}}"></script>
    <script src="{{asset('js/chart/utils.js')}}"></script>
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        var ctx = document.getElementById("myChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
    <script>
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                    ],
                    backgroundColor: [
                        window.chartColors.orange,
                        window.chartColors.blue,
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    "APBD",
                    "APBN"
                ]
            },
            options: {
                responsive: true
            }
        };

        window.onload = function() {
            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myPie = new Chart(ctx, config);
        };
    </script>
@endsection