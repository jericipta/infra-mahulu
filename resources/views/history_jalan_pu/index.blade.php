@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
    <ul class="breadcrumb breadcrumb-top">
        <li> Data Jalan</li>
        <li><a href="/users">Jalan PU</a></li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="block full">
                <div class="table-responsive">
                <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center" style="font-size: 12px;">ID</th>
                        <th class="text-center" style="font-size: 12px;">Dinas</th>
                        <th class="text-center" style="font-size: 12px;">Tanggal Proses</th>
                        <th class="text-center" style="font-size: 12px;">Status</th>
                        <th class="text-center" style="font-size: 12px;">Lokasi File</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $data_pu)
                        <tr>
                            <td style="font-size: 12px;">{{$data_pu->id}}</td>
                            <td style="font-size: 12px;">{{$data_pu->dinas}}</td>
                            <td style="font-size: 12px;">{{$data_pu->status}}</td>
                            <td style="font-size: 12px;">{{$data_pu->lokasi_file}}</td>
                            <td class="text-center" style="font-size: 12px;">
                                <a href="#" data-toggle="modal" data-target="#myModal" class="btnEdit">
                                    Lihat</a> 
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
<script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
<script>$(function(){ TablesDatatables.init(); });</script>
@endsection