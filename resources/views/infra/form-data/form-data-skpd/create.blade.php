@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Insert</li>
    <li><a href="">Jalan Daerah / Kota</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
     {!! Form::open(array('action' => 'FormDataSKPDController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-bordered')) !!}
        <div class="form-group">
            {!! Form::Label('id-skpd', 'Nama SKPD') !!}
            {!! Form::select('id-skpd', $skpd, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Skpd']) !!}
        </div>
        <div class="form-group">
            {!! Form::Label('nama-skpd', 'Nama Form SKPD') !!}
            <input type="text" id="nama-skpd" name="nama-skpd" class="form-control" placeholder="Nama Form SKPD">
        </div>
        <div class="form-group">
            {!! Form::label('File Form') !!}
            {!! Form::file('file-form', null) !!}
        </div>
        <div class="form-group">
            {!! Form::Label('table-name', 'Table Form') !!}
            <input type="text" id="table-name" name="table-name" class="form-control" placeholder="Table Form">
        </div>
        <div class="form-group">
            {!! Form::label('Keterangan') !!}
            {!! Form::textarea('keterangan', '', ['class' => 'form-control']) !!}
        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="{{ url('/form-data-skpd')}}" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    {!! Form::close() !!}
</div>
@endsection
