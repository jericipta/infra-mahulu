@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li><a href="">Jalan Daerah / Kota</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="{{ URL::to( asset('form-data-jalan-kota/create'))}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="" data-original-title="Tambah Data Jalan"><i class="fa fa-plus-square"></i></a>
        </div>
        <h2><strong>Form Data - Jalan Daerah Kota</strong></h2>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Id</th>
                <th class="text-center" style="font-size: 12px;">Dinas</th>
                <th style="font-size: 12px;">Tanggal Proses</th>
                <th style="font-size: 12px;">Status</th>
                <th style="font-size: 12px;">Lokasi File</th>
                <th class="text-center" style="font-size: 12px;">Hasil</th>
            </tr>
            </thead>
            <tbody>
            @foreach($formdata as $val)
                <tr>
                    <td class="text-center" style="font-size: 12px;">{{$val->id}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$val->dinas}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$val->tanggal_proses}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$val->status}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$val->lokasi}}</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ URL::to('form-data-jalan-kota/' . $val->id . '/edit') }}" class="btn btn-primary" data-toggle="tooltip" title="Edit Selected">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="/form-data-jalan-kota/{{$val->name}}" class="btn btn-primary" data-toggle="tooltip" title="View Selected"><i class="fa fa-search"></i></a>
                            <a href="#modal-delete"  class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" 
                            class="modal-delete2" id="modal-delete2" data-id="{{$val->id}}"><i class="fa fa-trash"></i></a> 
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div id="modal-delete" class="modal fade" role="dialog">
    {!! Form::open(array('action' => 'FormDataJDaerahKotaController@delete', 'method' => 'POST', 'role' => 'form')) !!}
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirm</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id2" name="id">
                <span class="help-block"></span>
            </div>
            <div class="modal-footer">
                {!! Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
                {!! Form::button('<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Confirm Delete', array('class' => 'btn btn-danger pull-right btn-flat', 'type' => 'submit', 'id' => 'confirm' )) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer_scripts')
<script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
<script>$(function(){ TablesDatatables.init(); });</script>
<script>
    $(document).on("click","#modal-delete2",function(){
        $('#id2').val($(this).attr('data-id'));
    });
</script>
@endsection