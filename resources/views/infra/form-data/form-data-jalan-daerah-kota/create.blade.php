@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Insert</li>
    <li><a href="">Jalan Daerah / Kota</a></li>
</ul>
@endsection

@section('content')
 <div class="block full">
     {!! Form::open(array('action' => 'FormDataJDaerahKotaController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-bordered')) !!}
        <div class="form-group">
            {!! Form::Label('id-dinas', 'Nama Dinas') !!}
            {!! Form::select('id-dinas', $dinas, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Dinas']) !!}
        </div>
        <div class="form-group">
            {!! Form::Label('tgl-proses', 'Tanggal Proses') !!}
            <input type="text" id="tgl-proses" name="tgl-proses" class="form-control input-datepicker" data-date-format="dd-MM-yy" placeholder="dd-MM-yy">
        </div>
        <div class="form-group">
            {!! Form::label('File Jalan') !!}
            {!! Form::file('jalan', null) !!}
        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="{{ url('/form-data-jalan-kota')}}" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    {!! Form::close() !!}
</div>
@endsection
