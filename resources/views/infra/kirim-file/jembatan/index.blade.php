@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<div class="content-header">
    <div class="header-section" style="padding: 20px 10px;">
        <h1 class="text-center">
            INFRASTRUKTUR JEMBATAN
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li>Tables</li>
    <li><a href="">Datatables</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="block-title">
         <a class="btn btn-sm btn-success" href="{{ URL::to( asset('TEMPLATE/template_jalan.xlsx'))}}" target="_blank">Unduh Format Data</a>
        {{--<button class="btn btn-success">Unduh Format Data</button>--}}
        <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#modal-default">Kirim Data</button>
    </div>

    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Nama Proyek</th>
                <th class="text-center" style="font-size: 12px;">Lokasi</th>
                <th style="font-size: 12px;">Panjang</th>
                <th style="font-size: 12px;">Sumaber Anggaran</th>
                <th style="font-size: 12px;">Tahun Anggaran</th>
                <th class="text-center" style="font-size: 12px;">Jumlah Anggaran</th>
                <th class="text-center" style="font-size: 12px;">Kord X</th>
                <th class="text-center" style="font-size: 12px;">Kord Y</th>
                <th class="text-center" style="font-size: 12px;">Tanggal Kirim</th>
            </tr>
            </thead>
            <tbody>
            @foreach($proyek as $proyeks)
                <tr>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->nama_proyek}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->lokasi}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->panjang}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->sumber_anggaran}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->tahun_angaran}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->jumlah_anggaran}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->kord_x1}}.{{$proyeks->kord_x2}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->kord_y1}}.{{$proyeks->kord_y2}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$proyeks->created_at}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Default Modal</h4>
            </div>
            <form action="savedb" method="POST" enctype="multipart/form-data">
                <div class="modal-body">
                    {{ csrf_field() }}

                    <div class="box-header with-border">
                        <h3 class="box-title">Import Proyek</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputFile">File input</label>
                            <input id="exampleInputFile" type="file" name="excelFile">
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Kirim Data</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
<script>$(function(){ TablesDatatables.init(); });</script>
@endsection