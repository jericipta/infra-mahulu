<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
    <meta name="description" content="">
    <meta name="author" content="Jeremy Kenedy">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="{{asset('css/jquery-ui.css')}}">
    {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    {{-- Custom App Styles --}}
    <link rel="stylesheet" href="{{asset('ProUI/css/bootstrap.min.css')}}">

    <link rel="stylesheet" href="{{asset('ProUI/css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('ProUI/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('ProUI/css/themes.css')}}">

    <script src="{{asset('ProUI/js/vendor/modernizr.min.js')}}"></script>

    {{-- Fonts --}}
    {!! HTML::style('https://fonts.googleapis.com/css?family=Roboto:300italic,400italic,400,100,300,600,700', array('type' => 'text/css', 'rel' => 'stylesheet')) !!}
    {!! HTML::style(asset('https://fonts.googleapis.com/icon?family=Material+Icons'), array('type' => 'text/css', 'rel' => 'stylesheet')) !!}

    <style type="text/css">
        @yield('template_fastload_css')

            @if (Auth::User() && (Auth::User()->profile) && (Auth::User()->profile->avatar_status == 0))
                .user-avatar-nav {
            background: url({{ Gravatar::get(Auth::user()->email) }}) 50% 50% no-repeat;
            background-size: auto 100%;
        }
        @endif

    </style>

    {{-- Scripts --}}
    <script>
        window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
    </script>
</head>
<body>

<div id="page-wrapper">
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        @include('infra.layouts.nav-side')
        <div id="main-container">
            @include('infra.layouts.header')
            <div id="page-content">
                @yield('header-breadcrumbs')
                @yield('content')
            </div>
        @include('infra.layouts.footer')
        </div>
    </div>
</div>


<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="{{asset('ProUI/js/vendor/jquery.min.js')}}"></script>
<script src="{{asset('ProUI/js/vendor/bootstrap.min.js')}}"></script>
<script src="{{asset('ProUI/js/plugins.js')}}"></script>
<script src="{{asset('ProUI/js/app.js')}}"></script>

<script src="{{asset('ProUI/js/pages/index.js')}}"></script>

@yield('footer_scripts')
</body>
</html>