@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<div class="content-header">
    <div class="header-section" style="padding: 20px 10px;">
        <h1 class="text-center">
            DATA INFRASTRUKTUR (SARANA DAN PRASARANA) KAMPUNG
        </h1>
        <h1 class="text-center">
            DI KABUPATEN MAHAKAM HULU
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li> Data Infrastruktur</li>
    <li><a href="/users">Air Bersih</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
          <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Lokasi</th>
                <th class="text-center" style="font-size: 12px;">Bidang Urusan</th>
                <th class="text-center" style="font-size: 12px;">Jumlah Penduduk / Jumlah KK</th>
                <th class="text-center" style="font-size: 12px;">SAB</th>
                <th class="text-center" style="font-size: 12px;">KapasitasLT/DK</th>
                <th class="text-center" style="font-size: 12px;">Sungai</th>
                <th class="text-center" style="font-size: 12px;">Keterangan</th>
            </tr>
          </thead>
          <tbody>

                @foreach ($data as $v)
                <tr>

                    <td class="text-left" style="font-size: 12px;">{{$v[0]}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$v[1]}}</td>
                    <td class="text-center" style="font-size: 12px;">{{$v[2]}}</td>
                    <td class="text-center" style="font-size: 12px;"><i class="{{ $v[3] === true ? "fa fa-check" : "" }}"></i></td>
                    <td class="text-center" style="font-size: 12px;"><i class="{{ $v[4] === true ? "fa fa-check" : "" }}"></i></td>
                    <td class="text-center" style="font-size: 12px;"><i class="{{ $v[5] === true  ? "fa fa-check" : "" }}"></i></td>
                    <td class="text-center" style="font-size: 12px;">{{$v[6]}}</td>
                </tr>
                @endforeach
          </tbody>
        </table>
    </div>
</div>
    
@endsection
@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
@endsection