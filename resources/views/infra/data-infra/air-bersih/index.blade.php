@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<div class="content-header">
    <div class="header-section" style="padding: 20px 10px;">
        <h1 class="text-center">
            DATA INFRASTRUKTUR (SARANA DAN PRASARANA) KAMPUNG
        </h1>
        <h1 class="text-center">
            DI KABUPATEN MAHAKAM HULU
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li> Data Infrastruktur</li>
    <li><a href="/users">Air Bersih</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a class="btn btn-success" id="update-btn" href="#" style="display: none;">Update</a>
            <a class="btn btn-success" id="save-btn" href="#" style="display: none;">Save</a>
        </div>
        <select class="form-control" style="width: 150px" id="kecamatan">
            <option selected disabled>Kecamatan</option>
            @foreach ($kecamatan as $kec)
                <option value="{{$kec->id}}">{{$kec->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
          <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Lokasi</th>
                <th class="text-center" style="font-size: 12px;">Bidang Urusan</th>
                <th class="text-center" style="font-size: 12px;">Jumlah Penduduk</th>
                <th class="text-center" style="font-size: 12px;">SAB</th>
                <th class="text-center" style="font-size: 12px;">KapasitasLT/DK</th>
                <th class="text-center" style="font-size: 12px;">Sungai</th>
                <th class="text-center" style="font-size: 12px;">Keterangan</th>
            </tr>
          </thead>
          <tbody id="result-table">

          </tbody>
        </table>
    </div>
</div>
    
@endsection
@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $('#kecamatan').change(function () {
            $('#result-table').html("");
            getValueKecamatan($('#kecamatan').val());
            $('#update-btn').show();
            $('#save-btn').hide();            
        })
        $('#update-btn').click(function () {
            $('.table-responsive td').each(function() {
                var cellText = $(this).html();
//                alert($(this).attr("class"));
                if ($(this).attr("class")==="text-center dinamis"){
//                    alert('ok');
                    if (cellText === '<i class="fa fa-check"></i>'){
                        $(this).html("<td class='text-center dinamis'><input type='checkbox' value='1' checked></td>");
                    } else {
                        $(this).html("<td class='text-center dinamis'><input type='checkbox' value='0'></td>");
                    }
                } else if($(this).attr("class")==="text-center dinamis-text"){
                    if (cellText === ""){
                        cellText = " ";
                    }
                    $(this).html("<td class='text-center dinamis'><input type='text' value='"+cellText+"'></td>");
                }
            });
            $('#update-btn').hide();
            $('#save-btn').show();
        });
        $('#save-btn').click(function () {
            var obj = [];
            $('table tr').each(function(index){
                var desa = $(this).find('td:first').text();
                item = {};
                item[desa] = [];
                $(this).find('input').each(function () {
                    var val = $(this).val();
                    if($(this).attr('type') === "checkbox"){
                        if ($(this).is(":checked")){
                            val = "1";
                        }else{
                            val = "0";
                        }    
                    }
                    item[desa].push(val);
                });
                obj.push(item);
            });
            var json = JSON.stringify(obj);
            // console.log(json);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/infra-air-bersih/post',
                type: 'POST',
                data: {"data":json},
                success: function( response ){
                    $("#example-datatable tbody").empty();
                    getValueKecamatan($('#kecamatan').val());
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            })
            $('#update-btn').show();
            $('#save-btn').hide();
        });

        function getValueKecamatan(val) {
            $.ajax({
                url: "/get-desa-air",
                type: "GET",
                data: {
                    'id_kec': $('#kecamatan').val()
                },
                success: function (res) {
                    for (var i=0;i < res['data_desa'].length;i++){
                        var nama_desa = res['data_desa'][i]['nama'];
                        var jumlah_pen = ((typeof res['data_desa_air'][i][nama_desa]['jumlah_penduduk'] === 'undefined') ? " " : res['data_desa_air'][i][nama_desa]['jumlah_penduduk']);
                        var sab = res['data_desa_air'][i][nama_desa]['sab'];
                        var kap_lt_dk = ((typeof res['data_desa_air'][i][nama_desa]['kapasitas_lt_dk'] === 'undefined') ? " " : res['data_desa_air'][i][nama_desa]['kapasitas_lt_dk']);
                        var sungai = ((typeof res['data_desa_air'][i][nama_desa]['sungai'] === 'undefined') ? " " : res['data_desa_air'][i][nama_desa]['sungai']);
                        if (sab===1){
                            sab = '<i class="fa fa-check"></i>';
                        } else {
                            sab = '';
                        }

                        var nm_desa = res['data_desa_air'][i][nama_desa];
                        var keterangan = ((typeof nm_desa['keterangan'] === 'undefined') ? " " : nm_desa['keterangan']);
                        $('#result-table').append("<tr><td class='text-left dinamis-text-disabled'>"+nama_desa+"</td>" +
                            "<td class='text-center dinamis-text'>"+keterangan+"</td>" +
                            "<td class='text-center dinamis-text'>"+jumlah_pen+"</td>" +
                            "<td class='text-center dinamis'>"+sab+"</td>" +
                            "<td class='text-center dinamis-text'>"+kap_lt_dk+"</td>" +
                            "<td class='text-center dinamis-text'>"+sungai+"</td>"+
                            "<td class='text-center dinamis-text'>"+keterangan+"</td>" +
                            "</tr>");
                    }
//                    console.log(res['data_desa_listrik'][0]['']);
                    $('#update-btn').show();
                    $('#save-btn').hide();
                }
            });
        }
    </script>
@endsection