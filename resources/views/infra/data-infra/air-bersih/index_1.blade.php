@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<div class="content-header">
    <div class="header-section" style="padding: 20px 10px;">
        <h1 class="text-center">
            DATA INFRASTRUKTUR (SARANA DAN PRASARANA) KAMPUNG
        </h1>
        <h1 class="text-center">
            DI KABUPATEN MAHAKAM HULU
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li> Data Infrastruktur</li>
    <li><a href="/users">Air Bersih</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a class="btn btn-success" id="update-btn" href="#">Update</a>
        </div>
        <select class="form-control" style="width: 150px" id="kecamatan">
            <option selected disabled>Kecamatan</option>
            @foreach ($kecamatan as $kec)
                <option value="{{$kec->id}}">{{$kec->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
          <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Lokasi</th>
                <th class="text-center" style="font-size: 12px;">Bidang Urusan</th>
                <th class="text-center" style="font-size: 12px;">Jumlah Penduduk / Jumlah KK</th>
                <th class="text-center" style="font-size: 12px;">SAB</th>
                <th class="text-center" style="font-size: 12px;">KapasitasLT/DK</th>
                <th class="text-center" style="font-size: 12px;">Sungai</th>
                <th class="text-center" style="font-size: 12px;">Keterangan</th>
            </tr>
          </thead>
          <tbody id="result-table">

          </tbody>
        </table>
    </div>
</div>
    
@endsection
@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $('#kecamatan').change(function () {
            $('#result-table').html("");
            $.ajax({
                url: "/get-desa-air",
                type: "GET",
                data: {
                    'id_kec': $('#kecamatan').val()
                },
                success: function (res) {
                    for (var i=0;i < res['data_desa'].length;i++){
                        var nama_desa = res['data_desa'][i]['nama'];
                        var jumlah_pen = res['data_desa_air'][i][nama_desa]['jumlah_pen'];
                        var sab = res['data_desa_air'][i][nama_desa]['sab'];
                        var kap_lt_dk = res['data_desa_air'][i][nama_desa]['kapasitas_lt_dk'];
                        var sungai = res['data_desa_air'][i][nama_desa]['sungai'];
                        if (sab===1){
                            sab = '<i class="fa fa-check"></i>';
                        } else {
                            sab = '';
                        }
                        $('#result-table').append("<tr><td class='text-center'>"+res['data_desa'][i]['nama']+"</td>" +
                            "<td class='text-center dinamis-text'>"+res['data_desa_air'][i][nama_desa]['keterangan']+"</td>" +
                            "<td class='text-center dinamis'>"+jumlah_pen+"</td>" +
                            "<td class='text-center dinamis'>"+sab+"</td>" +
                            "<td class='text-center dinamis'>"+kap_lt_dk+"</td>" +
                            "<td class='text-center dinamis'>"+sungai+"</td>"+
                            "<td class='text-center dinamis-text'>"+res['data_desa_air'][i][nama_desa]['keterangan']+"</td>" +
                            "</tr>");
                    }
//                    console.log(res['data_desa_listrik'][0]['']);

                }
            });
        })
        $('#update-btn').click(function () {
            $('.dinamis').html("<td class='text-center dinamis'><input type='checkbox'></td>");
        })
    </script>
@endsection