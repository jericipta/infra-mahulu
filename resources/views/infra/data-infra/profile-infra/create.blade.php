@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection



@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Insert</li>
    <li><a href="">Infrastruktur</a></li>
</ul>
@endsection

@section('content')
<style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview{
      border: 1px solid black;
      padding: 10px;
    }
    #image_preview img{
      width: 200px;
      padding: 5px;
    }
  </style>
<div class="block full">
     {!! Form::open(array('action' => 'ProfileInfraController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-horizontal  form-bordered')) !!}
        <div class="form-group col-sm-12 left">
            {!! Form::Label('id-kategori', 'Infrastruktur', ['class' => 'col-md-2 control-label left']) !!}
            <div class="col-md-4">
                {!! Form::select('id-kategori', $kategori, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Infrastruktur']) !!}
            </div>
            {!! Form::Label('id-jenis', 'Jenis', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <select id="id-jenis" name="id-jenis" class="form-control" >
                   <option>Silahkan Pilih Kategori</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('nama', 'Nama', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama">
            </div>
            {!! Form::Label('alamat', 'Alamat', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Alamat">
            </div>
        </div>
         <div class="form-group col-sm-12">
            {!! Form::Label('id-kecamatan', 'Kecamatan', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::select('id-kecamatan', $kecamatan, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Kecamatan']) !!}
            </div>
            {!! Form::Label('id-desa', 'Desa', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <select id="id-desa" name="id-desa" class="form-control" >
                    <option>Silahkan Pilih Kecamatan</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('id-tahun', 'Tahun Anggaran', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::select('tahun', $tahun, null, ['class' => 'select-chosen']) !!}
            </div>
            {!! Form::Label('nilai', 'Nilai Anggaran', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="number" id="nilai" name="nilai" class="form-control" placeholder="Nilai Anggaran">
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('catatan', 'Catatan Kondisi', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="text" id="catatan" name="catatan" class="form-control" placeholder="Catatan Kondisi">
            </div>
            {!! Form::Label('tgl-proses', 'Tanggal Proses', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::text('tgl-proses', NULL, array('id' => 'datepicker', 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('longitude', 'longitude', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-1" style="padding-right: 0px;">
                <input type="number" id="kord_x1" name="kord_x1" class="form-control" placeholder="0">
            </div>
            <div class="col-md-3" style="padding-left: 0px;">
                <input type="number" id="kord_x2" name="kord_x2" class="form-control" placeholder="00">
            </div>
            {!! Form::Label('latitude', 'latitude', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-1" style="padding-right: 0px;">
                <input type="number" id="kord_y1" name="kord_y1" class="form-control" placeholder="0">
            </div>
            <div class="col-md-3" style="padding-left: 0px;">
                <input type="number" id="kord_y2" name="kord_y2" class="form-control" placeholder="00">
            </div>
        </div>
        <div class="form-group">
            {!! Form::Label('uploadFile', 'Foto', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <input type="file" id="uploadFile" name="uploadFile[]" multiple/>
                <div id="image_preview"></div>
            </div>
        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="{{ url('/infra-form-infrastruktur')}}" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer_scripts')

<script type="text/javascript">
    $("#uploadFile").change(function(){
        $('#image_preview').html("");
        var total_file=document.getElementById("uploadFile").files.length;
        for(var i=0;i<3;i++)
        {
            $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
        }
    });


    $("add").on("click",function(){
        $('#id2').val($(this).attr('data-id'));
    });
</script>
<script>
    $('#id-kategori').change(function(){
        $.get("{{ url('infra-master-jenis/jenis')}}", 
            { option: $(this).val() }, 
            function(data) {
                var model = $('#id-jenis');
                model.empty();

                $.each(data, function(index, element) {
                    model.append("<option value='"+ index +"'>" + element + "</option>");
                });
            });
    });

    $('#id-kecamatan').change(function(){
        $.get("{{ url('infra-master-desa/desa')}}", 
            { option: $(this).val() }, 
            function(data) {
                var model = $('#id-desa');
                model.empty();

                $.each(data, function(index, element) {
                    model.append("<option value='"+ index +"'>" + element + "</option>");
                });
            });
    });
</script>
<script>
    $(function() {
        $( "#datepicker" ).datepicker();
    });
</script>
@endsection