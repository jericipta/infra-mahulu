@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection



@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Insert</li>
    <li><a href="">Infrastruktur</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
     {!! Form::open(array('action' => 'ProfileInfraController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-horizontal  form-bordered')) !!}
        <div class="form-group col-sm-6">
            {!! Form::Label('id-skpd', 'Infrastruktur', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::select('id-skpd', $kategori, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Infrastruktur']) !!}
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('id-skpd', 'Jenis', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <select id="itemselect" name="itemselect" class="form-control" >
                   <option>Silahkan Pilih Kategori</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('nama', 'Nama', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama">
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('alamat', 'Alamat', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Alamat">
            </div>
        </div>
         <div class="form-group col-sm-6">
            {!! Form::Label('id-kecamatan', 'Nama Kecamatan', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::select('id-kecamatan', $kecamatan, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Kecamatan']) !!}
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('id-desa', 'Desa', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <select id="itemdesa" name="itemdesa" class="form-control" >
                    <option>Silahkan Pilih Kecamatan</option>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('alamat', 'Tahun Anggaran', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                {!! Form::select('id-tahun', $tahun, null, ['class' => 'select-chosen']) !!}
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('alamat', 'Nilai Anggaran', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Nilai Anggaran">
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('alamat', 'Catatan Kondisi', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <input type="text" id="alamat" name="alamat" class="form-control" placeholder="Catatan Kondisi">
            </div>
        </div>
        <div class="form-group col-sm-6">
            {!! Form::Label('tgl-proses', 'Tanggal Proses', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <input type="text" id="tgl-proses" name="tgl-proses" class="form-control input-datepicker" data-date-format="dd-MM-yy" placeholder="dd-MM-yy">
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('tgl-proses', 'Tanggal Proses', ['class' => 'col-md-3 control-label']) !!}
            <div class="col-md-9">
                <input type="file" id="uploadFile" name="uploadFile[]" multiple/>
                <div id="image_preview"></div>
            </div>
        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="{{ url('/infra-form-infrastruktur')}}" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    {!! Form::close() !!}
</div>

<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="{{ URL::to( asset('/'))}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="" data-original-title="Tambah Data"><i class="fa fa-plus-square"></i></a>
        </div>
        <h2><strong>Form Data - Detail Profile</strong></h2>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Tanggal</th>
                <th class="text-center" style="font-size: 12px;">Kegiatan</th>
                <th style="font-size: 12px;">Hasil</th>
                <th style="font-size: 12px;">Foto</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <form id="fileupload" action="/" method="POST" enctype="multipart/form-data">
        <!-- Redirect browsers with JavaScript disabled to the origin page -->
        <noscript><input type="hidden" name="redirect" value="https://blueimp.github.io/jQuery-File-Upload/"></noscript>
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="row fileupload-buttonbar">
            <div class="col-lg-7">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span>Add files...</span>
                    <input type="file" name="files[]" multiple>
                </span>
                <button type="submit" class="btn btn-primary start">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start upload</span>
                </button>
                <button type="reset" class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel upload</span>
                </button>
                <button type="button" class="btn btn-danger delete">
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" class="toggle">
                <!-- The global file processing state -->
                <span class="fileupload-process"></span>
            </div>
            <!-- The global progress state -->
            <div class="col-lg-5 fileupload-progress fade">
                <!-- The global progress bar -->
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                    <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended">&nbsp;</div>
            </div>
        </div>
        <!-- The table listing the files available for upload/download -->
        <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    </form>
    <!-- The blueimp Gallery widget -->
    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>
</div>

@endsection

@section('footer_scripts')
<!-- <script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Start</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Error</span> {%=file.error%}</div>
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Delete</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle">
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Cancel</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>
<script src="{{asset('js/upload/vendor/jquery.ui.widget.js')}}"></script>
<script src="//blueimp.github.io/JavaScript-Templates/js/tmpl.min.js"></script>
<script src="//blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>
<script src="{{asset('js/load-image.all.min.js')}}"></script>
<script src="{{asset('js/canvas-to-blob.min.js')}}"></script>
<script src="{{asset('js/upload/jquery.iframe-transport.js')}}"></script>
<script src="{{asset('js/upload/jquery.fileupload.js')}}"></script>
<script src="{{asset('js/upload/jquery.fileupload-process.js')}}"></script>
<script src="{{asset('js/upload/jquery.fileupload-image.js')}}"></script>
<script src="{{asset('js/upload/jquery.fileupload-audio.js')}}"></script>
<script src="{{asset('js/upload/jquery.fileupload-video.js')}}"></script>
<script src="{{asset('js/upload/jquery.fileupload-validate.js')}}"></script>
<script src="{{asset('js/upload/jquery.fileupload-ui.js')}}"></script>
<script src="{{asset('js/upload/main.js')}}"></script>
 -->
<script type="text/javascript">
  $("#uploadFile").change(function(){
     $('#image_preview').html("");
     var total_file=document.getElementById("uploadFile").files.length;
     for(var i=0;i<total_file;i++)
     {
      $('#image_preview').append("<img src='"+URL.createObjectURL(event.target.files[i])+"'>");
     }
  });

  $('form').ajaxForm(function() 
   {
    alert("Uploaded SuccessFully");
   }); 
</script>
@endsection