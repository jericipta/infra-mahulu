@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection



@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Edit</li>
    <li><a href="">Infrastruktur</a></li>
</ul>
@endsection

@section('content')
<style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview{
      border: 1px solid black;
      padding: 10px;
    }
    #image_preview img{
      width: 200px;
      padding: 5px;
    }
  </style>
<div class="block full">
     {!! Form::open(array('action' => 'ProfileInfraController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-horizontal  form-bordered')) !!}
        <div class="form-group col-sm-12">
            {!! Form::Label('id-kategori', 'Infrastruktur', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::select('id-kategori', $kategori, $profile->id_kategori, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Infrastruktur']) !!}
            </div>
            {!! Form::Label('id-jenis', 'Jenis', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::select('id-jenis', $jenis, $profile->id_jenis, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Jenis']) !!}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('nama', 'Nama', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="text" id="nama" name="nama" value="{{$profile->nama}}" class="form-control" placeholder="Nama">
            </div>
            {!! Form::Label('alamat', 'Alamat', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="text" id="alamat" name="alamat" value="{{$profile->alamat}}" class="form-control" placeholder="Alamat">
            </div>
        </div>
         <div class="form-group col-sm-12">
            {!! Form::Label('id-kecamatan', 'Kecamatan', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::select('id-kecamatan', $kecamatan, $profile->id_kecamatan, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Kecamatan']) !!}
            </div>
            {!! Form::Label('id-desa', 'Desa', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::select('id-desa', $desa, $profile->id_desa, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Kecamatan']) !!}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('id-tahun', 'Tahun', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::select('tahun', $tahun, $profile->tahun, ['class' => 'select-chosen']) !!}
            </div>
            {!! Form::Label('nilai', 'Nilai Anggaran', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="text" id="nilai" name="nilai" value="{{$profile->nilai}}" class="form-control" placeholder="Nilai Anggaran">
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('catatan', 'Catatan', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                <input type="text" id="catatan" name="catatan" value="{{$profile->catatan}}" class="form-control" placeholder="Catatan Kondisi">
            </div>
            {!! Form::Label('tgl-proses', 'Tanggal Proses', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-4">
                {!! Form::text('tgl-proses', $profile->tgl_proses, array('id' => 'datepicker', 'class' => 'form-control')) !!}
            </div>
        </div>
        <div class="form-group col-sm-12">
            {!! Form::Label('uploadFile', 'Foto', ['class' => 'col-md-3 control-label']) !!}
            <div class="row push col-sm-9">
                <div class="col-sm-4 col-md-4">
                    <a href="/images/{{$profile->image_1}}" data-toggle="lightbox-image">
                        <img src="/thumbnail/{{$profile->image_1}}" alt="image">
                    </a>
                </div>
                <div class="col-sm-4 col-md-4">
                    <a href="/images/{{$profile->image_2}}" data-toggle="lightbox-image">
                        <img src="/thumbnail/{{$profile->image_2}}" alt="image">
                    </a>
                </div>
                <div class="col-sm-4 col-md-4">
                    <a href="/images/{{$profile->image_3}}" data-toggle="lightbox-image">
                        <img src="/thumbnail/{{$profile->image_3}}" alt="image">
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="{{ url('/infra-form-infrastruktur')}}" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    {!! Form::close() !!}
</div>

<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="#" class="btn btn-sm btn-success" id="add" data-toggle="modal" data-target="#myModal" title="" data-original-title="Tambah Data"><i class="fa fa-plus-square"></i></a>
        </div>
        <h2><strong>Form Data - Detail Profile</strong></h2>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Tanggal</th>
                <th class="text-center" style="font-size: 12px;">Kegiatan</th>
                <th style="font-size: 12px;">Hasil</th>
                <th style="font-size: 12px;">Foto</th>
            </tr>
            </thead>
            <tbody>
                @foreach($pDetail as $key => $value)
                    <tr data-id="{{$value->id}}">
                        <td>{{ $value->tanggal }}</td>
                        <td>{{ $value->kegiatan}}</td>
                        <td>{{ $value->hasil}}</td>
                        <td>
                            <a href="/images/profile-infra/{{$value->image_1}}" data-toggle="lightbox-image">
                                <img src="/thumbnail/profile-infra/{{$value->image_1}}" alt="image">
                            </a>
                            <a href="/images/profile-infra/{{$value->image_2}}" data-toggle="lightbox-image">
                                <img src="/thumbnail/profile-infra/{{$value->image_2}}" alt="image">
                            </a>
                            <a href="/images/profile-infra/{{$value->image_3}}" data-toggle="lightbox-image">
                                <img src="/thumbnail/profile-infra/{{$value->image_3}}" alt="image">
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    {!! Form::open(array('action' => 'ProfileInfraDetailController@store', 'method' => 'POST', 'files' => true, 'role' => 'form')) !!}
    <input type="hidden" name="id-parent" value="{{$profile->id}}">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Profile Infrastruktur Detail</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {!! Form::Label('tanggal', 'Tanggal', ['class' => 'control-label']) !!}
                    <input type="text" id="tgl-proses" name="tgl-proses" class="form-control input-datepicker" data-date-format="dd-mm-yy" placeholder="dd-MM-yy">
                </div>
                <div class="form-group">
                    {!! Form::Label('kegiatan', 'Kegiatan', ['class' => 'control-label']) !!}
                    <input type="text" id="kegiatan" name="kegiatan" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group">
                    {!! Form::Label('hasil', 'Hasil', ['class' => 'control-label']) !!}
                    <input type="text" id="hasil" name="hasil" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_1" name="image_1" multiple/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_1_name" name="image_1_name" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_2" name="image_2" multiple/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_2_name" name="image_2_name" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_3" name="image_3" multiple/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_3_name" name="image_3_name" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_4" name="image_4"/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_4_name" name="image_4_name" class="form-control" placeholder="Nama">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" id="bnt-update">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

@endsection

@section('footer_scripts')

<script>
    $('#id-kategori').change(function(){
        $.get("{{ url('infra-master-jenis/jenis')}}", 
            { option: $(this).val() }, 
            function(data) {
                var model = $('#id-jenis');
                model.empty();

                $.each(data, function(index, element) {
                    model.append("<option value='"+ index +"'>" + element + "</option>");
                });
            });
    });

    $('#id-kecamatan').change(function(){
        $.get("{{ url('infra-master-desa/desa')}}", 
            { option: $(this).val() }, 
            function(data) {
                var model = $('#id-desa');
                model.empty();

                $.each(data, function(index, element) {
                    model.append("<option value='"+ index +"'>" + element + "</option>");
                });
            });
    });

    function storeData() {
        $.ajax({
            url: "/infra-form-infrastruktur-d",
            type: "POST",
            data: $('#frmData').serialize(),
            beforeSend: function () {
                $(".loader-container").show();
            },
            success: function (data) {
                console.log(data);
                if (data.message == 'Success') {
                    $(".loader-container").hide();
                    swal('Berhasil!', 'Simpan Data Berhasil', 'success');
                    var id = 'headTabCppt';
                } else {
                    $(".loader-container").hide();
                    swal('Gagal!', 'Simpan Data Gagal', 'warning');
                }
            }
        });
    }
</script>
<script>
    $(function() {
        $( "#datepicker" ).datepicker();
    });
</script>
@endsection