@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<div class="content-header">
    <div class="header-section" style="padding: 20px 10px;">
        <h1 class="text-center">
            DATA INFRASTRUKTUR (SARANA DAN PRASARANA) KAMPUNG
        </h1>
        <h1 class="text-center">
            DI KABUPATEN MAHAKAM HULU
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li> Data Infrastruktur</li>
    <li><a href="/users">Listrik</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a class="btn btn-success" id="update-btn" href="#" style="display: none;">Update</a>
            <a class="btn btn-success" id="save-btn" href="#" style="display: none;">Save</a>
        </div>
        <select class="form-control" style="width: 150px" id="kecamatan">
            <option selected disabled>Kecamatan</option>
            @foreach ($kecamatan as $kec)
            <option value="{{$kec->id}}">{{$kec->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
          <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;width: 150px;">Lokasi</th>
                <th class="text-center" style="font-size: 12px;">Bidang Urusan</th>
                <th class="text-center" style="font-size: 12px;">PLTD</th>
                <th class="text-center" style="font-size: 12px;">Genset</th>
                <th class="text-center" style="font-size: 12px;">PLTS Komunal</th>
                <th class="text-center" style="font-size: 12px;">Listrik</th>
                <th class="text-center" style="font-size: 12px;">Jam Ops</th>
                <th class="text-center" style="font-size: 12px;">Keterangan</th>
            </tr>
          </thead>
          <tbody id="result-table">

          </tbody>
        </table>
    </div>
</div>
    
@endsection
@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $('#kecamatan').change(function () {
            $('#result-table').html("");
            getValueKecamatan($('#kecamatan').val());
            $('#update-btn').show();
            $('#save-btn').hide();
        });
        $('#update-btn').click(function () {
            $('.table-responsive td').each(function() {
                var cellText = $(this).html();
                if ($(this).attr("class")==="text-center dinamis"){
                    if (cellText === '<i class="fa fa-check"></i>'){
                        $(this).html("<td class='text-center dinamis'><input type='checkbox' checked></td>");
                    } else {
                        $(this).html("<td class='text-center dinamis'><input type='checkbox' ></td>");
                    }
                } else if($(this).attr("class")==="text-left dinamis-text"){
                    if (cellText === ""){
                        cellText = " ";
                    }
                    $(this).html("<td class='text-left dinamis-text'><input type='text' value='"+cellText+"'></td>");
                }
                
            });
            $('#update-btn').hide();
            $('#save-btn').show();
        });

        $('#save-btn').click(function () {
            var obj = [];
            $('table tr').each(function(index){
                var desa = $(this).find('td:first').text();
                item = {};
                item[desa] = [];
                $(this).find('input').each(function () {
                    var val = $(this).val();
                    if($(this).attr('type') === "checkbox"){
                        if ($(this).is(":checked")){
                            val = "1";
                        }else{
                            val = "0";
                        }    
                    }
                    item[desa].push(val);
                });
                obj.push(item);
            });
            var json = JSON.stringify(obj);
            // console.log(json);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '/infra-listrik/post',
                type: 'POST',
                data: {"data":json},
                success: function( response ){
                    $("#example-datatable tbody").empty();
                    getValueKecamatan($('#kecamatan').val());
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    console.log( errorThrown );
                }
            })
            $('#update-btn').show();
            $('#save-btn').hide();
        });

        function getValueKecamatan(val) {
            $.ajax({
                url: "/get-desa",
                type: "GET",
                data: {
                    'id_kec': val
                },
                success: function (res) {
                    for (var i=0;i < res['data_desa'].length;i++){
                        var nama_desa = res['data_desa'][i]['nama'];
                        var pltd = res['data_desa_listrik'][i][nama_desa]['pltd'];
                        var genset = res['data_desa_listrik'][i][nama_desa]['genset'];
                        var plts_komunal = res['data_desa_listrik'][i][nama_desa]['plts_komunal'];
                        var listrik = res['data_desa_listrik'][i][nama_desa]['listrik'];
                        if (pltd===1){
                            pltd = '<i class="fa fa-check"></i>';
                        } else {
                            pltd = '';
                        }
                        if (genset===1){
                            genset = '<i class="fa fa-check"></i>';
                        } else {
                            genset = '';
                        }
                        if (plts_komunal===1){
                            plts_komunal = '<i class="fa fa-check"></i>';
                        } else {
                            plts_komunal = '';
                        }
                        if (listrik===1){
                            listrik = '<i class="fa fa-check"></i>';
                        } else {
                            listrik = '';
                        }

                        var desa = res['data_desa_listrik'][i][nama_desa];
                        var keterangan = ((typeof desa['keterangan'] === 'undefined') ? " " : desa['keterangan']);
                        var bidang_urusan = ((typeof desa['bidang_urusan'] === 'undefined') ? " " : desa['bidang_urusan']);
                        console.log(res);
                        $('#result-table').append("<tr><td class='text-left dinamis-text-disabled'>"+nama_desa+"</td>" +
                            "<td class='text-left dinamis-text'>"+bidang_urusan+"</td>" +
                            "<td class='text-center dinamis'>"+pltd+"</td>" +
                            "<td class='text-center dinamis'>"+genset+"</td>" +
                            "<td class='text-center dinamis'>"+plts_komunal+"</td>" +
                            "<td class='text-center dinamis'>"+listrik+"</td>"+
                            "<td class='text-left dinamis-text'>"+((typeof desa['jam_operasional'] === 'undefined') ? " " : desa['jam_operasional'])+"</td>" +
                            "<td class='text-left dinamis-text'>"+ keterangan + "</td>" +
                            "</tr>");
                    }

                }
            });
        }
    </script>
@endsection