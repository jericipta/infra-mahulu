@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<div class="content-header">
    <div class="header-section" style="padding: 20px 10px;">
        <h1 class="text-center">
            DATA INFRASTRUKTUR (SARANA DAN PRASARANA) KAMPUNG
        </h1>
        <h1 class="text-center">
            DI KABUPATEN MAHAKAM HULU
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li> Data Infrastruktur</li>
    <li><a href="/users">Listrik</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a class="btn btn-success" id="update-btn" href="#">Update</a>
        </div>
        <select class="form-control" style="width: 150px" id="kecamatan">
            <option selected disabled>Kecamatan</option>
            @foreach ($kecamatan as $kec)
            <option value="{{$kec->id}}">{{$kec->nama}}</option>
            @endforeach
        </select>
        <div class="block-options pull-right">
            <button id="tes">tes</button>
        </div>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
          <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Lokasi</th>
                <th class="text-center" style="font-size: 12px;">Bidang Urusan</th>
                <th class="text-center" style="font-size: 12px;">PLTD</th>
                <th class="text-center" style="font-size: 12px;">Genset</th>
                <th class="text-center" style="font-size: 12px;">PLTS Komunal</th>
                <th class="text-center" style="font-size: 12px;">Tdk ada Listrik</th>
                <th class="text-center" style="font-size: 12px;">Jam Operasional</th>
                <th class="text-center" style="font-size: 12px;">Keterangan</th>
            </tr>
          </thead>
          <tbody id="result-table">

          </tbody>
        </table>
    </div>
</div>
    
@endsection
@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $('#kecamatan').change(function () {
            $('#result-table').html("");
            $.ajax({
                url: "/get-desa",
                type: "GET",
                data: {
                    'id_kec': $('#kecamatan').val()
                },
                success: function (res) {
                    for (var i=0;i < res['data_desa'].length;i++){
                        var nama_desa = res['data_desa'][i]['nama'];
                        var pltd = res['data_desa_listrik'][i][nama_desa]['pltd'];
                        var genset = res['data_desa_listrik'][i][nama_desa]['genset'];
                        var plts_komunal = res['data_desa_listrik'][i][nama_desa]['plts_komunal'];
                        var listrik = res['data_desa_listrik'][i][nama_desa]['listrik'];
                        if (pltd===1){
                            pltd = '<i class="fa fa-check"></i>';
                        } else {
                            pltd = '';
                        }
                        if (genset===1){
                            genset = '<i class="fa fa-check"></i>';
                        } else {
                            genset = '';
                        }
                        if (plts_komunal===1){
                            plts_komunal = '<i class="fa fa-check"></i>';
                        } else {
                            plts_komunal = '';
                        }
                        if (listrik===1){
                            listrik = '<i class="fa fa-check"></i>';
                        } else {
                            listrik = '';
                        }

                        $('#result-table').append("<tr><td class='text-center'>"+res['data_desa'][i]['nama']+"</td>" +
                            "<td class='text-center dinamis-text'>"+res['data_desa_listrik'][i][nama_desa]['keterangan']+"</td>" +
                            "<td class='text-center dinamis'>"+pltd+"</td>" +
                            "<td class='text-center dinamis'>"+genset+"</td>" +
                            "<td class='text-center dinamis'>"+plts_komunal+"</td>" +
                            "<td class='text-center dinamis'>"+listrik+"</td>"+
                            "<td class='text-center dinamis-text'>"+res['data_desa_listrik'][i][nama_desa]['jam_operasional']+"</td>" +
                            "<td class='text-center dinamis-text'>"+res['data_desa_listrik'][i][nama_desa]['keterangan']+"</td>" +
                            "</tr>");
                        console.log(res['data_desa_listrik'][i][nama_desa]);
                    }
//                    console.log(res['data_desa_listrik'][0]['']);

                }
            });
        })
        $('#update-btn').click(function () {
            $('.dinamis').html("<td class='text-center dinamis'><input type='checkbox'></td>");
        })

        var obj = [];
        $( "#tes" ).click(function() {
            $("#example-datatable tbody tr").each(function (index) {
                obj[index] = [];
                $(this).find("td input").each(function(index2){
                    obj[index][index2] = [$(this).is(':checked')];
                    console.log($(this).is(':checked'));
                });
            });
            var json = JSON.stringify(obj); 
            console.log(json);
        });
    </script>
@endsection