@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
    <ul class="breadcrumb breadcrumb-top">
        <li> Master</li>
        <li><a href="/akses">Jenis Akses</a></li>
    </ul>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="block full">
                {!! Form::open(array('action' => 'MasterDinasController@store', 'method' => 'POST', 'role' => 'form', 'class' => 'form-bordered')) !!}
                    <div class="form-group">
                        <label for="name">Nama Dinas</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="Nama Dinas">
                    </div>
                    <div class="form-group form-actions">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Save</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <div class="col-md-7 pull-right">
            <div class="block full">
                <div class="table-responsive">
                <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center" style="font-size: 12px;">Id</th>
                        <th class="text-center" style="font-size: 12px;">Nama Dinas</th>
                        <th class="text-center" style="font-size: 12px;">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dinas as $val)
                        <tr>
                            <td style="font-size: 12px;">{{$val->id}}</td>
                            <td style="font-size: 12px;"">{{$val->name}}</td>
                            <td class="text-center" style="font-size: 12px;">
                                <a href="#" data-toggle="modal" data-target="#myModal" class="btnEdit">
                                    <i class="fa fa-pencil"></i></a> | 
                                <a href="#" data-toggle="modal" data-target="#modal-delete" class="modal-delete" data-id="{{$val->id}}">
                                    <i class="fa fa-trash"></i></a> 
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    {!! Form::open(array('action' => 'MasterDinasController@update', 'method' => 'POST', 'role' => 'form')) !!}
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Jenis Update</h4>
            </div>
            <div class="modal-body">
                <label class="col-md-3 control-label" for="example-text-input">Nama</label>
                <input type="hidden" id="id" name="id">
                <input type="text" id="name" name="name" class="form-control" placeholder="Nama">
                <span class="help-block"></span>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="bnt-update">Update</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<div id="modal-delete" class="modal fade" role="dialog">
    {!! Form::open(array('action' => 'MasterDinasController@delete', 'method' => 'POST', 'role' => 'form')) !!}
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirm</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id2" name="id">
                <span class="help-block"></span>
            </div>
            <div class="modal-footer">
                {!! Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
                {!! Form::button('<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Confirm Delete', array('class' => 'btn btn-danger pull-right btn-flat', 'type' => 'submit', 'id' => 'confirm' )) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection

@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
    $("body").on("click",".btnEdit",function(){
        var id = $(this).parent("td").prev("td").prev("td").text();
        var nama = $(this).parent("td").prev("td").text();
        $('#name').val(nama);
        $('#id').val(id);
    });
    $("body").on("click",".modal-delete",function(){
        $('#id2').val($(this).attr('data-id'));
    });
    </script>
@endsection