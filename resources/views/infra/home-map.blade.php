@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection


@section('header-breadcrumbs')

<ul class="breadcrumb breadcrumb-top">
    <li> Map</li>
</ul>
@endsection
 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css"
   integrity="sha512-M2wvCLH6DSRazYeZRIm1JnYyh22purTM+FDB5CsyxtQJYeKq83arPe5wgbNmcFXGqiSH2XR8dT/fJISVA1r/zQ=="
   crossorigin=""/>
    <!-- Make sure you put this AFTER Leaflet's CSS -->
 <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"
   integrity="sha512-lInM/apFSqyy1o6s89K4iQUKg6ppXEgsVxT35HbzUupEVRh2Eu9Wdl4tHj7dZO0s1uvplcYGmt3498TtHq+log=="
   crossorigin=""></script> 
    <style>
        html, body {
            height: 100%;
            margin: 0;
        }
        #map {
            width: 100%;
            height: 75%;
        }
    </style>
@section('content')
<div id='map'></div>
<script src="{{ asset('json/sample-geojson.js') }}" type="text/javascript"></script>

<script>
    var sites = {!! json_encode($proyek) !!};
    console.log(sites);
    var puskesmas = L.layerGroup();

    var table = "<table width='400px;'><tr><td>Nama Proyek</td><td>"+sites[0].nama_proyek+"</td></tr>" +
                "<tr><td>Lokasi</td><td>"+sites[0].lokasi+"</td></tr></table>"
    L.marker([sites[0].kord_x1+'.'+sites[0].kord_x2,sites[0].kord_y1+'.'+sites[0].kord_y2]).bindPopup(table,{ maxWidth : "auto"}).addTo(puskesmas);

    var mbAttr = 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="http://mapbox.com">Mapbox</a>',
        mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';

    var grayscale   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
        streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets',   attribution: mbAttr});

    var map = L.map('map', {
        center: [0.944625,115.0232333,17],
        zoom: 8,
        layers: [grayscale,puskesmas]
    })

    var baseLayers = {
        "Grayscale": grayscale,
        "Streets": streets
    };
    var overlays = {
        "Puskesmas": puskesmas
    };
    L.control.layers(baseLayers,overlays).addTo(map);
    var baseballIcon = L.icon({
        iconUrl: 'images/baseball-marker.png',
        iconSize: [32, 37],
        iconAnchor: [16, 37],
        popupAnchor: [0, -28]
    });

    function onEachFeature(feature, layer) {
        var popupContent = "<p>Mahakam Hulu</p>";

        if (feature.properties && feature.properties.popupContent) {
            popupContent += feature.properties.popupContent;
        }

        layer.bindPopup(popupContent);
    }

    L.geoJSON([bicycleRental, campus], {

        style: function (feature) {
            return feature.properties && feature.properties.style;
        },

        onEachFeature: onEachFeature,

        pointToLayer: function (feature, latlng) {
            return L.circleMarker(latlng, {
                radius: 8,
                fillColor: "#ff7800",
                color: "#000",
                weight: 1,
                opacity: 1,
                fillOpacity: 0.8
            });
        }
    }).addTo(map);

    L.geoJSON(freeBus, {

        filter: function (feature, layer) {
            if (feature.properties) {
                // If the property "underConstruction" exists and is true, return false (don't render features under construction)
                return feature.properties.underConstruction !== undefined ? !feature.properties.underConstruction : true;
            }
            return false;
        },

        onEachFeature: onEachFeature
    }).addTo(map);

    var coorsLayer = L.geoJSON(coorsField, {

        pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {icon: baseballIcon});
        },

        onEachFeature: onEachFeature
    }).addTo(map);

</script>

    
@endsection
@section('footer_scripts')
@endsection