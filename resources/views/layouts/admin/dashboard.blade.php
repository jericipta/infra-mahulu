@yield('content')
<div class="wrapper">
@extends('layouts.admin.index')
@include('layouts.admin.nav')
@include('layouts.admin.nav-side')
<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            {{--<!-- Small boxes (Stat box) -->--}}
            {{--<div class="col-md-5">--}}
            {{--<div class="box box-success">--}}
                    {{--<form action="savedb" method="POST" enctype="multipart/form-data">--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<div class="box-header with-border">--}}
                            {{--<h3 class="box-title">Import Proyek</h3>--}}
                        {{--</div>--}}
                        {{--<!-- /.box-header -->--}}
                        {{--<!-- form start -->--}}
                            {{--<div class="box-body">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="exampleInputEmail1">Nama Proyek</label>--}}
                                    {{--<input class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="nama_proyek">--}}
                                {{--</div>--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="exampleInputFile">File input</label>--}}
                                    {{--<input id="exampleInputFile" type="file" name="excelFile">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.box-body -->--}}
                            {{--<div class="box-footer">--}}
                                {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
                            {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
                {{--<!-- ./col -->--}}
            {{--</div>--}}
            {{--<!-- /.row -->--}}
            {{--<!-- Main row -->--}}
            <div class="row">
                <div class="col-md-12">
                    <div style="margin-bottom: 5%">
                        <div class="col-lg-3 col-xs-6">
                            <button class="btn btn-success">Unduh Format Data</button>
                            <button class="btn btn-success">Kirim Data</button>
                        </div>
                    </div>
                <!-- Left col -->
                <table class="table table-bordered">
                    <thead>
                        <td>Nama Proyek</td><td>Lokasi</td><td>Panjang</td><td>Sumber Anggaran</td><td>Tahun Anggaran</td><td>Kord-X</td><td>Kord-Y</td><td>Tanggal Kirim</td>
                    </thead>
                    <tbody>
                        @foreach($proyek as $proyeks)
                            <tr>
                                <td>{{$proyeks->nama_proyek}}</td>
                                <td>{{$proyeks->lokasi}}</td>
                                <td>{{$proyeks->tahun_angaran}}</td>
                                <td>{{$proyeks->keterangan}}</td>
                                <td>{{$proyeks->kord_x1}}</td>
                                <td>{{$proyeks->kord_y1}}</td>
                                <td>{{$proyeks->created_at}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                <!-- right col -->
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>
    @include('layouts.admin.footer')
</div>