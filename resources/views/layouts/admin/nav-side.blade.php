<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="admin">
                    <i class="fa fa-dashboard"></i> <span>Jalan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Jalan Kota</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Jalan Desa</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-tree"></i> <span>Jembatan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Jembatan Besar</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Jembatan Sedang</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Jembatan Kecil</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-object-group"></i> <span>Pengairan</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Irigasi</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Sungai</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Danau</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-file-o"></i> <span>Fasilitas Umum</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Taman Kota</a></li>
                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Terminal</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>