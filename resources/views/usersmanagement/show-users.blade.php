@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li> Users List</li>
    <li><a href="/users">Users</a></li>
</ul>
@endsection

@section('content')
<div class="block full">
    <div class="block-title">
         <div class="block-options pull-right">
            <a href="{{ URL::to( asset('/users/deleted'))}}" class="btn btn-sm btn-danger" data-toggle="tooltip" title="" data-original-title="List User Deleted"><i class="fa fa-trash"></i></a>
        </div>
        <div class="block-options pull-right">
            <a href="{{ URL::to( asset('/users/create'))}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="" data-original-title="Tambah Data User"><i class="fa fa-plus-square"></i></a>
        </div>
        <h2><strong>List User</strong></h2>
    </div>
    <div class="table-responsive">
            <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 14px;">ID</th>
                        <th style="font-size: 14px;">Username</th>
                        <th style="font-size: 14px;">Email</th>
                        <th style="font-size: 14px;">First Name</th>
                        <th style="font-size: 14px;">Last Name</th>
                        <th style="font-size: 14px;">Role</th>
                        <th style="font-size: 14px;">Created</th>
                        <th style="font-size: 14px;">Updated</th>
                        <th style="font-size: 14px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td style="font-size: 12px;">{{$user->id}}</td>
                            <td style="font-size: 12px;">{{$user->name}}</td>
                            <td style="font-size: 12px;">{{$user->email}}</td>
                            <td style="font-size: 12px;">{{$user->first_name}}</td>
                            <td style="font-size: 12px;">{{$user->last_name}}</td>
                            <td style="font-size: 12px;">
                                @foreach ($user->roles as $user_role)
                                    @if ($user_role->name == 'User')
                                        @php
                                            $levelIcon        = 'fa fa-user';
                                            $levelName        = 'User';
                                            $levelBgClass     = 'mdl-color--blue-200';
                                            $leveIconlBgClass = 'mdl-color--blue-500';
                                        @endphp
                                    @elseif ($user_role->name == 'Admin')
                                        @php
                                            $levelIcon        = 'fa fa-users';
                                            $levelName        = 'Admin';
                                            $levelBgClass     = 'mdl-color--red-200';
                                            $leveIconlBgClass = 'mdl-color--red-500';
                                        @endphp
                                    @elseif ($user_role->name == 'Unverified')
                                        @php
                                            $levelIcon        = 'fa fa-user-secret';
                                            $levelName        = 'Unverified';
                                            $levelBgClass     = 'mdl-color--orange-200';
                                            $leveIconlBgClass = 'mdl-color--orange-500';
                                        @endphp
                                    @else
                                        @php
                                            $levelIcon        = 'fa fa-user-secret';
                                            $levelName        = 'Unverified';
                                            $levelBgClass     = 'mdl-color--orange-200';
                                            $leveIconlBgClass = 'mdl-color--orange-500';
                                        @endphp
                                    @endif
                                @endforeach
                                <span><i class="{{$levelIcon}}"></i> {{ $levelName }}</span>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric mdl-layout--large-screen-only">{{$user->created_at}}</td>
                            <td class="mdl-data-table__cell--non-numeric mdl-layout--large-screen-only">{{$user->updated_at}}</td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ URL::to('users/' . $user->id . '/edit') }}" class="btn btn-primary" data-toggle="tooltip" title="Edit Selected">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="/profile/{{$user->name}}" class="btn btn-primary" data-toggle="tooltip" title="View Selected"><i class="fa fa-search"></i></a>
                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" class="modal-delete" data-id="{{$user->id}}"><i class="fa fa-trash"></i></a> 
                            </td>
                        </tr>
                        @endforeach
                  </tbody>
            </table>
    </div>
</div>

<div id="modal-delete" class="modal fade" role="dialog">
    {!! Form::open(array('url' => 'users/' . $user->id, 'class' => 'inline-block', 'id' => 'delete_'.$user->id)) !!}
    {!! Form::hidden('_method', 'DELETE') !!}
    <div class="modal-dialog">
        <!— Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirm</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id2" name="id">
                <span class="help-block"></span>
            </div>
            <div class="modal-footer">
                {!! Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
                {!! Form::button('<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Confirm Delete', array('class' => 'btn btn-danger pull-right btn-flat', 'type' => 'submit', 'id' => 'confirm' )) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
@section('footer_scripts')
    <script src="{{asset('ProUI/js/pages/tablesDatatables.js')}}"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $("body").on("click",".modal-delete",function(){
            $('#id2').val($(this).attr('data-id'));
        });
    </script>
@endsection