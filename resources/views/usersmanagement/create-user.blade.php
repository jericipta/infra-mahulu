@extends('infra.layouts.dashboard')
@section('template_title')
    Welcome {{ Auth::user()->name }}
@endsection

@section('header')
    {{ trans('auth.loggedIn', ['name' => Auth::user()->name]) }}
@endsection

@section('header-breadcrumbs')
<ul class="breadcrumb breadcrumb-top">
    <li>Users List</li>
    <li><a href="">Create New User</a></li>
</ul>
@endsection

@section('content')
  <div class="block full">

    <div class="row">
       {!! Form::open(array('action' => 'UsersManagementController@store', 'method' => 'POST', 'role' => 'form',
          'class' => 'form-horizontal ui-formwizard')) !!}
       <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
               {{ Form::label('unit', 'UNIT', array('class' => 'col-md-4 control-label')) }}
              <div class="col-md-8">
                {{ Form::text('unit', Input::old('unit'),array('class' => 'form-control')) }}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
               {!! Form::label('nip', 'NIP', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
                {!! Form::text('nip', NULL, array('id' => 'nip', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('name', trans('auth.name') , array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
                {!! Form::text('name', NULL, array('id' => 'name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z,0-9]*')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('phone', 'Phone', array('class' => 'col-md-4 control-label')) !!}
              <div class="col-md-8">
                {!! Form::text('phone', NULL, array('id' => 'phone', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('email', trans('auth.email') , array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
                {!! Form::email('email', NULL, array('id' => 'email', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('first_name', 'First Name', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
                {!! Form::text('first_name', NULL, array('id' => 'first_name', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('last_name', 'Last Name', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
                {!! Form::text('last_name', NULL, array('id' => 'last_name', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('tanggal_lahir', 'Tanggal Lahir', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
                {!! Form::text('tanggal_lahir', NULL, array('id' => 'datepicker', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('id_skpd', 'SKPD Name', array('class' => 'col-md-4 control-label')) !!}
              <div class="col-md-8">
                <select class="select-chosen" data-placeholder="Select SKPD" name="skpd" id="skpd">
                    <option value="" disabled="true"></option>
                    @if ($skpd->count())
                      @foreach($skpd as $skpds)
                        <option value="{{ $skpds->id }}">{{ $skpds->nama_skpd }}</option>
                      @endforeach
                    @endif
                </select>
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('bagian', 'Bagian', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
              {!! Form::text('bagian', NULL, array('id' => 'bagian', 'class' => 'form-control' )) !!} 
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('role', trans('forms.label-userrole_id'), array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
                <select class="select-chosen" data-placeholder="Select SKPD" name="role" id="role">
                    <option value="" disabled="true"></option>
                    @if ($roles->count())
                          @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                          @endforeach
                        @endif
                </select>
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('password', 'Password', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
              {!! Form::password('password', array('id' => 'password', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('password_confirmation', 'Confirm Password', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
              {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('jabatan', 'Jabatan', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
              {!! Form::text('jabatan', NULL, array('id' => 'jabatan', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              {!! Form::label('bio', 'Bio', array('class' => 'col-md-4 control-label')); !!}
              <div class="col-md-8">
              {!! Form::textarea('bio',  NULL, array('id' => 'bio', 'class' => 'form-control')) !!}
              </div>
          </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-8 col-md-offset-4  pull-left">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                <a href="{{ url('/users/') }}" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Back</a>
            </div>
        </div>
       {!! Form::close() !!}
    </div>
  </div>

@endsection

@section('footer_scripts')

  @include('scripts.mdl-required-input-fix')
  @include('scripts.gmaps-address-lookup-api3')
  <script>
      $(function() {
          $( "#datepicker" ).datepicker();
      });
  </script>
  <script type="text/javascript">
    mdl_dialog('.dialog-button-save');
    mdl_dialog('.dialog-button-icon-save');
  </script>

@endsection