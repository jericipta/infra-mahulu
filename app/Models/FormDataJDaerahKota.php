<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class FormDataJDaerahKota extends Model
{
    protected $table = "t_form_data_jl_d_kota";
    protected $fillable = [
        'dinas',
        'tanggal_proses',
        'status',
        'lokasi'
    ];

    public static function joinDinas(){
    	return DB::table('t_form_data_jl_d_kota as data')
            ->join('t_m_dinas', 'data.dinas', '=', 't_m_dinas.id')
            ->select('data.*','t_m_dinas.name')
            ->get();
    }
}
