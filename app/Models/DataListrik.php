<?php
/**
 * Created by PhpStorm.
 * User: eBdesk
 * Date: 1/6/2018
 * Time: 8:00 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataListrik extends Model
{
    protected $table = "infra_listrik";
}