<?php
/**
 * Created by PhpStorm.
 * User: eBdesk
 * Date: 1/7/2018
 * Time: 4:08 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DataAirBersih extends Model
{
    protected $table = "infra_air_bersih";
}