<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TMInfraJenis extends Model
{
    protected $table = 't_m_infra_jenis';

    public function kategori(){
     	return $this->belongsTo('App\Models\TMInfraKategori');
  	}

  	public static function getCombo($idKategori){
     	$tmJenis = DB::table('t_m_infra_jenis')->select('id,	nama');
    	
    	if($idKategori != "")
    		$tmJenis->where('id_kategori', $idKategori);

        return $tmJenis->get();
  	}
}