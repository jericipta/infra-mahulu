<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProyekP extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "proyek_p";
    protected $fillable = [
        'id','nama_file', 'nama_user', 'path_file','id_proyek'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}