<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileInfraDetail extends Model
{
    protected $table = 't_form_infrastruktur_d';
    protected $fillable = ['id_form_infra_p', '	tanggal', 'kegiatan', 'hasil', 'image_1','image_2','image_3','image_3', 'image_1_name', 'image_2_name', 'image_3_name', 'image_4_name'];
}
