<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProfileInfra extends Model
{
    protected $table = 't_form_infrastruktur_p';
    protected $fillable = ['id_kategori', 'id_jenis', 'id_kecamatan', 'id_desa', 'nama', 'alamat'
                          , 'tahun', 'nilai', 'catatan', 'tgl_proses','kord_x1','kord_x2','kord_y1'
                          , 'kord_y2','image_1','image_2','image_3'];

  	public static function getInfra(){
     	return DB::table('t_form_infrastruktur_p as profile')
            ->join('t_m_desa', 'profile.id_desa', '=', 't_m_desa.id')
            ->join('t_m_infra_jenis', 'profile.id_jenis', '=', 't_m_infra_jenis.id')
            ->join('t_m_infra_kategory', 'profile.id_kategori', '=', 't_m_infra_kategory.id')
            ->join('t_kecamatan', 'profile.id_kecamatan', '=', 't_kecamatan.id')
            ->select('profile.id','profile.alamat',
            		't_m_desa.nama as nama_desa',
            		't_m_infra_jenis.nama as nama_jenis',
            		't_m_infra_kategory.nama as nama_kategori',
            		't_kecamatan.nama as nama_kecamatan')
            ->get();
  	}

}
