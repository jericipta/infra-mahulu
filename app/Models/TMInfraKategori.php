<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TMInfraKategori extends Model
{
    protected $table = 't_m_infra_kategory';

    public function jenis(){
     	return $this->hasMany('App\Models\TMInfraJenis');
  	}
}
