<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class FormDataJSKPD extends Model
{
    protected $table = "t_form_data_skpd";
    protected $fillable = [
        'ID_FORM_DATA_SKPD',
        'ID_SKPD',
        'NAMA_FORM_DATA_SKPD',
        'KETERANGAN',
        'FILE_FORM',
        'TABEL_FORM'
    ];

	public function skpd()
    {
        return $this->hasMany('App\Models\Skpd', 'foreign_key');
    }

    public static function joinSkpd(){
    	return DB::table('t_form_data_skpd as fskpd')
            ->join('skpd', 'fskpd.ID_SKPD', '=', 'skpd.id')
            ->select('fskpd.*','skpd.nama_skpd')
            ->get();
    }
}	
