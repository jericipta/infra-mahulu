<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MasterDinas extends Model
{
    protected $table = "t_m_dinas";
    protected $fillable = [
        'id',
        'name',
        'created_date',
        'updated_date'
    ];
}
