<?php
/**
 * Created by PhpStorm.
 * User: eBdesk
 * Date: 1/6/2018
 * Time: 8:00 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataTelekomunikasi extends Model
{
    protected $table = "t_infra_telekomunikasi";
}