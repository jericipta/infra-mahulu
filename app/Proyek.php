/**
 * Created by PhpStorm.
 * User: eBdesk
 * Date: 11/29/2017
 * Time: 2:53 AM
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Proyek extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "proyek";
    protected $fillable = [
        'nama_proyek', 'lokasi', 'panjang','sumber_anggran','id','tahun_anggaran','jumlah_anggaran','kord_x1','kord_x2','kord_y1','kord_y2','keterangan'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}