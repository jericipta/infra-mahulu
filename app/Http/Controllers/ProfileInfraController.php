<?php

namespace App\Http\Controllers;

use App\Models\FormDataJSKPD;
use App\Models\ProfileInfra; 
use App\Models\ProfileInfraDetail;
use App\Models\Skpd;
use Illuminate\Http\Request;
use App\Models\TMKecamatan;
use App\Models\TMInfraKategori; 
use Image;
use App\Models\TMInfraJenis;
use App\Models\TMDesa;
use DB;

class ProfileInfraController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formdata = ProfileInfra::getInfra();
        return view('infra.data-infra.profile-infra.index',['formdata'=>$formdata,'empty'=>Array()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = TMInfraKategori::pluck('nama', 'id');
        $kecamatan = TMKecamatan::pluck('nama', 'id');
        $tahun[2015] = '2015';
        $tahun[2016] = '2016';
        $tahun[2017] = '2017';
        $tahun[2018] = '2018';
        $tahun[2019] = '2019';
        $tahun[2020] = '2020';
        return view('infra.data-infra.profile-infra.create',['kategori'=>$kategori,'kecamatan'=>$kecamatan,'tahun'=>$tahun]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $image1="";
        $image2="";
        $image3="";
        $i=0;
        if(sizeof($request->file('uploadFile')) > 0){
            foreach ($request->file('uploadFile') as $key => $value) {
                $imageName = time(). $key . '.' . $value->getClientOriginalExtension();
                $destinationPath = public_path('/thumbnail');
                $img = Image::make($value->getRealPath());
                $img->resize(100, 100, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'.$imageName);
               
                $destinationPath = public_path('/images');
                $value->move($destinationPath, $imageName);

                if($i==0){
                    $image1 = $imageName;
                }elseif ($i==1) {
                    $image2 = $imageName;
                }elseif ($i==2) {
                    $image3 = $imageName;
                }
                $i++;
            }
        }
        

        $date = date($request->input('tgl-proses'));
        $tgl_proses = date_format(date_create_from_format('d/m/Y', $date), 'Y-m-d');

        $profile = ProfileInfra::create([
            'id_kategori' => $request->input('id-kategori'),
            'id_jenis' => $request->input('id-jenis'),
            'nama' => $request->input('nama'),
            'alamat' => $request->input('alamat'),
            'id_kecamatan' => $request->input('id-kecamatan'),
            'id_desa' => $request->input('id-desa'),
            'tahun' => $request->input('tahun'),
            'nilai' => $request->input('nilai'),
            'catatan' => $request->input('catatan'),
            'tgl_proses' => $tgl_proses,
            'kord_x1' => ($request->input('kord_x1') == null) ? 0 : $request->input('kord_x1'),
            'kord_x2' => ($request->input('kord_x2') == null) ? 0 : $request->input('kord_x2'),
            'kord_y1' => ($request->input('kord_y1') == null) ? 0 : $request->input('kord_y1'),
            'kord_y2' => ($request->input('kord_y2') == null) ? 0 : $request->input('kord_y2'),
            'image_1' => $image1,
            'image_2' => $image2,
            'image_3' => $image3,
        ]);
        // echo $image1 . " = " . $image2. " = " . $image3 ;die;   
        $profile->save();
        return redirect('infra-form-infrastruktur');
    }

    public function profileMap($id)
    {
        $proyek = DB::table('t_form_infrastruktur_p')->where('id','=', $id)->get();
        return view('infra.home-map',['proyek'=>$proyek]);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\ProfileInfra  $profileInfra
     * @return \Illuminate\Http\Response
     */
    public function show(ProfileInfra $profileInfra)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfileInfra  $profileInfra
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = ProfileInfra::find($id);
        $pDetail = DB::table('t_form_infrastruktur_d')
            ->where('id_form_infra_p', '=', $profile->id)
            ->get();
        $kategori = TMInfraKategori::pluck('nama', 'id');
        $kecamatan = TMKecamatan::pluck('nama', 'id');
        $jenis = TMInfraJenis::where('id_kategori',$profile->id_kategori)->pluck('nama', 'id');
        $desa = TMDesa::where('id_kec',$profile->id_kecamatan)->pluck('nama', 'id');
        $tahun[2015] = '2015';
        $tahun[2016] = '2016';
        $tahun[2017] = '2017';
        $tahun[2018] = '2018';
        $tahun[2019] = '2019';
        $tahun[2020] = '2020';

        $date = date($profile->tgl_proses);
        $tgl_proses = date_format(date_create_from_format('Y-m-d', $date), 'd/m/Y');
        $profile->tgl_proses = $tgl_proses;
        return view('infra.data-infra.profile-infra.edit',['kategori'=>$kategori,'kecamatan'=>$kecamatan,'tahun'=>$tahun,
            'profile'=> $profile,'jenis'=>$jenis, 'desa'=>$desa, 'pDetail' => $pDetail]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfileInfra  $profileInfra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfileInfra $profileInfra)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfileInfra  $profileInfra
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfileInfra $profileInfra)
    {
        //
    }

    public function delete(Request $request)
    {
        DB::table('t_form_infrastruktur_d')->where('id_form_infra_p', '=', $request->input('id'))->delete();
        $pp = ProfileInfra::find($request->input('id'));
        $pp->delete();
        return redirect('infra-form-infrastruktur');
    }
}
