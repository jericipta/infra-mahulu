<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoleModel;
use Illuminate\Support\Facades\DB;
class RoleController extends Controller
{
    //
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $role = RoleModel::all();

        return view('role.index',['role'=>$role]);
    }

    public function store(Request $request){
        $skpd = RoleModel::create([
            'name' => $request->input('role_name'),
            'slug' => $request->input('role-list'),
            'description' => $request->input('descript'),
            'level' => $request->input('level')
        ]);
        $skpd->save();
        return redirect('role');
    }
    public function update(Request $request) {
        $id = $request->input('idRoleedit');
        $name = $request->input('role_name');
        $slug = $request->input('slug-list-edit');
        $desc = $request->input('descript');
        $lvl = $request->input('level');
        $roles = RoleModel::find($id);
        $roles->slug = $slug;
        $roles->name = $name;
        $roles->description = $desc;
        $roles->level = $lvl;
        $roles->save();
        return redirect('role');
    }

    public function delete(Request $request)
    {
        $role = RoleModel::find($request->input('id'));
 
        $role->delete();

        // redirect
        return redirect('role');
    }
}
