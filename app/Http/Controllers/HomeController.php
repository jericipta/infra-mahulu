<?php

namespace App\Http\Controllers;

use App\Models\Proyek;
use App\Models\ProyekP;
use Mockery\Exception;
use PHPExcel;
use PHPExcel_IOFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('infra.home');
    }

    public function home() {
        $proyek = DB::table('proyek_p')->get();
        // $proyek = DB::table('proyek')->where('nama_proyek','!=', 'null')->get();
        return view('infra.home-p',['proyek'=>$proyek]);
    }

    public function detail($id) {
        $proyek = DB::table('proyek')->where('nama_proyek','!=', 'null')->where('id_parent','=', $id)->get();
        return view('infra.home',['proyek'=>$proyek]);
    }

    public function import()
    {
        $length = 10;   
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }


        $time = time() . $randomString ;



        $image = Input::file('excelFile');
        $imagePath = $image->getRealPath();
        $rdmInt = rand(0,999);
        $target = '../public/excel_file/'.$rdmInt.$image->getClientOriginalName();
        move_uploaded_file($imagePath, $target);
        $path = $target;
        try {
            $inputFileType = PHPExcel_IOFactory::identify($path);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($path);
        } catch (Exception $e) {
            die('Error loading file "' . pathinfo($path, PATHINFO_BASENAME) . '": ' . $e->getMessage());
        }

//  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();    

        $proyekP = new ProyekP();
        $proyekP->id_proyek = $time;
        $proyekP->nama_file = $image->getClientOriginalName();
        $proyekP->nama_user = \Auth::user()->name;
        $proyekP->path_file = $target;
        $proyekP->save();

//  Loop through each row of the worksheet in turn
        for ($row = 5; $row <= $highestRow; $row++) {
            //  Read a row of data into an array
            try {
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                    NULL,
                    TRUE,
                    FALSE);
//            var_dump($rowData);
//              exit();
                $proyek = new Proyek();
                $proyek->id_parent = $time;
                $proyek->nama_proyek = $rowData[0][1];
                $proyek->lokasi = $rowData[0][2];
                $proyek->panjang = $rowData[0][3];
                $proyek->sumber_anggaran = $rowData[0][4];
                $proyek->tahun_angaran = $rowData[0][5];
                $proyek->jumlah_anggaran = $rowData[0][6];
                $proyek->kord_x1 = $rowData[0][7];
                $proyek->kord_x2 = $rowData[0][8];
                $proyek->kord_y1 = $rowData[0][9];
                $proyek->kord_y2 = $rowData[0][10];
                $proyek->keterangan = $rowData[0][11];
                $proyek->save();
            } catch (Exception $e){
                var_dump($e);die;
            }
        }
        return redirect('/');

    }

    public function imagesUpload()
    {
        return view('infra.imagesUpload');
    }

    public function homeMap($id)
    {
        $proyek = DB::table('proyek')->where('id','=', $id)->get();
        return view('infra.home-map',['proyek'=>$proyek]);
    }

    public function imagesUploadPost(Request $request)
    {
        // request()->validate([
        //     'uploadFile' => 'required',
        // ]);

        foreach ($request->file('uploadFile') as $key => $value) {
            $imageName = time(). $key . '.' . $value->getClientOriginalExtension();
            $value->move(public_path('images'), $imageName);
        }

        return response()->json(['success'=>'Images Uploaded Successfully.']);
    }

}
