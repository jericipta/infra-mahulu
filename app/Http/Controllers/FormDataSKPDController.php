<?php

namespace App\Http\Controllers;

use App\Models\FormDataJSKPD;
use App\Models\Skpd;
use Illuminate\Http\Request;

class FormDataSKPDController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $formdata = FormDataJSKPD::joinSkpd();
        return view('infra.form-data.form-data-skpd.index',['formdata'=>$formdata]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $skpd = Skpd::pluck('nama_skpd', 'id');
        return view('infra.form-data.form-data-skpd.create',['skpd'=>$skpd]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file-form');
       
        //Display File Name
        echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';

        //Display File Extension
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';

        //Display File Real Path
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';

        //Display File Size
        echo 'File Size: '.$file->getSize();
        echo '<br>';

        //Display File Mime Type
        echo 'File Mime Type: '.$file->getMimeType();

        //Move Uploaded File
        $destinationPath = 'uploads\form-data\skpd';
        $file->move($destinationPath,$file->getClientOriginalName());
        

        $fskpd = FormDataJSKPD::create([
            'ID_SKPD' => $request->input('id-skpd'),
            'NAMA_FORM_DATA_SKPD' => $request->input('nama-skpd'),
            'KETERANGAN' => $request->input('keterangan'),
            'FILE_FORM' => $destinationPath . '\\'. $file->getClientOriginalName(),
            'TABEL_FORM' => $request->input('table-name'),
        ]);
        $fskpd->save();
        return redirect('form-data-skpd');
    }   

    /**
     * Display the specified resource.
     *
     * @param  \App\FormDataJSKPD  $formDataJSKPD
     * @return \Illuminate\Http\Response
     */
    public function show(FormDataJSKPD $formDataJSKPD)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormDataJSKPD  $formDataJSKPD
     * @return \Illuminate\Http\Response
     */
    public function edit(FormDataJSKPD $formDataJSKPD)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormDataJSKPD  $formDataJSKPD
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormDataJSKPD $formDataJSKPD)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormDataJSKPD  $formDataJSKPD
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormDataJSKPD $formDataJSKPD)
    {
        //
    }

    public function delete(Request $request)
    {
        //Delete the instrument

        // $skpd = FormDataJSKPD::find($request->input('id'));
        $skpd = FormDataJSKPD::where('ID_FORM_DATA_SKPD', $request->input('id'));
        $skpd->delete();

        // redirect
        return redirect('form-data-skpd');
    }
}
