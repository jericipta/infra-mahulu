<?php

namespace App\Http\Controllers;

use App\Models\TMInfraJenis;
use Illuminate\Http\Request;
use Response;
class TMInfraJenisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        var_dump('tes');
        die;
    }

    public function jenis(Request $request)
    {
        $data = TMInfraJenis::where('id_kategori',$request->input('option'))->pluck('nama', 'id');
        return Response::make($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TMInfraJenis  $tMInfraJenis
     * @return \Illuminate\Http\Response
     */
    public function show(TMInfraJenis $tMInfraJenis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TMInfraJenis  $tMInfraJenis
     * @return \Illuminate\Http\Response
     */
    public function edit(TMInfraJenis $tMInfraJenis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TMInfraJenis  $tMInfraJenis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TMInfraJenis $tMInfraJenis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TMInfraJenis  $tMInfraJenis
     * @return \Illuminate\Http\Response
     */
    public function destroy(TMInfraJenis $tMInfraJenis)
    {
        //
    }
}
