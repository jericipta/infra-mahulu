<?php

namespace App\Http\Controllers;

use App\KirimJembatan;
use Illuminate\Http\Request;
use DB;

class KirimJembatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proyek = DB::table('proyek')->get();
        return view('infra.kirim-file.fasum.index',['proyek'=>$proyek]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KirimJembatan  $kirimJembatan
     * @return \Illuminate\Http\Response
     */
    public function show(KirimJembatan $kirimJembatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KirimJembatan  $kirimJembatan
     * @return \Illuminate\Http\Response
     */
    public function edit(KirimJembatan $kirimJembatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KirimJembatan  $kirimJembatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KirimJembatan $kirimJembatan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KirimJembatan  $kirimJembatan
     * @return \Illuminate\Http\Response
     */
    public function destroy(KirimJembatan $kirimJembatan)
    {
        //
    }
}
