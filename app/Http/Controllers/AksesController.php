<?php

namespace App\Http\Controllers;

use App\Models\Akses;
use Illuminate\Http\Request;

class AksesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $akses = Akses::all();
        return view('infra.akses.index',['akses'=>$akses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $akses = Akses::create([
            'name' => $request->input('name')
        ]);
        $akses->save();
        return redirect('akses');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function show(Akses $akses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function edit(Akses $akses)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id');
        $name = $request->input('name');
        $akses = Akses::find($id);
        $akses->name = $name;
        $akses->save();
        return redirect('akses');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Akses  $akses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Akses $akses)
    {
        $akses = Akses::find($id);
 
        $akses->delete();

        // redirect
        return redirect('akses');
    }

    public function delete(Request $request)
    {
        $akses = Akses::find($request->input('id'));
 
        $akses->delete();

        // redirect
        return redirect('akses');
    }
}
