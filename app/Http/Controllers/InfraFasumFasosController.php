<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class InfraFasumFasosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data[] = array("Kampung Long Apari", false,false,false,false,true,false,"","");
        $data[] = array("Kampung Noha Silat", false,false,false,false,true,false,"","");
        $data[] = array("Kampung Long Tivab", false,false,true,false,true,false,"","");
        $data[] = array("Kampung Tiong Ohang", false,false,false,false,false,false,"","");
        $data[] = array("Kampung Long Bu'u", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Noha Buan", false,false,false,true,false,false,"","");
        $data[] = array("Kampung Long Pemaneh I", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Long Pemaneh II", false,false,false,true,false,false,"","");
        $data[] = array("Kampung Long Pemaneh III", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Long Keriok", false,false,false,false,true,false,"","");

        return view('infra.data-infra.listrik.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
