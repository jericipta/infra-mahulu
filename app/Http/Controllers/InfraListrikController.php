<?php

namespace App\Http\Controllers;

use App\Models\DataJalanModel;
use App\Models\TMKecamatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\TMDesa;
use App\Models\DataListrik;
use Illuminate\Support\Facades\DB;
class InfraListrikController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data[] = array("Kampung Long Apari", false,false,false,false,true,false,"","");
        $data[] = array("Kampung Noha Silat", false,false,false,false,true,false,"","");
        $data[] = array("Kampung Long Tivab", false,false,true,false,true,false,"","");
        $data[] = array("Kampung Tiong Ohang", false,false,false,false,false,false,"","");
        $data[] = array("Kampung Long Bu'u", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Noha Buan", false,false,false,true,false,false,"","");
        $data[] = array("Kampung Long Pemaneh I", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Long Pemaneh II", false,false,false,true,false,false,"","");
        $data[] = array("Kampung Long Pemaneh III", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Long Keriok", false,false,false,false,true,false,"","");
        $kecamatan = new TMKecamatan();
        $data_kecamatan = $kecamatan->get();
        return view('infra.data-infra.listrik.index', ['data'=>$data, 'kecamatan'=>$data_kecamatan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function post()
    {
        $listrik = new DataListrik;
        // $a = json_decode($request->getContent(), true);
        $a = Input::all();
        $a = json_decode($a['data']);
        $e = 0;
        $c = "success";
        for($i=1;$i<count($a);$i++){
            foreach ($a[$i] as $key => $value){
                try {
                    $arr = array(
                        'location'=>$key,
                        'bidang_urusan' => $value[0],
                        'pltd' => $value[1],
                        'genset' => $value[2],
                        'plts' => $value[3],
                        'plts_komunal' => $value[3],
                        'listrik' => $value[4],
                        'jam_operasional' => $value[5],
                        'keterangan' => $value[6]
                    );
                    DB::table('infra_listrik')->where('location', '=', $key)->delete();
                    DB::table('infra_listrik')->insert($arr);
                    $c = "success";
                } catch (\Exception $ex) {
                    $c = "success";
                }
            }
        };
        return $c;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // strore nya di mana zal
        //waktu submit insert ke db
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function desa(Request $req)
    {
        $desa_data = new TMDesa();
        $data_lis = new DataListrik();
        $data_desa = $desa_data->where('id_kec','=',$req->id_kec)->get();
        $resut_desa_listrik = array(new \ArrayObject());
        $nama_desa = array();
        foreach ($data_desa as $d_desa){
            $data_listrik = $data_lis->where('location','=',$d_desa['nama'])->get();
            $desa_listrik = array($d_desa['nama']=>new \ArrayObject());
            foreach ($data_listrik as $d_listrik){
                $desa_listrik[$d_desa['nama']]= array(
                    "pltd"=>$d_listrik['pltd'],
                    "bidang_urusan"=>$d_listrik['bidang_urusan'],
                    "genset"=>$d_listrik['genset'],
                    "plts"=>$d_listrik['plts'],
                    "plts_komunal"=>$d_listrik['plts_komunal'],
                    "listrik"=>$d_listrik['listrik'],
                    "jam_operasional"=>$d_listrik['jam_operasional'],
                    "keterangan"=>$d_listrik['keterangan'],
                    "created_at"=>$d_listrik['created_at'],
                    "updated_at"=>$d_listrik['updated_at'],
                );
            }
            array_push($nama_desa,$desa_listrik);
        }
        $resut_desa_listrik=$nama_desa;
        return ['data_desa'=>$data_desa, 'data_desa_listrik'=>$resut_desa_listrik];
    }

    public function new(Request $req)
    {
        return $req;
    }
    
}
