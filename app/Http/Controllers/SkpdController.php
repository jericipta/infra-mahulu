<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Skpd;

class SkpdController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $skpd = Skpd::all();
        return view('skpd.index',['skpd'=>$skpd]);
    }

    public function store(Request $request){
        $skpd = Skpd::create([
            'nama_skpd' => $request->input('nama_skpd'),
            'alamat_skpd' => $request->input('alamat_skpd')
        ]);
        $skpd->save();
        return redirect('skpd');
    }

    public function update(Request $request){
        $id = $request->input('idRoleedit');
        $name = $request->input('role_name');
        $desc = $request->input('descript');
        $roles = Skpd::find($id);
        $roles->nama_skpd = $name;
        $roles->alamat_skpd = $desc;
        $roles->save();
        return redirect('skpd');
    }

    public function destroy($id)
    {
        //
    }

    public function delete(Request $request)
    {
        //Delete the instrument
        $skpd = Skpd::find($request->input('id'));
 
        $skpd->delete();

        // redirect
        return redirect('skpd');
    }

}
