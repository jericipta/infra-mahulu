<?php

namespace App\Http\Controllers;

use App\Models\ProfileInfraDetail;
use App\Models\FormDataJSKPD;
use App\Models\ProfileInfra; 
use App\Models\Skpd;
use Illuminate\Http\Request;
use App\Models\TMKecamatan;
use App\Models\TMInfraKategori; 
use Image;
use App\Models\TMInfraJenis;
use App\Models\TMDesa;

class ProfileInfraDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    private static function saveImage($requestFile){
        // check null
        if($requestFile == null){
            return "";
        }

        $destinationPath = public_path('/thumbnail/profile-infra');
        $image = time().'.'.$requestFile->getClientOriginalExtension();
        $img = Image::make($requestFile->getRealPath());
        
        $img->resize(50, 50, function ($constraint) {
            $constraint->aspectRatio();
        })->save($destinationPath.'/'.$image);

        $destinationPath = public_path('/images/profile-infra');
        $requestFile->move($destinationPath, $image);
        return $image;
    }

    public function store(Request $request)
    {

        $image1 = $this->saveImage($request->file('image_1'));
        $image2 = $this->saveImage($request->file('image_2'));
        $image3 = $this->saveImage($request->file('image_3'));
        $image4 = $this->saveImage($request->file('image_4'));

        $profileD = ProfileInfraDetail::create([
            'id_form_infra_p' => $request->input('id-parent'),
            'tanggal' => $request->input('tgl-proses'),
            'kegiatan' => $request->input('kegiatan'),
            'hasil' => $request->input('hasil'),
            'image_1' => $image1,
            'image_1_name' => $request->input('image_1_name'),
            'image_2' => $image2,
            'image_2_name' => $request->input('image_2_name'),
            'image_3' => $image3,
            'image_3_name' => $request->input('image_3_name'),
            'image_4' => $image4,
            'image_4_name' => $request->input('image_4_name'),
        ]);
        $profileD->save();
        return redirect()->action('\App\Http\Controllers\ProfileInfraController@edit', [$request->input("id-parent")]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProfileInfraDetail  $profileInfraDetail
     * @return \Illuminate\Http\Response
     */
    public function show(ProfileInfraDetail $profileInfraDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfileInfraDetail  $profileInfraDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfileInfraDetail $profileInfraDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfileInfraDetail  $profileInfraDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProfileInfraDetail $profileInfraDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfileInfraDetail  $profileInfraDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfileInfraDetail $profileInfraDetail)
    {
        //
    }
}
