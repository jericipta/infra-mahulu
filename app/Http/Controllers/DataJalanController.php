<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DataJalanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data_pu = DB::table('t_jalan_pu')->get();
        return view('history_jalan_pu.index',['data'=>$data_pu]);
    }
}
