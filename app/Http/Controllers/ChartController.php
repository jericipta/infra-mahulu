<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\RoleModel;
use Mapper;

class ChartController extends Controller
{
    public function index() {
        return view('chart.chart');
    }

    public function maps() {
//        Mapper::map(53.381128999999990000, -1.470085000000040000);
        Mapper::map(55.381128999999990000, -1.470085000000040000);
        return view('maps.index');
    }
}
