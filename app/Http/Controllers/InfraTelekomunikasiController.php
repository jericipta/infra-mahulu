<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TMKecamatan;
use App\Models\TMDesa;
use App\Models\DataTelekomunikasi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class InfraTelekomunikasiController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data[] = array("Kampung Long Apari", false,false,false,false,true,false,"","");
        $data[] = array("Kampung Noha Silat", false,false,false,false,true,false,"","");
        $data[] = array("Kampung Long Tivab", false,false,true,false,true,false,"","");
        $data[] = array("Kampung Tiong Ohang", false,false,false,false,false,false,"","");
        $data[] = array("Kampung Long Bu'u", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Noha Buan", false,false,false,true,false,false,"","");
        $data[] = array("Kampung Long Pemaneh I", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Long Pemaneh II", false,false,false,true,false,false,"","");
        $data[] = array("Kampung Long Pemaneh III", false,false,true,false,false,false,"","");
        $data[] = array("Kampung Long Keriok", false,false,false,false,true,false,"","");
        $kecamatan = new TMKecamatan();
        $data_kecamatan = $kecamatan->get();
        return view('infra.data-infra.telekomunikasi.index', ['data'=>$data, 'kecamatan'=>$data_kecamatan]);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function post(){
        $a = Input::all();
        $a = json_decode($a['data']);
        $e = 0;
        $c = "success";
        for($i=1;$i<count($a);$i++){
            foreach ($a[$i] as $key => $value){

                try {
                    if(sizeof($value) > 0){
                        $arr = array(
                            'lokasi'=>$key,
                            'bidang_urusan' => $value[0],
                            'akses_telekomunikasi' => $value[1],
                            'keterangan' => $value[2]
                        );
                        DB::table('t_infra_telekomunikasi')->where('lokasi', '=', $key)->delete();
                        DB::table('t_infra_telekomunikasi')->insert($arr);
                        $c = "success";
                    }
                } catch (\Exception $ex) {
                    $c = $ex;
                }
            }
        };
        return $c;
    }
    public function create()
    {
        //
        $listrik = new DataTelekomunikasi();
        // $a = json_decode($request->getContent(), true);
        $a = Input::all();
        $a = json_decode($a['data']);
        $e = 0;
        $c = "success";
        for($i=1;$i<count($a);$i++){
            foreach ($a[$i] as $key => $value){
                try {
                   $arr = array(
                        'lokasi'=>$key,
                        'bidang_urusan' => $value[0],
                        'akses_telekomunikasi' => $value[1],
                        'keterangan' => $value[2]
                    );
                    DB::table('t_infra_telekomunikasi')->where('location', '=', $key)->delete();
                    DB::table('t_infra_telekomunikasi')->insert($arr);
                    $c = "success";
                } catch (\Exception $ex) {
                    $c = $ex;
                }
            }
        };
        return $c;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function desa(Request $req)
    {
        $desa_data = new TMDesa();
        $data_lis = new DataTelekomunikasi();
        $data_desa = $desa_data->where('id_kec','=',$req->id_kec)->get();
        $resut_desa_listrik = array(new \ArrayObject());
        $nama_desa = array();
        foreach ($data_desa as $d_desa){
            $data_air = $data_lis->where('lokasi','=',$d_desa['nama'])->get();
            $desa_air = array($d_desa['nama']=>new \ArrayObject());
            foreach ($data_air as $d_air){
                $desa_air[$d_desa['nama']]= array(
                    "bidang_urusan"=>$d_air['bidang_urusan'],
                    "akses_telekomunikasi"=>$d_air['akses_telekomunikasi'],
                    "ket"=>$d_air['keterangan'],
                );
            }
            array_push($nama_desa,$desa_air);
        }
        $resut_desa_listrik=$nama_desa;
        return ['data_desa'=>$data_desa, 'data_desa_listrik'=>$resut_desa_listrik];
    }
}
