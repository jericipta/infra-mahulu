<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TMKecamatan;
use App\Models\TMDesa;
use App\Models\DataAirBersih;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class InfraAirBersihController extends Controller
{
        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $data[] = array("Kampung Long Apari", "",578,false,false,0,"");
        // $data[] = array("Kampung Noha Silat", "",345,true,false,0,"");
        // $data[] = array("Kampung Long Tivab", "",345,false,false,0,"");
        // $data[] = array("Kampung Tiong Ohang", "",720,false,false,0,"");
        // $data[] = array("Kampung Long Bu'u", "",602,true,false,0,"");
        // $data[] = array("Kampung Noha Buan", "",747,false,false,0,"");
        // $data[] = array("Kampung Long Pemaneh I", "",473,true,false,0,"");
        // $data[] = array("Kampung Long Pemaneh II", "",187,true,false,0,"");
        // $data[] = array("Kampung Long Pemaneh III", "",198,true,false,0,"");
        // $data[] = array("Kampung Long Keriok", "",503,true,false,0,"");
        $kecamatan = new TMKecamatan();
        $data_kecamatan = $kecamatan->get();
        // return view('infra.data-infra.air-bersih.index', ['data'=>$data, 'kecamatan'=>$data_kecamatan]);
        return view('infra.data-infra.air-bersih.index', ['kecamatan'=>$data_kecamatan]);
    }

    public function post(){
        $listrik = new DataAirBersih();
        // $a = json_decode($request->getContent(), true);
        $a = Input::all();
        $a = json_decode($a['data']);
        $e = 0;
        $c = "success";
        for($i=1;$i<count($a);$i++){
            foreach ($a[$i] as $key => $value){
                try {
                    if(sizeof($value) > 0){
                        $arr = array(
                            'location'=>$key,
                            'jumlah_penduduk' => $value[1],
                            'sab' => $value[2],
                            'kapasitas_lt_dk' => $value[3],
                            'sungai' => $value[4],
                            'keterangan' => $value[5]
                        );
                            // DB::table('infra_air_bersih')->where('location',$key)->update($arr);
                        // DB::table('infra_air_bersih')->where('location', '=', $key)->delete();
                        DB::table('infra_air_bersih')->where('location', '=', $key)->delete();
                        DB::table('infra_air_bersih')->insert($arr);
                        $c = "success";    
                    }
                    
                } catch (\Exception $ex) {
                    $c = $ex;
                }
            }
        };
        return $c;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $listrik = new DataAirBersih();
        // $a = json_decode($request->getContent(), true);
        $a = Input::all();
        $a = json_decode($a['data']);
        $e = 0;
        $c = "success";
        for($i=1;$i<count($a);$i++){
            foreach ($a[$i] as $key => $value){
                try {
                    $check = DB::table('infra_air_bersih')->where('location','=',$key)->get();
                    if (count($check) == 0){
                        $arr = array(
                            'location'=>$key,
                            'jumlah_penduduk' => $value[1],
                            'sab' => $value[2],
                            'kapasitas_lt_dk' => $value[3],
                            'sungai' => $value[4],
                            'keterangan ' => $value[5]
                        );
                        DB::table('infra_air_bersih')->insert($arr);
                    } else {
                        $arr = array(
                            'jumlah_penduduk' => $value[1],
                            'sab' => $value[2],
                            'kapasitas_lt_dk' => $value[3],
                            'sungai' => $value[4],
                            'keterangan ' => $value[5]
                        );
                        DB::table('infra_air_bersih')->where('location',$key)->update($arr);
                    }
                    $c = "success";
                } catch (\Exception $ex) {
                    $c = $ex;
                }
            }
        };
        return $a;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function desa(Request $req)
    {
        $desa_data = new TMDesa();
        $data_lis = new DataAirBersih();
        $data_desa = $desa_data->where('id_kec','=',$req->id_kec)->get();
        $resut_desa_listrik = array(new \ArrayObject());
        $nama_desa = array();
        foreach ($data_desa as $d_desa){
            $data_air = $data_lis->where('location','=',$d_desa['nama'])->get();
            $desa_air = array($d_desa['nama']=>new \ArrayObject());
            foreach ($data_air as $d_air){
                $desa_air[$d_desa['nama']]= array(
                    "jumlah_penduduk"=>$d_air['jumlah_penduduk'],
                    "sab"=>$d_air['sab'],
                    "kapasitas_lt_dk"=>$d_air['kapasitas_lt_dk'],
                    "sungai"=>$d_air['sungai'],
                    "keterangan"=>$d_air['keterangan'],
                );
            }
            array_push($nama_desa,$desa_air);
        }
        $resut_desa_listrik=$nama_desa;
        return ['data_desa'=>$data_desa, 'data_desa_air'=>$resut_desa_listrik];
    }
}
