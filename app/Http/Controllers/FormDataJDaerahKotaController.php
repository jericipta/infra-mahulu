<?php

namespace App\Http\Controllers;

use App\Models\FormDataJDaerahKota;
use App\Models\MasterDinas;
use Illuminate\Http\Request;

class FormDataJDaerahKotaController extends Controller
{
    const VIEW_PATH = 'infra.form-data.form-data-jalan-daerah-kota.';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $formdata = FormDataJDaerahKota::joinDinas();
        return view(self::VIEW_PATH.'index',['formdata'=>$formdata]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dinas = MasterDinas::pluck('name', 'id');
        return view(self::VIEW_PATH.'create',['dinas' => $dinas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('jalan');
       
        //Display File Name
        echo 'File Name: '.$file->getClientOriginalName();
        echo '<br>';

        //Display File Extension
        echo 'File Extension: '.$file->getClientOriginalExtension();
        echo '<br>';

        //Display File Real Path
        echo 'File Real Path: '.$file->getRealPath();
        echo '<br>';

        //Display File Size
        echo 'File Size: '.$file->getSize();
        echo '<br>';

        //Display File Mime Type
        echo 'File Mime Type: '.$file->getMimeType();

        //Move Uploaded File
        $destinationPath = 'uploads\form-data\jalan-daerah-kota';
        $file->move($destinationPath,$file->getClientOriginalName());
        

        $fskpd = FormDataJDaerahKota::create([
            'dinas' => $request->input('id-dinas'),
            // 'tanggal_proses' => $request->input('tgl-proses'),
            'status' => 1,
            'lokasi' => $destinationPath . '\\'. $file->getClientOriginalName()
        ]);
        $fskpd->save();
        return redirect('form-data-jalan-kota');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormDataJDaerahKota  $formDataJDaerahKota
     * @return \Illuminate\Http\Response
     */
    public function show(FormDataJDaerahKota $formDataJDaerahKota)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormDataJDaerahKota  $formDataJDaerahKota
     * @return \Illuminate\Http\Response
     */
    public function edit(FormDataJDaerahKota $formDataJDaerahKota)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormDataJDaerahKota  $formDataJDaerahKota
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormDataJDaerahKota $formDataJDaerahKota)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormDataJDaerahKota  $formDataJDaerahKota
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormDataJDaerahKota $formDataJDaerahKota)
    {
        //
    }

    public function delete(Request $request)
    {
        //Delete the instrument

        // $skpd = FormDataJSKPD::find($request->input('id'));
        $jDaerahKota = FormDataJDaerahKota::find( $request->input('id'));
        $jDaerahKota->delete();

        // redirect
        return redirect('form-data-jalan-kota');
    }
}
