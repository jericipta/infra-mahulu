<?php

namespace App\Http\Controllers;

use App\Models\MasterDinas;
use Illuminate\Http\Request;

class MasterDinasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $dinas = MasterDinas::all();
        return view('infra.master.dinas.index',['dinas'=>$dinas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dinas = MasterDinas::create([
            'name' => $request->input('name'),
        ]);
        $dinas->save();
        return redirect('form-master-dinas');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MasterDinas  $masterDinas
     * @return \Illuminate\Http\Response
     */
    public function show(MasterDinas $masterDinas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MasterDinas  $masterDinas
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterDinas $masterDinas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MasterDinas  $masterDinas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MasterDinas  $masterDinas
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterDinas $masterDinas)
    {
        //
    }

    public function delete(Request $request)
    {
        $dinas = MasterDinas::find($request->input('id'));
        $dinas->delete();
        return redirect('form-master-dinas');
    }
}
