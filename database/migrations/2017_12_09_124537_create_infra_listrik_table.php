<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfraListrikTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infra_listrik', function (Blueprint $table) {
            $table->increments('id');
            $table->string('location')->nullable();
            $table->foreign('bidang')->nullable();
            $table->boolean('pltd')->default(false);
            $table->boolean('genset')->default(false);
            $table->boolean('plts')->default(false);
            $table->boolean('plts_komunal')->default(false);
            $table->text('listrik')->nullable();
            $table->string('jam_operasional')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('infra_listrik', function (Blueprint $table) {
            //
        });
    }
}
