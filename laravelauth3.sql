-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2018 at 04:04 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelauth3`
--

-- --------------------------------------------------------

--
-- Table structure for table `activations`
--

CREATE TABLE `activations` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `akses`
--

CREATE TABLE `akses` (
  `id` int(2) NOT NULL,
  `name` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akses`
--

INSERT INTO `akses` (`id`, `name`, `updated_at`, `created_at`) VALUES
(1, 'sample update', '2017-12-20 05:29:00', '2017-12-20 04:54:19'),
(2, 'sample2', '2017-12-20 05:06:13', '2017-12-20 05:06:13');

-- --------------------------------------------------------

--
-- Table structure for table `infra_air_bersih`
--

CREATE TABLE `infra_air_bersih` (
  `id` int(11) NOT NULL,
  `location` varchar(191) DEFAULT NULL,
  `jumlah_penduduk` varchar(10) NOT NULL DEFAULT '0',
  `sab` tinyint(1) NOT NULL DEFAULT '0',
  `kapasitas_lt_dk` varchar(100) DEFAULT NULL,
  `sungai` varchar(191) DEFAULT NULL,
  `keterangan` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `infra_air_bersih`
--

INSERT INTO `infra_air_bersih` (`id`, `location`, `jumlah_penduduk`, `sab`, `kapasitas_lt_dk`, `sungai`, `keterangan`) VALUES
(1, 'Noha Silat', '231', 1, '200Lt', 'ada', 'Air Bersih');

-- --------------------------------------------------------

--
-- Table structure for table `infra_listrik`
--

CREATE TABLE `infra_listrik` (
  `id` int(10) UNSIGNED NOT NULL,
  `bidang_urusan` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pltd` tinyint(1) NOT NULL DEFAULT '0',
  `genset` tinyint(1) NOT NULL DEFAULT '0',
  `plts` tinyint(1) NOT NULL DEFAULT '0',
  `plts_komunal` tinyint(1) NOT NULL DEFAULT '0',
  `listrik` tinyint(1) DEFAULT NULL,
  `jam_operasional` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `infra_listrik`
--

INSERT INTO `infra_listrik` (`id`, `bidang_urusan`, `location`, `pltd`, `genset`, `plts`, `plts_komunal`, `listrik`, `jam_operasional`, `keterangan`, `created_at`, `updated_at`) VALUES
(580, ' sample 1', 'Long Pakaq', 1, 1, 1, 1, 0, ' 12', 'update baru', NULL, NULL),
(581, ' ', 'Long Pakaq Baru', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(582, ' ', 'Long Lunuk', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(583, ' ', 'Long Lunuk Baru', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(584, ' ', 'Lirung Ubing', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(585, ' ', 'Long Pahangai I', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(586, ' ', 'Long Pahangai II', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(587, ' ', 'Liu Mulang', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(588, ' ', 'Datah Naha', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(589, ' ', 'Long Tuyoq', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(590, ' ', 'Naha Aruq\n', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(591, ' ', 'Delang Kerohong\n', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(592, ' ', 'Long Isun', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(593, ' tambah update', 'Long Apari', 1, 1, 1, 1, 1, '12 update', 'tambah baru', NULL, NULL),
(594, ' ', 'Noha Silat', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(595, ' ', 'Noha Tivab', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(596, ' ', 'Tiong Ohang', 0, 0, 0, 0, 0, ' tambah lagi', ' ', NULL, NULL),
(597, ' ', 'Tiong Bu\'u', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(598, ' ', 'Noha Buan', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(599, ' ', 'Long Penaneh I', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(600, ' ', 'Long Penaneh II', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(601, ' ', 'Long Penaneh III', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL),
(602, ' ', 'Long Kerioq', 0, 0, 0, 0, 0, ' ', ' ', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_15_105324_create_roles_table', 1),
(4, '2016_01_15_114412_create_role_user_table', 1),
(5, '2016_01_26_115212_create_permissions_table', 1),
(6, '2016_01_26_115523_create_permission_role_table', 1),
(7, '2016_02_09_132439_create_permission_user_table', 1),
(8, '2017_03_09_082449_create_social_logins_table', 1),
(9, '2017_03_09_082526_create_activations_table', 1),
(10, '2017_03_20_213554_create_themes_table', 1),
(11, '2017_03_21_042918_create_profiles_table', 1),
(12, '2017_05_20_095210_create_tasks_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `slug`, `description`, `model`, `created_at`, `updated_at`) VALUES
(1, 'Can View Users', 'view.users', 'Can view users', 'Permission', '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(2, 'Can Create Users', 'create.users', 'Can create new users', 'Permission', '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(3, 'Can Edit Users', 'edit.users', 'Can edit users', 'Permission', '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(4, 'Can Delete Users', 'delete.users', 'Can delete users', 'Permission', '2017-12-09 02:58:53', '2017-12-09 02:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(2, 2, 1, '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(3, 3, 1, '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(4, 4, 1, '2017-12-09 02:58:53', '2017-12-09 02:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `bagian` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telepon` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme_id` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `id_skpd` int(11) NOT NULL,
  `jabatan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_profile_bg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '/images/default-user-bg.png',
  `tanggal_lahir` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `nip`, `user_id`, `bagian`, `no_telepon`, `theme_id`, `id_skpd`, `jabatan`, `user_profile_bg`, `tanggal_lahir`, `created_at`, `updated_at`) VALUES
(1, '', 1, NULL, '', 1, 0, '', '/images/default-user-bg.png', '0000-00-00 00:00:00', '2017-12-09 02:58:54', '2017-12-09 02:58:54'),
(2, '', 2, NULL, '', 1, 0, '', '/images/default-user-bg.png', '0000-00-00 00:00:00', '2017-12-09 02:58:54', '2017-12-09 02:58:54'),
(3, '45678900986', 7, 'Ketua', '0987657', 1, 2, 'Tes', '/images/default-user-bg.png', '2017-12-12 00:00:00', '2017-12-24 12:23:51', '2017-12-24 12:23:51'),
(4, '123123213', 8, '123', '123123123', 1, 2, '123', '/images/default-user-bg.png', '2017-08-12 00:00:00', '2017-12-24 12:50:34', '2017-12-24 12:50:34'),
(5, '123123213', 9, NULL, '010109922321', 1, 2, '123123', '/images/default-user-bg.png', '2017-06-12 00:00:00', '2017-12-24 14:53:14', '2017-12-24 14:53:14'),
(6, '12345', 10, 'bos', '0812', 1, 2, 'bos', '/images/default-user-bg.png', '2019-07-01 00:00:00', '2018-01-07 07:25:13', '2018-01-07 07:25:13'),
(7, '001123321', 11, 'Biro Perlengkapan', '081123321123', 1, 2, 'Kepala', '/images/default-user-bg.png', '1984-03-01 00:00:00', '2018-01-07 08:08:54', '2018-01-07 08:08:54');

-- --------------------------------------------------------

--
-- Table structure for table `proyek`
--

CREATE TABLE `proyek` (
  `nama_proyek` varchar(255) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `panjang` varchar(255) DEFAULT NULL,
  `sumber_anggaran` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `id_parent` varchar(100) NOT NULL DEFAULT '""',
  `tahun_angaran` varchar(4) DEFAULT NULL,
  `jumlah_anggaran` varchar(255) DEFAULT NULL,
  `kord_x1` varchar(255) DEFAULT NULL,
  `kord_x2` varchar(255) DEFAULT NULL,
  `kord_y1` varchar(255) DEFAULT NULL,
  `kord_y2` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `proyek`
--

INSERT INTO `proyek` (`nama_proyek`, `lokasi`, `panjang`, `sumber_anggaran`, `id`, `id_parent`, `tahun_angaran`, `jumlah_anggaran`, `kord_x1`, `kord_x2`, `kord_y1`, `kord_y2`, `keterangan`, `created_at`, `updated_at`) VALUES
('Jalan Baru Riau', 'Riau', '12km', 'APBD', 71, '1515633733h1NaXFe5Zq', '2016', '3120000', '12', '12', '12', '12', 'Lunas', '2018-01-11 01:22:13', '2018-01-11 01:22:13'),
('Jalan Baru Riau', 'Riau', '12km', 'APBD', 72, '1515633733h1NaXFe5Zq', '2016', '3120000', '12', '12', '12', '12', 'Lunas', '2018-01-11 01:22:13', '2018-01-11 01:22:13'),
('Jalan Baru Riau', 'Riau', '12km', 'APBD', 73, '1515633733h1NaXFe5Zq', '2016', '3120000', '12', '12', '12', '12', 'Lunas', '2018-01-11 01:22:13', '2018-01-11 01:22:13'),
('Jalan Baru Riau', 'Riau', '12km', 'APBD', 74, '1515633733h1NaXFe5Zq', '2016', '3120000', '12', '12', '12', '12', 'Lunas', '2018-01-11 01:22:13', '2018-01-11 01:22:13'),
('Jalan Baru Riau', 'Riau', '12km', 'APBD', 75, '1515633733h1NaXFe5Zq', '2016', '3120000', '12', '12', '12', '12', 'Lunas', '2018-01-11 01:22:13', '2018-01-11 01:22:13'),
('Jalan Baru Riau', 'Riau', '12km', 'APBD', 76, '1515633733h1NaXFe5Zq', '2016', '3120000', '12', '12', '12', '12', 'Lunas', '2018-01-11 01:22:13', '2018-01-11 01:22:13'),
('Jalan Baru Riau', 'Riau', '12km', 'APBD', 77, '1515633733h1NaXFe5Zq', '2016', '3120000', '12', '12', '12', '12', 'Lunas', '2018-01-11 01:22:13', '2018-01-11 01:22:13');

-- --------------------------------------------------------

--
-- Table structure for table `proyek_p`
--

CREATE TABLE `proyek_p` (
  `id` int(11) NOT NULL,
  `nama_file` varchar(200) DEFAULT NULL,
  `nama_user` varchar(200) DEFAULT NULL,
  `path_file` varchar(200) DEFAULT NULL,
  `id_proyek` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `proyek_p`
--

INSERT INTO `proyek_p` (`id`, `nama_file`, `nama_user`, `path_file`, `id_proyek`, `created_at`, `updated_at`) VALUES
(1, 'contoh.xlsx', 'reta46 123', '../public/excel_file/820contoh.xlsx', '1515633733h1NaXFe5Zq', '2018-01-11 01:22:13', '2018-01-11 01:22:13');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `level`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', 'Admin Role', 5, '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(2, 'User', 'user', 'User Role', 1, '2017-12-09 02:58:53', '2017-12-09 02:58:53'),
(3, 'Unverified', 'unverified', 'Unverified Role', 0, '2017-12-09 02:58:53', '2017-12-09 02:58:53');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(2, 2, 2, '2017-12-09 02:58:54', '2017-12-09 02:58:54'),
(4, 3, 8, '2017-12-24 12:50:34', '2017-12-24 12:50:34'),
(5, 1, 1, '2017-12-24 14:37:04', '2017-12-24 14:37:04'),
(6, 1, 9, '2017-12-24 14:53:14', '2017-12-24 14:53:14'),
(8, 1, 10, '2018-01-07 07:25:13', '2018-01-07 07:25:13'),
(9, 2, 11, '2018-01-07 08:08:54', '2018-01-07 08:08:54'),
(10, 2, 7, '2018-01-10 19:35:20', '2018-01-10 19:35:20');

-- --------------------------------------------------------

--
-- Table structure for table `skpd`
--

CREATE TABLE `skpd` (
  `id` int(11) NOT NULL,
  `nama_skpd` varchar(100) NOT NULL,
  `alamat_skpd` text NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `skpd`
--

INSERT INTO `skpd` (`id`, `nama_skpd`, `alamat_skpd`, `updated_at`, `created_at`) VALUES
(2, 'Dinas PU', 'Jl. Thamrin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'DINAS PERUMAHAN', 'Jakarta', '2017-12-25 07:53:26', '2017-12-25 07:53:26');

-- --------------------------------------------------------

--
-- Table structure for table `social_logins`
--

CREATE TABLE `social_logins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `provider` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `social_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `telekomunikasi`
--

CREATE TABLE `telekomunikasi` (
  `id` int(11) NOT NULL,
  `lokasi` varchar(255) NOT NULL,
  `bidang_urusan` varchar(100) DEFAULT NULL,
  `akses_telekomunikasi` tinyint(4) NOT NULL DEFAULT '0',
  `ket` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `telekomunikasi`
--

INSERT INTO `telekomunikasi` (`id`, `lokasi`, `bidang_urusan`, `akses_telekomunikasi`, `ket`) VALUES
(1, 'Long Bagun Ulu', 'undefined', 1, 'kajss'),
(2, 'Long Bagun Ilir', 'undefined', 1, 'undefined'),
(3, 'Batoq Kelo', 'kajsk', 1, 'ask'),
(4, 'Batu Majang', 'undefined', 1, 'undefined'),
(5, 'Ujoh Bilang', 'undefined', 1, 'undefined'),
(6, 'Long Melaham', 'undefined', 1, 'undefined'),
(7, 'Mamahak Ulu', 'undefined', 1, 'undefined'),
(8, 'Mamahak Besar', 'undefined', 1, 'undefined'),
(9, 'Rukun Damai', 'undefined', 1, 'undefined'),
(10, 'Long Merah', 'undefined', 1, 'undefined'),
(11, 'Long Hurai', 'undefined', 1, 'undefined'),
(12, 'Long Apari', 'undefined', 1, 'undefined'),
(13, 'Noha Silat', 'Jatim', 1, 'Ada'),
(14, 'Noha Tivab', 'undefined', 1, 'undefined'),
(15, 'Tiong Ohang', 'undefined', 1, 'undefined'),
(16, 'Tiong Bu\'u', 'undefined', 1, 'undefined'),
(17, 'Noha Buan', 'undefined', 1, 'undefined'),
(18, 'Long Penaneh I', 'undefined', 1, 'undefined'),
(19, 'Long Penaneh II', 'undefined', 1, 'undefined'),
(20, 'Long Penaneh III', 'undefined', 1, 'undefined'),
(21, 'Long Kerioq', 'undefined', 1, 'undefined');

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE `themes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `taggable_id` int(10) UNSIGNED NOT NULL,
  `taggable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `name`, `link`, `notes`, `status`, `taggable_id`, `taggable_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Default', 'material.min.css', NULL, 1, 1, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(2, 'Amber / Blue', 'material.amber-blue.min.css', NULL, 1, 2, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(3, 'Amber / Cyan', 'material.amber-cyan.min.css', NULL, 1, 3, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(4, 'Amber / Deep Orange', 'material.amber-deep_orange.min.css', NULL, 1, 4, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(5, 'Amber / Deep Purple', 'material.amber-deep_purple.min.css', NULL, 1, 5, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(6, 'Amber / Green', 'material.amber-green.min.css', NULL, 1, 6, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(7, 'Amber / Indigo', 'material.amber-indigo.min.css', NULL, 1, 7, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(8, 'Amber / Light Blue', 'material.amber-light_blue.min.css', NULL, 1, 8, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(9, 'Amber / Light Green', 'material.amber-light_green.min.css', NULL, 1, 9, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(10, 'Amber / Lime', 'material.amber-lime.min.css', NULL, 1, 10, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(11, 'Amber / Orange', 'material.amber-orange.min.css', NULL, 1, 11, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(12, 'Amber / Pink', 'material.amber-pink.min.css', NULL, 1, 12, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(13, 'Amber / Purple', 'material.amber-purple.min.css', NULL, 1, 13, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(14, 'Amber / Red', 'material.amber-red.min.css', NULL, 1, 14, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(15, 'Amber / Teal', 'material.amber-teal.min.css', NULL, 1, 15, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(16, 'Amber / Yellow', 'material.amber-yellow.min.css', NULL, 1, 16, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(17, 'Blue Grey / Amber', 'material.blue_grey-amber.min.css', NULL, 1, 17, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(18, 'Blue Grey / Blue', 'material.blue_grey-blue.min.css', NULL, 1, 18, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(19, 'Blue Grey / Cyan', 'material.blue_grey-cyan.min.css', NULL, 1, 19, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(20, 'Blue Grey / Deep Orange', 'material.blue_grey-deep_orange.min.css', NULL, 1, 20, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(21, 'Blue Grey / Deep Purple', 'material.blue_grey-deep_purple.min.css', NULL, 1, 21, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(22, 'Blue Grey / Green', 'material.blue_grey-green.min.css', NULL, 1, 22, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(23, 'Blue Grey / Indigo', 'material.blue_grey-indigo.min.css', NULL, 1, 23, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(24, 'Blue Grey / Light Blue', 'material.blue_grey-light_blue.min.css', NULL, 1, 24, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(25, 'Blue Grey / Light Green', 'material.blue_grey-light_green.min.css', NULL, 1, 25, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(26, 'Blue Grey / Lime', 'material.blue_grey-lime.min.css', NULL, 1, 26, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(27, 'Blue Grey / Orange', 'material.blue_grey-orange.min.css', NULL, 1, 27, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(28, 'Blue Grey / Pink', 'material.blue_grey-pink.min.css', NULL, 1, 28, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(29, 'Blue Grey / Purple', 'material.blue_grey-purple.min.css', NULL, 1, 29, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(30, 'Blue Grey / Red', 'material.blue_grey-red.min.css', NULL, 1, 30, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(31, 'Blue Grey / Teal', 'material.blue_grey-teal.min.css', NULL, 1, 31, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(32, 'Blue Grey / Yellow', 'material.blue_grey-yellow.min.css', NULL, 1, 32, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(33, 'Blue / Amber', 'material.blue-amber.min.css', NULL, 1, 33, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(34, 'Blue / Cyan', 'material.blue-cyan.min.css', NULL, 1, 34, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(35, 'Blue / Deep Orange', 'material.blue-deep_orange.min.css', NULL, 1, 35, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(36, 'Blue / Deep Purple', 'material.blue-deep_purple.min.css', NULL, 1, 36, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(37, 'Blue / Green', 'material.blue-green.min.css', NULL, 1, 37, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(38, 'Blue / Indigo', 'material.blue-indigo.min.css', NULL, 1, 38, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(39, 'Blue / Light Blue', 'material.blue-light_blue.min.css', NULL, 1, 39, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(40, 'Blue / Light Green', 'material.blue-light_green.min.css', NULL, 1, 40, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(41, 'Blue / Lime', 'material.blue-lime.min.css', NULL, 1, 41, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(42, 'Blue / Orange', 'material.blue-orange.min.css', NULL, 1, 42, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(43, 'Blue / Pink', 'material.blue-pink.min.css', NULL, 1, 43, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(44, 'Blue / Purple', 'material.blue-purple.min.css', NULL, 1, 44, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(45, 'Blue / Red', 'material.blue-red.min.css', NULL, 1, 45, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(46, 'Blue / Teal', 'material.blue-teal.min.css', NULL, 1, 46, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(47, 'Blue / Yellow', 'material.blue-yellow.min.css', NULL, 1, 47, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(48, 'Brown / Amber', 'material.brown-amber.min.css', NULL, 1, 48, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(49, 'Brown / Blue', 'material.brown-blue.min.css', NULL, 1, 49, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(50, 'Brown / Cyan', 'material.brown-cyan.min.css', NULL, 1, 50, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(51, 'Brown / Deep Orange', 'material.brown-deep_orange.min.css', NULL, 1, 51, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(52, 'Brown / Deep Purple', 'material.brown-deep_purple.min.css', NULL, 1, 52, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(53, 'Brown / Green', 'material.brown-green.min.css', NULL, 1, 53, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(54, 'Brown / Indigo', 'material.brown-indigo.min.css', NULL, 1, 54, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(55, 'Brown / Light Blue', 'material.brown-light_blue.min.css', NULL, 1, 55, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(56, 'Brown / Light Green', 'material.brown-light_green.min.css', NULL, 1, 56, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(57, 'Brown / Lime', 'material.brown-lime.min.css', NULL, 1, 57, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(58, 'Brown / Orange', 'material.brown-orange.min.css', NULL, 1, 58, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(59, 'Brown / Pink', 'material.brown-pink.min.css', NULL, 1, 59, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(60, 'Brown / Purple', 'material.brown-purple.min.css', NULL, 1, 60, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(61, 'Brown / Red', 'material.brown-red.min.css', NULL, 1, 61, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(62, 'Brown / Teal', 'material.brown-teal.min.css', NULL, 1, 62, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(63, 'Brown / Yellow', 'material.brown-yellow.min.css', NULL, 1, 63, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(64, 'Cyan / Amber', 'material.cyan-amber.min.css', NULL, 1, 64, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(65, 'Cyan / Blue', 'material.cyan-blue.min.css', NULL, 1, 65, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(66, 'Cyan / Deep Orange', 'material.cyan-deep_orange.min.css', NULL, 1, 66, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(67, 'Cyan / Deep Purple', 'material.cyan-deep_purple.min.css', NULL, 1, 67, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(68, 'Cyan / Green', 'material.cyan-green.min.css', NULL, 1, 68, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(69, 'Cyan / Indigo', 'material.cyan-indigo.min.css', NULL, 1, 69, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(70, 'Cyan / Light Blue', 'material.cyan-light_blue.min.css', NULL, 1, 70, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(71, 'Cyan / Light Green', 'material.cyan-light_green.min.css', NULL, 1, 71, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(72, 'Cyan / Lime', 'material.cyan-lime.min.css', NULL, 1, 72, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(73, 'Cyan / Orange', 'material.cyan-orange.min.css', NULL, 1, 73, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(74, 'Cyan / Pink', 'material.cyan-pink.min.css', NULL, 1, 74, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(75, 'Cyan / Purple', 'material.cyan-purple.min.css', NULL, 1, 75, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(76, 'Cyan / Red', 'material.cyan-red.min.css', NULL, 1, 76, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(77, 'Cyan / Teal', 'material.cyan-teal.min.css', NULL, 1, 77, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(78, 'Cyan / Yellow', 'material.cyan-yellow.min.css', NULL, 1, 78, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(79, 'Deep Orange / Amber', 'material.deep_orange-amber.min.css', NULL, 1, 79, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(80, 'Deep Orange / Blue', 'material.deep_orange-blue.min.css', NULL, 1, 80, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(81, 'Deep Orange / Cyan', 'material.deep_orange-cyan.min.css', NULL, 1, 81, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(82, 'Deep Orange / Deep Purple', 'material.deep_orange-deep_purple.min.css', NULL, 1, 82, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(83, 'Deep Orange / Green', 'material.deep_orange-green.min.css', NULL, 1, 83, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(84, 'Deep Orange / Indigo', 'material.deep_orange-indigo.min.css', NULL, 1, 84, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(85, 'Deep Orange / Light Blue', 'material.deep_orange-light_blue.min.css', NULL, 1, 85, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(86, 'Deep Orange / Light Green', 'material.deep_orange-light_green.min.css', NULL, 1, 86, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(87, 'Deep Orange / Lime', 'material.deep_orange-lime.min.css', NULL, 1, 87, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(88, 'Deep Orange / Orange', 'material.deep_orange-orange.min.css', NULL, 1, 88, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(89, 'Deep Orange / Pink', 'material.deep_orange-pink.min.css', NULL, 1, 89, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(90, 'Deep Orange / Purple', 'material.deep_orange-purple.min.css', NULL, 1, 90, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(91, 'Deep Orange / Red', 'material.deep_orange-red.min.css', NULL, 1, 91, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(92, 'Deep Orange / Teal', 'material.deep_orange-teal.min.css', NULL, 1, 92, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(93, 'Deep Orange / Yellow', 'material.deep_orange-yellow.min.css', NULL, 1, 93, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(94, 'Deep Purple / Amber', 'material.deep_purple-amber.min.css', NULL, 1, 94, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(95, 'Deep Purple / Blue', 'material.deep_purple-blue.min.css', NULL, 1, 95, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(96, 'Deep Purple / Cyan', 'material.deep_purple-cyan.min.css', NULL, 1, 96, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(97, 'Deep Purple / Deep Orange', 'material.deep_purple-deep_orange.min.css', NULL, 1, 97, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(98, 'Deep Purple / Green', 'material.deep_purple-green.min.css', NULL, 1, 98, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(99, 'Yellow / Teal', 'material.yellow-teal.min.css', NULL, 1, 99, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(100, 'Yellow / Red', 'material.yellow-red.min.css', NULL, 1, 100, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(101, 'Yellow / Orange', 'material.yellow-orange.min.css', NULL, 1, 101, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(102, 'Yellow / Pink', 'material.yellow-pink.min.css', NULL, 1, 102, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(103, 'Yellow / Purple', 'material.yellow-purple.min.css', NULL, 1, 103, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(104, 'Yellow / Lime', 'material.yellow-lime.min.css', NULL, 1, 104, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(105, 'Yellow / Light Green', 'material.yellow-light_green.min.css', NULL, 1, 105, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(106, 'Yellow / Indigo', 'material.yellow-indigo.min.css', NULL, 1, 106, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(107, 'Yellow / Light Blue', 'material.yellow-light_blue.min.css', NULL, 1, 107, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(108, 'Yellow / Green', 'material.yellow-green.min.css', NULL, 1, 108, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(109, 'Yellow / Deep Purple', 'material.yellow-deep_purple.min.css', NULL, 1, 109, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(110, 'Yellow / Deep Orange', 'material.yellow-deep_orange.min.css', NULL, 1, 110, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(111, 'Yellow / Cyan', 'material.yellow-cyan.min.css', NULL, 1, 111, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(112, 'Yellow / Blue', 'material.yellow-blue.min.css', NULL, 1, 112, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(113, 'Yellow / Amber', 'material.yellow-amber.min.css', NULL, 1, 113, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(114, 'Teal / Yellow', 'material.teal-yellow.min.css', NULL, 1, 114, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(115, 'Teal / Red', 'material.teal-red.min.css', NULL, 1, 115, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(116, 'Teal / Purple', 'material.teal-purple.min.css', NULL, 1, 116, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(117, 'Teal / Pink', 'material.teal-pink.min.css', NULL, 1, 117, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(118, 'Teal / Orange', 'material.teal-orange.min.css', NULL, 1, 118, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(119, 'Teal / Lime', 'material.teal-lime.min.css', NULL, 1, 119, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(120, 'Teal / Light Green', 'material.teal-light_green.min.css', NULL, 1, 120, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(121, 'Teal / Light Blue', 'material.teal-light_blue.min.css', NULL, 1, 121, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(122, 'Teal / Indigo', 'material.teal-indigo.min.css', NULL, 1, 122, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(123, 'Teal / Green', 'material.teal-green.min.css', NULL, 1, 123, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(124, 'Teal / Deep Purple', 'material.teal-deep_purple.min.css', NULL, 1, 124, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(125, 'Teal / Deep Orange', 'material.teal-deep_orange.min.css', NULL, 1, 125, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(126, 'Teal / Cyan', 'material.teal-cyan.min.css', NULL, 1, 126, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(127, 'Teal / Blue', 'material.teal-blue.min.css', NULL, 1, 127, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(128, 'Teal / Amber', 'material.teal-amber.min.css', NULL, 1, 128, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(129, 'Red / Yellow', 'material.red-yellow.min.css', NULL, 1, 129, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(130, 'Red / Teal', 'material.red-teal.min.css', NULL, 1, 130, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(131, 'Red / Purple', 'material.red-purple.min.css', NULL, 1, 131, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(132, 'Red / Pink', 'material.red-pink.min.css', NULL, 1, 132, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(133, 'Red / Orange', 'material.red-orange.min.css', NULL, 1, 133, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(134, 'Red / Lime', 'material.red-lime.min.css', NULL, 1, 134, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(135, 'Red / Light Green', 'material.red-light_green.min.css', NULL, 1, 135, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(136, 'Red / Light Blue', 'material.red-light_blue.min.css', NULL, 1, 136, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(137, 'Red / Indigo', 'material.red-indigo.min.css', NULL, 1, 137, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(138, 'Red / Green', 'material.red-green.min.css', NULL, 1, 138, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(139, 'Red / Deep Purple', 'material.red-deep_purple.min.css', NULL, 1, 139, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(140, 'Red / Deep Orange', 'material.red-deep_orange.min.css', NULL, 1, 140, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(141, 'Red / Cyan', 'material.red-cyan.min.css', NULL, 1, 141, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(142, 'Red / Blue', 'material.red-blue.min.css', NULL, 1, 142, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(143, 'Red / Amber', 'material.red-amber.min.css', NULL, 1, 143, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(144, 'Purple / Yellow', 'material.purple-yellow.min.css', NULL, 1, 144, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(145, 'Purple / Teal', 'material.purple-teal.min.css', NULL, 1, 145, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(146, 'Purple / Pink', 'material.purple-pink.min.css', NULL, 1, 146, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(147, 'Purple / Orange', 'material.purple-orange.min.css', NULL, 1, 147, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(148, 'Purple / Lime', 'material.purple-lime.min.css', NULL, 1, 148, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(149, 'Purple / Light Green', 'material.purple-light_green.min.css', NULL, 1, 149, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(150, 'Purple / Light Blue', 'material.purple-light_blue.min.css', NULL, 1, 150, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(151, 'Purple / Indigo', 'material.purple-indigo.min.css', NULL, 1, 151, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(152, 'Purple / Green', 'material.purple-green.min.css', NULL, 1, 152, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(153, 'Purple / Deep Purple', 'material.purple-deep_purple.min.css', NULL, 1, 153, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(154, 'Purple / Deep Orange', 'material.purple-deep_orange.min.css', NULL, 1, 154, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(155, 'Purple / Cyan', 'material.purple-cyan.min.css', NULL, 1, 155, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(156, 'Purple / Blue', 'material.purple-blue.min.css', NULL, 1, 156, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(157, 'Purple / Amber', 'material.purple-amber.min.css', NULL, 1, 157, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(158, 'Pink / Yellow', 'material.pink-yellow.min.css', NULL, 1, 158, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(159, 'Pink / Teal', 'material.pink-teal.min.css', NULL, 1, 159, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(160, 'Pink / Red', 'material.pink-red.min.css', NULL, 1, 160, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(161, 'Pink / Purple', 'material.pink-purple.min.css', NULL, 1, 161, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(162, 'Pink / Orange', 'material.pink-orange.min.css', NULL, 1, 162, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(163, 'Pink / Lime', 'material.pink-lime.min.css', NULL, 1, 163, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(164, 'Pink / Light Green', 'material.pink-light_green.min.css', NULL, 1, 164, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(165, 'Pink / Light Blue', 'material.pink-light_blue.min.css', NULL, 1, 165, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(166, 'Pink / Indigo', 'material.pink-indigo.min.css', NULL, 1, 166, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(167, 'Pink / Green', 'material.pink-green.min.css', NULL, 1, 167, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(168, 'Pink / Deep Purple', 'material.pink-deep_purple.min.css', NULL, 1, 168, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(169, 'Pink / Deep Orange', 'material.pink-deep_orange.min.css', NULL, 1, 169, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(170, 'Pink / Cyan', 'material.pink-cyan.min.css', NULL, 1, 170, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(171, 'Pink / Blue', 'material.pink-blue.min.css', NULL, 1, 171, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(172, 'Pink / Amber', 'material.pink-amber.min.css', NULL, 1, 172, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:53', NULL),
(173, 'Orange / Yellow', 'material.orange-yellow.min.css', NULL, 1, 173, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(174, 'Orange / Teal', 'material.orange-teal.min.css', NULL, 1, 174, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(175, 'Orange / Red', 'material.orange-red.min.css', NULL, 1, 175, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(176, 'Orange / Purple', 'material.orange-purple.min.css', NULL, 1, 176, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(177, 'Orange / Pink', 'material.orange-pink.min.css', NULL, 1, 177, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(178, 'Orange / Lime', 'material.orange-lime.min.css', NULL, 1, 178, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(179, 'Orange / Light Green', 'material.orange-light_green.min.css', NULL, 1, 179, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(180, 'Orange / Light Blue', 'material.orange-light_blue.min.css', NULL, 1, 180, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(181, 'Orange / Indigo', 'material.orange-indigo.min.css', NULL, 1, 181, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(182, 'Orange / Green', 'material.orange-green.min.css', NULL, 1, 182, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(183, 'Orange / Deep Purple', 'material.orange-deep_purple.min.css', NULL, 1, 183, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(184, 'Orange / Deep Orange', 'material.orange-deep_orange.min.css', NULL, 1, 184, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(185, 'Orange / Cyan', 'material.orange-cyan.min.css', NULL, 1, 185, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(186, 'Orange / Amber', 'material.orange-amber.min.css', NULL, 1, 186, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(187, 'Orange / Blue', 'material.orange-blue.min.css', NULL, 1, 187, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(188, 'Lime / Yellow', 'material.lime-yellow.min.css', NULL, 1, 188, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(189, 'Lime / Teal', 'material.lime-teal.min.css', NULL, 1, 189, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(190, 'Lime / Red', 'material.lime-red.min.css', NULL, 1, 190, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(191, 'Lime / Purple', 'material.lime-purple.min.css', NULL, 1, 191, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(192, 'Lime / Pink', 'material.lime-pink.min.css', NULL, 1, 192, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(193, 'Lime / Orange', 'material.lime-orange.min.css', NULL, 1, 193, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(194, 'Lime / Light Green', 'material.lime-light_green.min.css', NULL, 1, 194, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(195, 'Lime / Light Blue', 'material.lime-light_blue.min.css', NULL, 1, 195, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(196, 'Lime / Indigo', 'material.lime-indigo.min.css', NULL, 1, 196, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(197, 'Lime / Green', 'material.lime-green.min.css', NULL, 1, 197, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(198, 'Lime / Deep Orange', 'material.lime-deep_orange.min.css', NULL, 1, 198, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(199, 'Lime / Deep Purple', 'material.lime-deep_purple.min.css', NULL, 1, 199, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(200, 'Lime / Cyan', 'material.lime-cyan.min.css', NULL, 1, 200, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(201, 'Lime / Blue', 'material.lime-blue.min.css', NULL, 1, 201, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(202, 'Lime / Amber', 'material.lime-amber.min.css', NULL, 1, 202, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(203, 'Light Green / Yellow', 'material.light_green-yellow.min.css', NULL, 1, 203, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(204, 'Light Green / Teal', 'material.light_green-teal.min.css', NULL, 1, 204, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(205, 'Light Green / Red', 'material.light_green-red.min.css', NULL, 1, 205, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(206, 'Light Green / Purple', 'material.light_green-purple.min.css', NULL, 1, 206, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(207, 'Light Green / Pink', 'material.light_green-pink.min.css', NULL, 1, 207, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(208, 'Light Green / Orange', 'material.light_green-orange.min.css', NULL, 1, 208, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(209, 'Light Green / Lime', 'material.light_green-lime.min.css', NULL, 1, 209, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(210, 'Light Green / Light Blue', 'material.light_green-light_blue.min.css', NULL, 1, 210, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(211, 'Light Green / Indigo', 'material.light_green-indigo.min.css', NULL, 1, 211, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(212, 'Light Green / Green', 'material.light_green-green.min.css', NULL, 1, 212, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(213, 'Light Green / Deep Purple', 'material.light_green-deep_purple.min.css', NULL, 1, 213, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(214, 'Light Green / Deep Orange', 'material.light_green-deep_orange.min.css', NULL, 1, 214, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(215, 'Light Green / Cyan', 'material.light_green-cyan.min.css', NULL, 1, 215, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(216, 'Light Green / Blue', 'material.light_green-blue.min.css', NULL, 1, 216, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(217, 'Light Green / Amber', 'material.light_green-amber.min.css', NULL, 1, 217, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(218, 'Light Blue / Yellow', 'material.light_blue-yellow.min.css', NULL, 1, 218, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(219, 'Light Blue / Teal', 'material.light_blue-teal.min.css', NULL, 1, 219, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(220, 'Light Blue / Red', 'material.light_blue-red.min.css', NULL, 1, 220, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(221, 'Light Blue / Purple', 'material.light_blue-purple.min.css', NULL, 1, 221, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(222, 'Light Blue / Pink', 'material.light_blue-pink.min.css', NULL, 1, 222, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(223, 'Light Blue / Orange', 'material.light_blue-orange.min.css', NULL, 1, 223, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(224, 'Light Blue / Lime', 'material.light_blue-lime.min.css', NULL, 1, 224, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(225, 'Light Blue / Light Green', 'material.light_blue-light_green.min.css', NULL, 1, 225, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(226, 'Light Blue / Indigo', 'material.light_blue-indigo.min.css', NULL, 1, 226, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(227, 'Light Blue / Green', 'material.light_blue-green.min.css', NULL, 1, 227, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(228, 'Light Blue / Deep Purple', 'material.light_blue-deep_purple.min.css', NULL, 1, 228, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(229, 'Light Blue / Deep Orange', 'material.light_blue-deep_orange.min.css', NULL, 1, 229, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(230, 'Light Blue / Cyan', 'material.light_blue-cyan.min.css', NULL, 1, 230, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(231, 'Light Blue / Blue', 'material.light_blue-blue.min.css', NULL, 1, 231, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(232, 'Light Blue / Amber', 'material.light_blue-amber.min.css', NULL, 1, 232, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(233, 'Indigo / Yellow', 'material.indigo-yellow.min.css', NULL, 1, 233, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(234, 'Indigo / Teal', 'material.indigo-teal.min.css', NULL, 1, 234, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(235, 'Indigo / Red', 'material.indigo-red.min.css', NULL, 1, 235, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(236, 'Indigo / Purple', 'material.indigo-purple.min.css', NULL, 1, 236, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(237, 'Indigo / Pink', 'material.indigo-pink.min.css', NULL, 1, 237, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(238, 'Indigo / Orange', 'material.indigo-orange.min.css', NULL, 1, 238, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(239, 'Indigo / Lime', 'material.indigo-lime.min.css', NULL, 1, 239, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(240, 'Indigo / Light Green', 'material.indigo-light_green.min.css', NULL, 1, 240, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(241, 'Indigo / Light Blue', 'material.indigo-light_blue.min.css', NULL, 1, 241, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(242, 'Indigo / Green', 'material.indigo-green.min.css', NULL, 1, 242, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(243, 'Indigo / Deep Purple', 'material.indigo-deep_purple.min.css', NULL, 1, 243, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(244, 'Indigo / Deep Orange', 'material.indigo-deep_orange.min.css', NULL, 1, 244, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(245, 'Indigo / Cyan', 'material.indigo-cyan.min.css', NULL, 1, 245, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(246, 'Indigo / Blue', 'material.indigo-blue.min.css', NULL, 1, 246, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(247, 'Indigo / Amber', 'material.indigo-amber.min.css', NULL, 1, 247, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(248, 'Grey / Yellow', 'material.grey-yellow.min.css', NULL, 1, 248, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(249, 'Grey / Teal', 'material.grey-teal.min.css', NULL, 1, 249, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(250, 'Grey / Red', 'material.grey-red.min.css', NULL, 1, 250, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(251, 'Grey / Purple', 'material.grey-purple.min.css', NULL, 1, 251, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(252, 'Grey / Pink', 'material.grey-pink.min.css', NULL, 1, 252, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(253, 'Grey / Orange', 'material.grey-orange.min.css', NULL, 1, 253, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(254, 'Grey / Lime', 'material.grey-lime.min.css', NULL, 1, 254, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(255, 'Grey / Light Green', 'material.grey-light_green.min.css', NULL, 1, 255, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(256, 'Grey / Light Blue', 'material.grey-light_blue.min.css', NULL, 1, 256, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(257, 'Grey / Indigo', 'material.grey-indigo.min.css', NULL, 1, 257, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(258, 'Grey / Green', 'material.grey-green.min.css', NULL, 1, 258, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(259, 'Grey / Deep Purple', 'material.grey-deep_purple.min.css', NULL, 1, 259, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(260, 'Grey / Deep Orange', 'material.grey-deep_orange.min.css', NULL, 1, 260, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(261, 'Grey / Cyan', 'material.grey-cyan.min.css', NULL, 1, 261, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(262, 'Grey / Blue', 'material.grey-blue.min.css', NULL, 1, 262, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(263, 'Grey / Amber', 'material.grey-amber.min.css', NULL, 1, 263, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(264, 'Green / Yellow', 'material.green-yellow.min.css', NULL, 1, 264, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(265, 'Green / Teal', 'material.green-teal.min.css', NULL, 1, 265, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(266, 'Green / Red', 'material.green-red.min.css', NULL, 1, 266, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(267, 'Green / Purple', 'material.green-purple.min.css', NULL, 1, 267, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(268, 'Green / Pink', 'material.green-pink.min.css', NULL, 1, 268, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(269, 'Green / Orange', 'material.green-orange.min.css', NULL, 1, 269, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(270, 'Green / Lime', 'material.green-lime.min.css', NULL, 1, 270, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(271, 'Green / Light Green', 'material.green-light_green.min.css', NULL, 1, 271, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(272, 'Green / Light Blue', 'material.green-light_blue.min.css', NULL, 1, 272, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(273, 'Green / Indigo', 'material.green-indigo.min.css', NULL, 1, 273, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(274, 'Green / Deep Purple', 'material.green-deep_purple.min.css', NULL, 1, 274, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(275, 'Green / Deep Orange', 'material.green-deep_orange.min.css', NULL, 1, 275, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(276, 'Green / Cyan', 'material.green-cyan.min.css', NULL, 1, 276, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(277, 'Green / Blue', 'material.green-blue.min.css', NULL, 1, 277, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(278, 'Green / Amber', 'material.green-amber.min.css', NULL, 1, 278, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(279, 'Deep Purple / Yellow', 'material.deep_purple-yellow.min.css', NULL, 1, 279, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(280, 'Deep Purple / Teal', 'material.deep_purple-teal.min.css', NULL, 1, 280, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(281, 'Deep Purple / Red', 'material.deep_purple-red.min.css', NULL, 1, 281, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(282, 'Deep Purple / Purple', 'material.deep_purple-purple.min.css', NULL, 1, 282, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(283, 'Deep Purple / Pink', 'material.deep_purple-pink.min.css', NULL, 1, 283, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(284, 'Deep Purple / Orange', 'material.deep_purple-orange.min.css', NULL, 1, 284, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(285, 'Deep Purple / Lime', 'material.deep_purple-lime.min.css', NULL, 1, 285, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(286, 'Deep Purple / Light Green', 'material.deep_purple-light_green.min.css', NULL, 1, 286, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(287, 'Deep Purple / Light Blue', 'material.deep_purple-light_blue.min.css', NULL, 1, 287, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL),
(288, 'Deep Purple / Indigo', 'material.deep_purple-indigo.min.css', NULL, 1, 288, 'theme', '2017-12-09 02:58:53', '2017-12-09 02:58:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_form_data_jl_d_kota`
--

CREATE TABLE `t_form_data_jl_d_kota` (
  `id` int(11) NOT NULL,
  `dinas` varchar(100) DEFAULT NULL,
  `tanggal_proses` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL DEFAULT '0',
  `lokasi` varchar(200) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_form_data_skpd`
--

CREATE TABLE `t_form_data_skpd` (
  `ID_FORM_DATA_SKPD` int(11) NOT NULL,
  `ID_SKPD` int(11) NOT NULL,
  `NAMA_FORM_DATA_SKPD` varchar(100) DEFAULT NULL,
  `KETERANGAN` varchar(250) DEFAULT NULL,
  `FILE_FORM` varchar(200) DEFAULT NULL,
  `TABEL_FORM` varchar(200) DEFAULT NULL,
  `UPDATED_AT` datetime NOT NULL,
  `CREATED_AT` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_form_data_skpd`
--

INSERT INTO `t_form_data_skpd` (`ID_FORM_DATA_SKPD`, `ID_SKPD`, `NAMA_FORM_DATA_SKPD`, `KETERANGAN`, `FILE_FORM`, `TABEL_FORM`, `UPDATED_AT`, `CREATED_AT`) VALUES
(6, 2, '12312313', '123123', 'uploads\\form-data\\skpd\\Halaman Utama.png', '123213', '2017-12-25 06:17:52', '2017-12-25 06:17:52'),
(8, 3, 'jeri', 'sample sadas', 'uploads\\form-data\\skpd\\21150236_1973520736269462_8085980254492185281_n - Copy.jpg', 'T_SADAS', '2018-01-07 13:17:19', '2018-01-07 13:17:19');

-- --------------------------------------------------------

--
-- Table structure for table `t_form_infrastruktur_d`
--

CREATE TABLE `t_form_infrastruktur_d` (
  `id` int(11) NOT NULL,
  `id_form_infra_p` int(11) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `kegiatan` varchar(255) NOT NULL,
  `hasil` varchar(255) NOT NULL,
  `image_1` varchar(100) DEFAULT NULL,
  `image_1_name` varchar(100) DEFAULT NULL,
  `image_2` varchar(100) DEFAULT NULL,
  `image_2_name` varchar(100) DEFAULT NULL,
  `image_3` varchar(100) DEFAULT NULL,
  `image_3_name` varchar(100) DEFAULT NULL,
  `image_4` varchar(100) DEFAULT NULL,
  `image_4_name` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_form_infrastruktur_p`
--

CREATE TABLE `t_form_infrastruktur_p` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_kecamatan` int(11) NOT NULL,
  `id_desa` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL,
  `nilai` decimal(16,2) NOT NULL,
  `catatan` varchar(255) NOT NULL,
  `tgl_proses` datetime NOT NULL,
  `kord_x1` int(11) NOT NULL DEFAULT '0',
  `kord_x2` int(11) NOT NULL DEFAULT '0',
  `kord_y1` int(11) NOT NULL DEFAULT '0',
  `kord_y2` int(11) NOT NULL DEFAULT '0',
  `image_1` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `image_3` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_form_infrastruktur_p`
--

INSERT INTO `t_form_infrastruktur_p` (`id`, `id_kategori`, `id_jenis`, `id_kecamatan`, `id_desa`, `nama`, `alamat`, `tahun`, `nilai`, `catatan`, `tgl_proses`, `kord_x1`, `kord_x2`, `kord_y1`, `kord_y2`, `image_1`, `image_2`, `image_3`, `created_at`, `updated_at`) VALUES
(7, 2, 7, 2, 11, 'sample 1', 'jakarta barat', 2015, '200000.00', 'tes 123', '2010-01-18 00:00:00', 0, 170132, 115, 342193, '15155935390.jpg', '15155935391.jpg', '15155935392.jpg', '2018-01-10 14:12:19', '2018-01-10 14:12:19');

-- --------------------------------------------------------

--
-- Table structure for table `t_jalan_pu`
--

CREATE TABLE `t_jalan_pu` (
  `ID_JALAN_PU` int(11) NOT NULL,
  `ID_FORM_DATA_SKPD` int(11) DEFAULT NULL,
  `NAMA_PROYEK` varchar(50) DEFAULT NULL,
  `LOKASI` varchar(50) DEFAULT NULL,
  `PANJANG` varchar(50) DEFAULT NULL,
  `SUMBER_ANGGARAN` varchar(50) DEFAULT NULL,
  `TAHUN_ANGGARAN` int(11) DEFAULT NULL,
  `JUMLAH_ANGGARAN` decimal(16,2) DEFAULT NULL,
  `KORD_X` decimal(6,6) DEFAULT NULL,
  `KORD_Y` decimal(6,6) DEFAULT NULL,
  `TGL_DATA` datetime DEFAULT NULL,
  `TGL_KIRIM` datetime DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `KETERANGAN` varchar(50) DEFAULT NULL,
  `FILE_SENT` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_jenis_akses`
--

CREATE TABLE `t_jenis_akses` (
  `ID_JENIS_AKSES` char(10) NOT NULL,
  `NAMA_JENIS_AKSES` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kecamatan`
--

CREATE TABLE `t_kecamatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kecamatan`
--

INSERT INTO `t_kecamatan` (`id`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'LONG APARI', '', '2017-12-27 09:19:50', NULL),
(2, 'LONG PAHANGAI', '', '2017-12-27 09:20:08', NULL),
(3, 'LONG BAGUN', '', '2017-12-27 09:20:08', NULL),
(4, 'LAHAM', '', '2017-12-27 09:20:25', NULL),
(5, 'LONG HUBUNG', '', '2017-12-27 09:20:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_log_history`
--

CREATE TABLE `t_log_history` (
  `ID_LOG_HISTORY` int(11) NOT NULL,
  `ID_USER` int(11) NOT NULL,
  `TANGGAL_AKSES` datetime NOT NULL,
  `ID_JENIS_AKSES` int(11) DEFAULT NULL,
  `IP_ADDRESS` char(1) NOT NULL,
  `KETERAINGAN` char(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_m_desa`
--

CREATE TABLE `t_m_desa` (
  `id` int(11) NOT NULL,
  `id_kec` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_m_desa`
--

INSERT INTO `t_m_desa` (`id`, `id_kec`, `nama`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Long Apari', NULL, '2017-12-27 09:41:15', NULL),
(2, 1, 'Noha Silat', NULL, '2017-12-27 09:41:15', NULL),
(3, 1, 'Noha Tivab', NULL, '2017-12-27 09:41:15', NULL),
(4, 1, 'Tiong Ohang', NULL, '2017-12-27 09:41:15', NULL),
(5, 1, 'Tiong Bu\'u', NULL, '2017-12-27 09:41:15', NULL),
(6, 1, 'Noha Buan', NULL, '2017-12-27 09:41:15', NULL),
(7, 1, 'Long Penaneh I', NULL, '2017-12-27 09:41:15', NULL),
(8, 1, 'Long Penaneh II', NULL, '2017-12-27 09:41:15', NULL),
(9, 1, 'Long Penaneh III', NULL, '2017-12-27 09:41:15', NULL),
(10, 1, 'Long Kerioq', NULL, '2017-12-27 09:41:15', NULL),
(11, 2, 'Long Pakaq', NULL, '2017-12-27 09:41:15', NULL),
(12, 2, 'Long Pakaq Baru', NULL, '2017-12-27 09:41:15', NULL),
(13, 2, 'Long Lunuk', NULL, '2017-12-27 09:41:15', NULL),
(14, 2, 'Long Lunuk Baru', NULL, '2017-12-27 09:41:15', NULL),
(15, 2, 'Lirung Ubing', NULL, '2017-12-27 09:41:15', NULL),
(16, 2, 'Long Pahangai I', NULL, '2017-12-27 09:41:15', NULL),
(17, 2, 'Long Pahangai II', NULL, '2017-12-27 09:41:15', NULL),
(18, 2, 'Liu Mulang', NULL, '2017-12-27 09:41:15', NULL),
(19, 2, 'Datah Naha', NULL, '2017-12-27 09:41:15', NULL),
(20, 2, 'Long Tuyoq', NULL, '2017-12-27 09:41:15', NULL),
(21, 2, 'Naha Aruq\r\n', NULL, '2017-12-27 09:44:28', NULL),
(22, 2, 'Delang Kerohong\r\n', NULL, '2017-12-27 09:44:28', NULL),
(23, 2, 'Long Isun', NULL, '2017-12-27 09:44:28', NULL),
(24, 3, 'Long Bagun Ulu', NULL, '2017-12-27 09:44:28', NULL),
(25, 3, 'Long Bagun Ilir', NULL, '2017-12-27 09:44:28', NULL),
(26, 3, 'Batoq Kelo', NULL, '2017-12-27 09:44:28', NULL),
(27, 3, 'Batu Majang', NULL, '2017-12-27 09:44:28', NULL),
(28, 3, 'Ujoh Bilang', NULL, '2017-12-27 09:44:28', NULL),
(29, 3, 'Long Melaham', NULL, '2017-12-27 09:44:28', NULL),
(30, 3, 'Mamahak Ulu', NULL, '2017-12-27 09:44:28', NULL),
(31, 3, 'Mamahak Besar', NULL, '2017-12-27 09:44:28', NULL),
(32, 3, 'Rukun Damai', NULL, '2017-12-27 09:44:28', NULL),
(33, 3, 'Long Merah', NULL, '2017-12-27 09:44:28', NULL),
(34, 3, 'Long Hurai', NULL, '2017-12-27 09:44:28', NULL),
(35, 4, 'Laham', NULL, '2017-12-27 09:44:28', NULL),
(36, 4, 'Muara Ratah', NULL, '2017-12-27 09:47:11', NULL),
(37, 4, 'Danum Paroy', NULL, '2017-12-27 09:47:11', NULL),
(38, 4, 'Nyaribungan', NULL, '2017-12-27 09:47:11', NULL),
(39, 4, 'Long Gelawang', NULL, '2017-12-27 09:47:11', NULL),
(40, 5, 'Long Hubung Ulu', NULL, '2017-12-27 09:47:11', NULL),
(41, 5, 'Long Hubung', NULL, '2017-12-27 09:47:11', NULL),
(42, 5, 'Datah Bilang Ulu', NULL, '2017-12-27 09:47:11', NULL),
(43, 5, 'Datah Bilang Ilir', NULL, '2017-12-27 09:47:11', NULL),
(44, 5, 'Datah Bilang Baru', NULL, '2017-12-27 09:47:11', NULL),
(45, 5, 'Lutan', NULL, '2017-12-27 09:47:11', NULL),
(46, 5, 'Sirau', NULL, '2017-12-27 09:47:11', NULL),
(47, 5, 'Mamahak Teboq', NULL, '2017-12-27 09:47:11', NULL),
(48, 5, 'Tri Pariq Makmur', NULL, '2017-12-27 09:47:11', NULL),
(49, 5, 'Wana Pariq', NULL, '2017-12-27 09:47:11', NULL),
(50, 5, 'Matalibaq', NULL, '2017-12-27 09:47:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_m_dinas`
--

CREATE TABLE `t_m_dinas` (
  `id` int(3) NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_m_dinas`
--

INSERT INTO `t_m_dinas` (`id`, `name`, `created_at`, `updated_at`) VALUES
(3, 'DINAS PU', '2017-12-24 14:29:21', '2017-12-24 14:29:21'),
(4, 'DINAS PU 2', '2017-12-24 14:29:25', '2017-12-24 14:29:25'),
(5, 'DINAS PU 3', '2017-12-24 14:29:30', '2017-12-24 14:29:30');

-- --------------------------------------------------------

--
-- Table structure for table `t_m_infra_jenis`
--

CREATE TABLE `t_m_infra_jenis` (
  `id` int(2) NOT NULL,
  `id_kategori` int(2) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_m_infra_jenis`
--

INSERT INTO `t_m_infra_jenis` (`id`, `id_kategori`, `nama`, `created_at`, `updated_at`) VALUES
(1, 1, 'PLTD', '2017-12-27 10:00:08', NULL),
(2, 1, 'Genset', '2017-12-27 10:00:08', NULL),
(3, 1, 'PLTS', '2017-12-27 10:00:08', NULL),
(4, 1, 'PLTS Komunal', '2017-12-27 10:00:08', NULL),
(5, 1, 'Tidak ada Listrik', '2017-12-27 10:00:08', NULL),
(6, 2, 'Sumber Air Bersih', '2017-12-27 10:00:08', NULL),
(7, 2, 'Sumber PAM', '2017-12-27 10:00:08', NULL),
(8, 2, 'Sungai', '2017-12-27 10:00:08', NULL),
(9, 3, 'Ada', '2017-12-27 10:00:08', NULL),
(10, 3, 'Tidak Ada', '2017-12-27 10:00:08', NULL),
(11, 4, 'Jalan Nasional', '2017-12-27 10:00:08', NULL),
(12, 4, 'Jalan Propinsi', '2017-12-27 10:00:08', NULL),
(13, 4, 'Jalan Daerah/Kota', '2017-12-27 10:00:08', NULL),
(14, 4, 'Jalan Kampung', '2017-12-27 10:00:08', NULL),
(15, 5, 'SMA/SMK', '2017-12-27 10:00:08', NULL),
(16, 5, 'SMP', '2017-12-27 10:00:08', NULL),
(17, 5, 'SD', '2017-12-27 10:00:08', NULL),
(18, 5, 'TK', '2017-12-27 10:00:08', NULL),
(19, 5, 'Kantor Petinggi', '2017-12-27 10:00:08', NULL),
(20, 5, 'Puskesmas', '2017-12-27 10:00:08', NULL),
(21, 5, 'Dermaga', '2017-12-27 10:00:08', NULL),
(22, 5, 'Pasar', '2017-12-27 10:00:08', NULL),
(23, 5, 'Gedung PKK', '2017-12-27 10:00:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_m_infra_kategory`
--

CREATE TABLE `t_m_infra_kategory` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_m_infra_kategory`
--

INSERT INTO `t_m_infra_kategory` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'LISTRIK', '2017-12-27 09:30:30', NULL),
(2, 'AIR BERSIH', '2017-12-27 09:30:30', NULL),
(3, 'TELEKOMUNIKASI', '2017-12-27 09:30:42', NULL),
(4, 'JALAN', '2017-12-27 09:30:42', NULL),
(5, 'FASUM/FASOS', '2017-12-27 09:30:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `signup_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_confirmation_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signup_sm_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `password`, `remember_token`, `activated`, `token`, `signup_ip_address`, `signup_confirmation_ip_address`, `signup_sm_ip_address`, `admin_ip_address`, `updated_ip_address`, `deleted_ip_address`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'reta46 123', 'Miracle', 'Carroll', 'admin@admin.com', '$2y$10$8.tI1kErQCELggvplD0gpu5o50CQGqBMAJMtQWtc9gEB11kAUfulO', '2dgwbBQ4lFUklX1P16fC4f58Yg01IUsC2oHSIGh0wWpVifuTTGxeyKgvVXPB', 1, '13JwRzbRYquLN24Z1gFxwWmyFQogrKQuPFD2aFiIEoxyugHp1zvinx22AVYDTpMM', NULL, '109.227.228.62', NULL, '79.143.24.202', '0.0.0.0', NULL, '2017-12-09 02:58:54', '2017-12-24 14:37:04', NULL),
(2, 'kitty.bayer', 'Woodrow', 'Hand', 'user@user.com', '$2y$10$aaS.2zzXzLnKRw4hNpj.eOmCu9Qo23jOCsaRUTelIs1/reegQf5uW', NULL, 1, '6d8ZLZymiY1cARWv05qDZHyoRJBbqphb1Zd2HgxvCYQIUXM3rA153qWqeIwtdVjH', '113.56.120.54', '206.158.7.43', NULL, NULL, NULL, '0.0.0.0', '2017-12-09 02:58:54', '2017-12-10 20:59:06', '2017-12-10 20:59:06'),
(6, 'user2', 'User', 'Test', 'user2@user.com', '$2y$10$HkmrUhW45p0Z9w8Jac6RLuS2fbu349/O/4Ueyx0q4XCQhM1VlX40e', NULL, 1, 'MYhSTQRtPbfJJzjcFTQezyxOBd7fCCKHSkknUSko6b6RRxttllVPegTAeNuIxewn', NULL, NULL, NULL, '0.0.0.0', NULL, '0.0.0.0', '2017-12-24 12:13:47', '2017-12-24 12:49:12', '2017-12-24 12:49:12'),
(7, 'test2', 'User', 'Test', 'user4@user.com', '$2y$10$fYksvimG9IcaqdRoFnt4xuQqRBT4OfYf.wIt90JbGfV2Td62alxiG', '980ERhXCBiuRCQN5dlGk5LrXcanoCUOmqMxFsLPGuAUt2EKDqAa7edhcZzQX', 1, '1CSq5J6M7PpNY3mx1so30EpuWeQ71k1iUpIVlS6bTOCFQTVLCboOUrS3nbEPhpw1', NULL, NULL, NULL, '0.0.0.0', '0.0.0.0', NULL, '2017-12-24 12:23:51', '2018-01-10 19:35:20', NULL),
(8, 'User', 'Userlima', 'Lima', 'user5@mail.com', '$2y$10$3lHj41Koy./iPti.FADqW.DthEZ9IK9WopKXGLzm8mzxa0ylYLmcW', 'pvYeOpKIQWdz5PlsyP7pOoOm6tDn5QEuN00nMAssXIiZIePVHH8qVVBlfGfj', 1, 'o8SM6gQ2TOXrWZ4bPWDaziQC4yvtiM8A9RCtSzE2tXMgytITZphSiGZQrcloBXaX', NULL, NULL, NULL, '0.0.0.0', NULL, '0.0.0.0', '2017-12-24 12:50:34', '2017-12-24 14:45:57', '2017-12-24 14:45:57'),
(9, 'tes', 'Tes', 'Lagi', 'tes@mail.com', '$2y$10$p7E6Tr4c7EHRZlnTj6t1GOldBgMf/LzuQ6M2EES280aoOzzl0PWXa', NULL, 1, '1E9Xv8ntK4oIXjLIgpkCcMtsz66O25zkthVM4HlSGRKr7v29lwp7Y0NXLSUd2ew3', NULL, NULL, NULL, '0.0.0.0', NULL, '0.0.0.0', '2017-12-24 14:53:14', '2018-01-10 19:35:53', '2018-01-10 19:35:53'),
(10, 'Jeri', 'Jeri', 'Alam', 'jeri@mail.com', '$2y$10$m3EZDquGGH2dzQ10sUULUeBRHL0U/CBZtTQV7HtKmqPLlkiZcjOdq', NULL, 1, '7yu5NgFPcXnb0i2hZ2bcryy62ku0Ethxa1zZpl34zIlT9xn811hygxjH4co1aLiI', NULL, NULL, NULL, '0.0.0.0', NULL, '0.0.0.0', '2018-01-07 07:25:13', '2018-01-07 07:58:04', '2018-01-07 07:58:04'),
(11, 'Johan', 'Johan', 'Firman', 'johan@mahulu.go.id', '$2y$10$HRb0vRyBEGUcmvqbDx0Ble96XQp/OB2GXOUC5rNX0QIkQEwWWzOZS', 'zYyx8Rn443WEQ4wODfo06YesVS5XVLQHE503bSJ0e6GtV48yHqwjMs2lTCa3', 1, '5wqZ7hNijDeLZxwmy1Xh7vfC8r2JSSxirziu6pVX993RuDWY8qKEHwNz4w0i7zqZ', NULL, NULL, NULL, '0.0.0.0', NULL, '0.0.0.0', '2018-01-07 08:08:54', '2018-01-10 19:34:57', '2018-01-10 19:34:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activations`
--
ALTER TABLE `activations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activations_user_id_index` (`user_id`);

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infra_air_bersih`
--
ALTER TABLE `infra_air_bersih`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `infra_listrik`
--
ALTER TABLE `infra_listrik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_slug_unique` (`slug`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_user_permission_id_index` (`permission_id`),
  ADD KEY `permission_user_user_id_index` (`user_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_theme_id_foreign` (`theme_id`),
  ADD KEY `profiles_user_id_index` (`user_id`);

--
-- Indexes for table `proyek`
--
ALTER TABLE `proyek`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama_proyek` (`nama_proyek`);

--
-- Indexes for table `proyek_p`
--
ALTER TABLE `proyek_p`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indexes for table `skpd`
--
ALTER TABLE `skpd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_skpd` (`id`);

--
-- Indexes for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_logins_user_id_index` (`user_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_user_id_foreign` (`user_id`);

--
-- Indexes for table `telekomunikasi`
--
ALTER TABLE `telekomunikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `themes`
--
ALTER TABLE `themes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `themes_name_unique` (`name`),
  ADD UNIQUE KEY `themes_link_unique` (`link`),
  ADD KEY `themes_taggable_id_taggable_type_index` (`taggable_id`,`taggable_type`),
  ADD KEY `themes_id_index` (`id`);

--
-- Indexes for table `t_form_data_jl_d_kota`
--
ALTER TABLE `t_form_data_jl_d_kota`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `t_form_data_skpd`
--
ALTER TABLE `t_form_data_skpd`
  ADD PRIMARY KEY (`ID_FORM_DATA_SKPD`);

--
-- Indexes for table `t_form_infrastruktur_d`
--
ALTER TABLE `t_form_infrastruktur_d`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `f_form_infra` (`id_form_infra_p`);

--
-- Indexes for table `t_form_infrastruktur_p`
--
ALTER TABLE `t_form_infrastruktur_p`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `f_jenis_kategori2` (`id_kategori`),
  ADD KEY `f_kecamatan2` (`id_kecamatan`),
  ADD KEY `f_jenis2` (`id_jenis`),
  ADD KEY `f_desa2` (`id_desa`);

--
-- Indexes for table `t_jalan_pu`
--
ALTER TABLE `t_jalan_pu`
  ADD PRIMARY KEY (`ID_JALAN_PU`);

--
-- Indexes for table `t_jenis_akses`
--
ALTER TABLE `t_jenis_akses`
  ADD PRIMARY KEY (`ID_JENIS_AKSES`);

--
-- Indexes for table `t_kecamatan`
--
ALTER TABLE `t_kecamatan`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `t_log_history`
--
ALTER TABLE `t_log_history`
  ADD PRIMARY KEY (`ID_LOG_HISTORY`);

--
-- Indexes for table `t_m_desa`
--
ALTER TABLE `t_m_desa`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `f_desa_kecamatan` (`id_kec`);

--
-- Indexes for table `t_m_dinas`
--
ALTER TABLE `t_m_dinas`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `t_m_infra_jenis`
--
ALTER TABLE `t_m_infra_jenis`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `f_jenis_kategori` (`id_kategori`);

--
-- Indexes for table `t_m_infra_kategory`
--
ALTER TABLE `t_m_infra_kategory`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activations`
--
ALTER TABLE `activations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `infra_air_bersih`
--
ALTER TABLE `infra_air_bersih`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `infra_listrik`
--
ALTER TABLE `infra_listrik`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=603;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `permission_user`
--
ALTER TABLE `permission_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `proyek`
--
ALTER TABLE `proyek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `proyek_p`
--
ALTER TABLE `proyek_p`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `skpd`
--
ALTER TABLE `skpd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `social_logins`
--
ALTER TABLE `social_logins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `telekomunikasi`
--
ALTER TABLE `telekomunikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `themes`
--
ALTER TABLE `themes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=289;
--
-- AUTO_INCREMENT for table `t_form_data_jl_d_kota`
--
ALTER TABLE `t_form_data_jl_d_kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_form_data_skpd`
--
ALTER TABLE `t_form_data_skpd`
  MODIFY `ID_FORM_DATA_SKPD` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_form_infrastruktur_d`
--
ALTER TABLE `t_form_infrastruktur_d`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_form_infrastruktur_p`
--
ALTER TABLE `t_form_infrastruktur_p`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_jalan_pu`
--
ALTER TABLE `t_jalan_pu`
  MODIFY `ID_JALAN_PU` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_kecamatan`
--
ALTER TABLE `t_kecamatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_log_history`
--
ALTER TABLE `t_log_history`
  MODIFY `ID_LOG_HISTORY` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_m_desa`
--
ALTER TABLE `t_m_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `t_m_dinas`
--
ALTER TABLE `t_m_dinas`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_m_infra_jenis`
--
ALTER TABLE `t_m_infra_jenis`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `t_m_infra_kategory`
--
ALTER TABLE `t_m_infra_kategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `activations`
--
ALTER TABLE `activations`
  ADD CONSTRAINT `activations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_theme_id_foreign` FOREIGN KEY (`theme_id`) REFERENCES `themes` (`id`),
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `social_logins`
--
ALTER TABLE `social_logins`
  ADD CONSTRAINT `social_logins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `t_form_infrastruktur_d`
--
ALTER TABLE `t_form_infrastruktur_d`
  ADD CONSTRAINT `f_form_infra` FOREIGN KEY (`id_form_infra_p`) REFERENCES `t_form_infrastruktur_p` (`id`);

--
-- Constraints for table `t_form_infrastruktur_p`
--
ALTER TABLE `t_form_infrastruktur_p`
  ADD CONSTRAINT `f_desa2` FOREIGN KEY (`id_desa`) REFERENCES `t_m_desa` (`id`),
  ADD CONSTRAINT `f_jenis2` FOREIGN KEY (`id_jenis`) REFERENCES `t_m_infra_jenis` (`id`),
  ADD CONSTRAINT `f_jenis_kategori2` FOREIGN KEY (`id_kategori`) REFERENCES `t_m_infra_kategory` (`id`),
  ADD CONSTRAINT `f_kecamatan2` FOREIGN KEY (`id_kecamatan`) REFERENCES `t_kecamatan` (`id`);

--
-- Constraints for table `t_m_desa`
--
ALTER TABLE `t_m_desa`
  ADD CONSTRAINT `f_desa_kecamatan` FOREIGN KEY (`id_kec`) REFERENCES `t_kecamatan` (`id`);

--
-- Constraints for table `t_m_infra_jenis`
--
ALTER TABLE `t_m_infra_jenis`
  ADD CONSTRAINT `f_jenis_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `t_m_infra_kategory` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
