<!DOCTYPE html>
<html lang="<?php echo e(config('app.locale')); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php if(trim($__env->yieldContent('template_title'))): ?><?php echo $__env->yieldContent('template_title'); ?> | <?php endif; ?> <?php echo e(config('app.name', Lang::get('titles.app'))); ?></title>
    <meta name="description" content="">
    <meta name="author" content="Jeremy Kenedy">
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    
    <link rel="stylesheet" href="<?php echo e(asset('ProUI/css/bootstrap.min.css')); ?>">

    <link rel="stylesheet" href="<?php echo e(asset('ProUI/css/plugins.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('ProUI/css/main.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('ProUI/css/themes.css')); ?>">

    <script src="<?php echo e(asset('ProUI/js/vendor/modernizr.min.js')); ?>"></script>

    
    <?php echo HTML::style('https://fonts.googleapis.com/css?family=Roboto:300italic,400italic,400,100,300,600,700', array('type' => 'text/css', 'rel' => 'stylesheet')); ?>

    <?php echo HTML::style(asset('https://fonts.googleapis.com/icon?family=Material+Icons'), array('type' => 'text/css', 'rel' => 'stylesheet')); ?>


    <style type="text/css">
        <?php echo $__env->yieldContent('template_fastload_css'); ?>

            <?php if(Auth::User() && (Auth::User()->profile) && (Auth::User()->profile->avatar_status == 0)): ?>
                .user-avatar-nav {
            background: url(<?php echo e(Gravatar::get(Auth::user()->email)); ?>) 50% 50% no-repeat;
            background-size: auto 100%;
        }
        <?php endif; ?>

    </style>

    
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>;
    </script>
</head>
<body>

<div id="page-wrapper">
    <div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
        <?php echo $__env->make('infra.layouts.nav-side', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div id="main-container">
            <?php echo $__env->make('infra.layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div id="page-content">
                <?php echo $__env->yieldContent('header-breadcrumbs'); ?>
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        <?php echo $__env->make('infra.layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
    </div>
</div>


<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="<?php echo e(asset('ProUI/js/vendor/jquery.min.js')); ?>"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="<?php echo e(asset('ProUI/js/vendor/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('ProUI/js/plugins.js')); ?>"></script>
<script src="<?php echo e(asset('ProUI/js/app.js')); ?>"></script>

<script src="<?php echo e(asset('ProUI/js/pages/index.js')); ?>"></script>

<?php echo $__env->yieldContent('footer_scripts'); ?>
</body>
</html>