<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
    <ul class="breadcrumb breadcrumb-top">
        <li> Users List</li>
        <li><a href="/users">Users</a></li>
    </ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <?php echo Form::open(array('action' => 'RoleController@update', 'method' => 'POST', 'role' => 'form')); ?>

            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <label class="col-md-3 control-label" for="example-text-input">Nama Role</label>
                        <input type="text" id="editRoleName" name="role_name" class="form-control" placeholder="Nama Role">
                        <input type="hidden" id="idRoleedit" name="idRoleedit">
                        <span class="help-block"></span>
                        <label class="col-md-3 control-label" for="example-text-input">Slug Role</label>
                        <input type="text" id="slug-list-edit" name="slug-list-edit" class="form-control" placeholder="Slug Role">
                        <label class="col-md-3 control-label" for="example-textarea-input">description</label>
                        <textarea id="edit-desc" name="descript" rows="9" class="form-control" placeholder=""></textarea>
                        <label class="col-md-3 control-label" for="example-text-input">Level</label>
                        <select id="edit-level" name="level">
                            <option value="" disabled>--Pilih Level--</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-success" id="bnt-update">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
            <?php echo Form::close(); ?>

        </div>
        <div class="col-md-5">
            <div class="block">
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
                    </div>
                    <h2><strong>Basic Form</strong> Elements</h2>
                </div>
                <?php echo Form::open(array('action' => 'RoleController@store', 'method' => 'POST', 'role' => 'form', 'class' => 'form-horizontal form-bordered')); ?>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Nama Role</label>
                    <div class="col-md-9">
                        <input type="text" id="example-text-input" name="role_name" class="form-control" placeholder="Nama Role">
                        <span class="help-block"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Slug Role</label>
                    <div class="col-md-9">
                        <input type="text" id="example-text-input" name="role-list" class="form-control" placeholder="Slug Role">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-textarea-input">description</label>
                    <div class="col-md-9">
                        <textarea id="example-textarea-input" name="descript" rows="9" class="form-control" placeholder=""></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label" for="example-text-input">Level</label>
                    <div class="col-md-9">
                        <select id="level" name="level">
                            <option value="" disabled>--Pilih Level--</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>
                <div class="from-group">
                    <button class="btn btn-success">Save</button>
                </div>
                <?php echo Form::close(); ?>

            </div>
        </div>
        <div class="col-md-7 pull-right">
            <div class="block">
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-alt btn-sm btn-default toggle-bordered enable-tooltip" data-toggle="button" title="Toggles .form-bordered class">No Borders</a>
                    </div>
                    <h2><strong>Basic Form</strong> Elements</h2>
                </div>
                <div class="table-responsive">
                    <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                        <thead>
                        <tr>
                            <th style="font-size: 14px;">ID</th>
                            <th style="font-size: 14px;">Nama</th>
                            <th style="font-size: 14px;">Slug</th>
                            <th style="font-size: 14px;">Description</th>
                            <th style="font-size: 14px;">Level</th>
                            <th style="font-size: 14px;">Opsi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $role; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $roles): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td style="font-size: 12px;"><span class="idRole"><?php echo e($roles->id); ?></span></td>
                                <td style="font-size: 12px;"><?php echo e($roles->name); ?></td>
                                <td style="font-size: 12px;"><?php echo e($roles->slug); ?></td>
                                <td style="font-size: 12px;"><?php echo e($roles->description); ?></td>
                                <td style="font-size: 12px;"><?php echo e($roles->level); ?></td>
                                <td style="font-size: 12px;text-align: center;"><a href="#" data-toggle="modal" data-target="#myModal" class="btnEdit"><i class="fa fa-pencil"></i></a> | <a href="#"><i class="fa fa-trash"></i></a> </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $("body").on("click",".btnEdit",function(){
            var id = $(this).parent("td").prev("td").prev("td").prev("td").prev("td").prev("td").text();
            var nama = $(this).parent("td").prev("td").prev("td").prev("td").prev("td").text();
            var slug = $(this).parent("td").prev("td").prev("td").prev("td").text();
            var desc = $(this).parent("td").prev("td").prev("td").text();
            var lvl = $(this).parent("td").prev("td").text();

            $('#editRoleName').val(nama);
            $('#slug-list-edit').val(slug);
            $('#edit-desc').val(desc);
            $('#edit-level').val(lvl);
            $('#idRoleedit').val(id);
        });
        $('#bnt-update').click(function () {
//            $.ajax({
//                url : "update/role",
//                type : "POST",
//                data : {
//                    'id' : id,
//                    'nama' : nama,
//                    'slug' : slug,
//                    'desc' : desc,
//                    'lvl' : lvl
//                },
//                success: function(response) {
                    alert("Role Updated");
//                    window.reload();
//                }
//            });
////            alert('edan');
        })
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>