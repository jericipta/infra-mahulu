<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
    <ul class="breadcrumb breadcrumb-top">
        <li> Skpd List</li>
        <li><a href="/users">Skpd</a></li>
    </ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-5">
            <div class="block full">
                <?php echo Form::open(array('action' => 'SkpdController@store', 'method' => 'POST', 'role' => 'form', 'class' => 'form-bordered')); ?>

                    <div class="form-group">
                        <label for="nama_skpd">Nama SKPD</label>
                        <input type="text" id="nama_skpd" name="nama_skpd" class="form-control" placeholder="Nama SKPD">
                    </div>
                     <div class="form-group">
                        <label for="alamat_skpd">Alamat</label>
                        <textarea id="alamat_skpd" name="alamat_skpd" rows="9" class="form-control" placeholder="Masukan Alamat yang Valid"></textarea>
                    </div>
                    <div class="form-group form-actions">
                        <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Save</button>
                        <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
                    </div>
                <?php echo Form::close(); ?>

            </div>
        </div>
        <div class="col-md-7 pull-right">
            <div class="block full">
                <div class="table-responsive">
                <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center" style="font-size: 12px;">ID</th>
                        <th class="text-center" style="font-size: 12px;">Nama SKPD</th>
                        <th class="text-center" style="font-size: 12px;">Alamat</th>
                        <th class="text-center" style="font-size: 12px;">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $skpd; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="font-size: 12px;"><?php echo e($val->id); ?></td>
                            <td style="font-size: 12px;""><?php echo e($val->nama_skpd); ?></td>
                            <td style="font-size: 12px;"><?php echo e($val->alamat_skpd); ?></td>
                            <td class="text-center" style="font-size: 12px;">
                                <a href="#" data-toggle="modal" data-target="#myModal" class="btnEdit">
                                    <i class="fa fa-pencil"></i></a> | 
                                <a href="#" data-toggle="modal" data-target="#modal-delete" class="modal-delete" data-id="<?php echo e($val->id); ?>">
                                    <i class="fa fa-trash"></i></a> 
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <?php echo Form::open(array('action' => 'SkpdController@update', 'method' => 'POST', 'role' => 'form')); ?>

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <label class="col-md-3 control-label" for="example-text-input">Nama SKPD</label>
                <input type="text" id="editRoleName" name="role_name" class="form-control" placeholder="Nama Role">
                <input type="hidden" id="idRoleedit" name="idRoleedit">
                <span class="help-block"></span>
                <label class="col-md-3 control-label" for="example-text-input">Alamat SKPD</label>
                <textarea id="edit-desc" name="descript" rows="9" class="form-control" placeholder=""></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="bnt-update">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
<div id="modal-delete" class="modal fade" role="dialog">
    <?php echo Form::open(array('action' => 'SkpdController@delete', 'method' => 'POST', 'role' => 'form')); ?>

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confirm Delete</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id2" name="id">
                <span class="help-block"></span>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )); ?>

                <?php echo Form::button('<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Confirm Delete', array('class' => 'btn btn-danger pull-right btn-flat', 'type' => 'submit', 'id' => 'confirm' )); ?>

            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
    $("body").on("click",".btnEdit",function(){
        var id = $(this).parent("td").prev("td").prev("td").prev("td").text();
        var nama = $(this).parent("td").prev("td").prev("td").text();
        var alamat = $(this).parent("td").prev("td").text();
        $('#editRoleName').val(nama);
        $('#edit-desc').val(alamat);
        $('#idRoleedit').val(id);
//            $('#edit-desc').val(desc);
    });
    $("body").on("click",".modal-delete",function(){
        $('#id2').val($(this).attr('data-id'));
    });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>