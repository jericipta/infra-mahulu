<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Insert</li>
    <li><a href="">Jalan Daerah / Kota</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
 <div class="block full">
     <?php echo Form::open(array('action' => 'FormDataJDaerahKotaController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-bordered')); ?>

        <div class="form-group">
            <?php echo Form::Label('id-dinas', 'Nama Dinas'); ?>

            <?php echo Form::select('id-dinas', $dinas, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Dinas']); ?>

        </div>
        <div class="form-group">
            <?php echo Form::Label('tgl-proses', 'Tanggal Proses'); ?>

            <input type="text" id="tgl-proses" name="tgl-proses" class="form-control input-datepicker" data-date-format="dd-MM-yy" placeholder="dd-MM-yy">
        </div>
        <div class="form-group">
            <?php echo Form::label('File Jalan'); ?>

            <?php echo Form::file('jalan', null); ?>

        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="<?php echo e(url('/form-data-jalan-kota')); ?>" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    <?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>