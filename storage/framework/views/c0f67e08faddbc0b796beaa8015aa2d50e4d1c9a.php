<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Insert</li>
    <li><a href="">Jalan Daerah / Kota</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
 <div class="block full">
    <form action="page_forms_general.html" method="post" class="form-bordered" onsubmit="return false;">
        <div class="form-group">
            <label for="example-nf-email">Email</label>
            <input type="email" id="example-nf-email" name="example-nf-email" class="form-control" placeholder="Enter Email..">
            <span class="help-block">Please enter your email</span>
        </div>
        <div class="form-group">
            <label for="example-nf-password">Password</label>
            <input type="password" id="example-nf-password" name="example-nf-password" class="form-control" placeholder="Enter Password..">
            <span class="help-block">Please enter your password</span>
        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-user"></i> Login</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
        </div>
    </form>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>