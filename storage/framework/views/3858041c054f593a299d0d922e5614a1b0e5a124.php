<link rel="stylesheet" href="<?php echo e(asset('AdminLTE/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')); ?>">
<link rel="stylesheet" href="<?php echo e(asset('AdminLTE/bootstrap/dist/css/bootstrap.min.css')); ?>">
<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('breadcrumbs'); ?>

    <li itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
        <a itemprop="item" href="<?php echo e(url('/')); ?>">
			<span itemprop="name">

			</span>
        </a>
        <meta itemprop="position" content="1" />
    </li>
    <li class="active">
        <?php echo e(trans('titles.dashboard')); ?>

    </li>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="mdl-grid margin-top-0-important padding-top-0-important">

        <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell margin-top-0-important mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop">
            <br>

        </div>
        <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell margin-top-0-important mdl-cell--2-col mdl-cell--2-col-tablet mdl-cell--12-col-desktop">
            <br>
            <center><span>Jumlah Anggaran 2016</span></center>
            <img src="<?php echo e(asset('Untitled-2.png')); ?>">
        </div>

    </div>

    <canvas id="chart-area" />
    <h2>ajksdhkjas</h2>
    <script src="<?php echo e(asset('js/dist/Chart.bundle.js')); ?>"></script>
    <script src="<?php echo e(asset('js/utils.js')); ?>"></script>
    <script>
    var randomScalingFactor = function() {
    return Math.round(Math.random() * 100);
    };

    var config = {
    type: 'pie',
    data: {
    datasets: [{
    data: [
    randomScalingFactor(),
    randomScalingFactor(),
    randomScalingFactor(),
    randomScalingFactor(),
    randomScalingFactor(),
    ],
    backgroundColor: [
    window.chartColors.red,
    window.chartColors.orange,
    window.chartColors.yellow,
    window.chartColors.green,
    window.chartColors.blue,
    ],
    label: 'Dataset 1'
    }],
    labels: [
    "Red",
    "Orange",
    "Yellow",
    "Green",
    "Blue"
    ]
    },
    options: {
    responsive: true
    }
    };

    window.onload = function() {
    var ctx = document.getElementById("chart-area").getContext("2d");
    window.myPie = new Chart(ctx, config);
    };

    document.getElementById('randomizeData').addEventListener('click', function() {
    config.data.datasets.forEach(function(dataset) {
    dataset.data = dataset.data.map(function() {
    return randomScalingFactor();
    });
    });

    window.myPie.update();
    });

    var colorNames = Object.keys(window.chartColors);
    document.getElementById('addDataset').addEventListener('click', function() {
    var newDataset = {
    backgroundColor: [],
    data: [],
    label: 'New dataset ' + config.data.datasets.length,
    };

    for (var index = 0; index < config.data.labels.length; ++index) {
    newDataset.data.push(randomScalingFactor());

    var colorName = colorNames[index % colorNames.length];;
    var newColor = window.chartColors[colorName];
    newDataset.backgroundColor.push(newColor);
    }

    config.data.datasets.push(newDataset);
    window.myPie.update();
    });

    document.getElementById('removeDataset').addEventListener('click', function() {
    config.data.datasets.splice(0, 1);
    window.myPie.update();
    });
    </script>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>