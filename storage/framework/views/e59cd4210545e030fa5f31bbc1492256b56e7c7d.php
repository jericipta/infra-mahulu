<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li><a href="">Jalan Daerah / Kota</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="<?php echo e(URL::to( asset('form-data-jalan-kota/create'))); ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="" data-original-title="Tambah Data Jalan"><i class="fa fa-plus-square"></i></a>
        </div>
        <h2><strong>List SKPD</strong></h2>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Id</th>
                <th class="text-center" style="font-size: 12px;">Dinas</th>
                <th style="font-size: 12px;">Tanggal Proses</th>
                <th style="font-size: 12px;">Status</th>
                <th style="font-size: 12px;">Lokasi File</th>
                <th class="text-center" style="font-size: 12px;">Hasil</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $formdata; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $val): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($val->id); ?></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($val->dinas); ?></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($val->tanggal_proses); ?></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($val->status); ?></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($val->lokasi); ?></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($val->lokasi); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
<script>$(function(){ TablesDatatables.init(); });</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>