<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li> Users List</li>
    <li><a href="/users">Users</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="block full">
    <div class="block-title row">
        <a href="<?php echo e(URL::to('/users/deleted')); ?>" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white pull-right" title="Show Deleted Users">
            <h4><i class="fa fa-trash" aria-hidden="true"></i></h4>
            <span class="sr-only">Show Deleted Users</span>
        </a>
        <a href="<?php echo e(url('/users/create')); ?>" class="mdl-button mdl-button--icon mdl-js-button mdl-js-ripple-effect mdl-color-text--white pull-right" title="Add New User">
            <h4><i class="fa fa-user-plus"></i></h4>
            <span class="sr-only">Add New User</span>
        </a>
    </div>
    <div class="table-responsive">
            <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 14px;">ID</th>
                        <th style="font-size: 14px;">Username</th>
                        <th style="font-size: 14px;">Email</th>
                        <th style="font-size: 14px;">First Name</th>
                        <th style="font-size: 14px;">Last Name</th>
                        <th style="font-size: 14px;">Role</th>
                        <th style="font-size: 14px;">Created</th>
                        <th style="font-size: 14px;">Updated</th>
                        <th style="font-size: 14px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="font-size: 12px;"><?php echo e($user->id); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->name); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->email); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->first_name); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->last_name); ?></td>
                            <td style="font-size: 12px;">
                                <?php $__currentLoopData = $user->roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($user_role->name == 'User'): ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-user';
                                            $levelName        = 'User';
                                            $levelBgClass     = 'mdl-color--blue-200';
                                            $leveIconlBgClass = 'mdl-color--blue-500';
                                         ?>
                                    <?php elseif($user_role->name == 'Admin'): ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-users';
                                            $levelName        = 'Admin';
                                            $levelBgClass     = 'mdl-color--red-200';
                                            $leveIconlBgClass = 'mdl-color--red-500';
                                         ?>
                                    <?php elseif($user_role->name == 'Unverified'): ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-user-secret';
                                            $levelName        = 'Unverified';
                                            $levelBgClass     = 'mdl-color--orange-200';
                                            $leveIconlBgClass = 'mdl-color--orange-500';
                                         ?>
                                    <?php else: ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-user-secret';
                                            $levelName        = 'Unverified';
                                            $levelBgClass     = 'mdl-color--orange-200';
                                            $leveIconlBgClass = 'mdl-color--orange-500';
                                         ?>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <span><i class="<?php echo e($levelIcon); ?>"></i> <?php echo e($levelName); ?></span>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric mdl-layout--large-screen-only"><?php echo e($user->created_at); ?></td>
                            <td class="mdl-data-table__cell--non-numeric mdl-layout--large-screen-only"><?php echo e($user->updated_at); ?></td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <div class="btn-group btn-group-xs">
                                    <a href="/profile/<?php echo e($user->name); ?>" class="btn btn-primary" data-toggle="tooltip" title="Edit Selected"><i class="fa fa-pencil"></i></a>
                                    
                                    <?php echo Form::open(array('url' => 'users/' . $user->id, 'class' => 'inline-block', 'id' => 'delete_'.$user->id)); ?>

                                        <?php echo Form::hidden('_method', 'DELETE'); ?>

                                        <button data-userid="<?php echo e($user->id); ?>" data-toggle="tooltip" title="Delete" class="btn btn-danger"><i class="fa fa-times"></i>
                                        </button>
                                    <?php echo Form::close(); ?>

                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
            </table>
    </div>
</div>


<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script type="text/javascript">
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a_user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            mdl_dialog('.dialiog-trigger<?php echo e($a_user->id); ?>','','#dialog_delete');
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        var userid;
        $('.dialiog-trigger-delete').click(function(event) {
            event.preventDefault();
            userid = $(this).attr('data-userid');
        });
        $('#confirm').click(function(event) {
            $('form#delete_'+userid).submit();
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>