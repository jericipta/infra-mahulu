<div id="sidebar">
    <div id="sidebar-scroll">
        <div class="sidebar-content">
            <a href="<?php echo e(URL::to('/')); ?>" class="sidebar-brand">
                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Infra</strong></span>
            </a>

            <ul class="sidebar-nav">
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-table sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Kirim File</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-kirim-jalan')); ?>">Jalan</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-kirim-jembatan')); ?>">Jembatan</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-kirim-pengairan')); ?>">Pengairan</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-kirim-fasum')); ?>">Fasum</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="gi gi-table sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Input Data</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-listrik')); ?>">Listrik</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-air-bersih')); ?>">Air Bersih</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-telekomunikasi')); ?>">Telekomunikasi</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-jalan')); ?>">Jalan</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-fasum-fasos')); ?>">FasumFasos</a>
                        </li>
                        <li>
                            <a href="<?php echo e(URL::to('/infra-form-infrastruktur')); ?>">Form Infrastruktur</a>
                        </li>
                    </ul>
                </li>
                <?php if (Auth::check() && Auth::user()->hasRole('admin')): ?>
                <li class="sidebar-header">
                    <span class="sidebar-header-title">Admin Feature</span>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-folder sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Users</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo e(url('/users')); ?>">Daftar User</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('/logs')); ?>">Log User</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('/role')); ?>">Role User</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-folder sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Master</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo e(url('/skpd')); ?>">SKPD</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('/akses')); ?>">Jenis Akses</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('/form-master-dinas')); ?>">Dinas</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-folder sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Form Data</span></a>
                    <ul>
                        <li>
                            <a href="<?php echo e(url('/form-data-skpd')); ?>">SKPD</a>
                        </li>
                        <li>
                            <a href="#" class="sidebar-nav-submenu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i>Jalan</a>
                            <ul>
                                <li>
                                    <a href="<?php echo e(url('/form-data-jalan-kota')); ?>">Daerah/Kota</a>
                                </li>
                                <li>
                                    <a href="#">Desa</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo e(url('/role')); ?>">Jembatan</a>
                        </li>
                        <li>
                            <a href="<?php echo e(url('/role')); ?>">Pengairan</a>
                        </li>
                    </ul>
                </li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>
