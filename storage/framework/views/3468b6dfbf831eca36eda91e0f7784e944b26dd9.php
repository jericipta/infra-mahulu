<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
    <ul class="breadcrumb breadcrumb-top">
        <li> Users List</li>
        <li><a href="/users">Users</a></li>
    </ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-5">
            <div class="block full">
                <?php echo Form::open(array('action' => 'SkpdController@store', 'method' => 'POST', 'role' => 'form', 'class' => 'form-horizontal form-bordered')); ?>

                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-text-input">Nama SKPD</label>
                        <div class="col-md-9">
                            <input type="text" id="example-text-input" name="nama_skpd" class="form-control" placeholder="Nama SKPD">
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="example-textarea-input">Alamat</label>
                        <div class="col-md-9">
                            <textarea id="example-textarea-input" name="alamat_skpd" rows="9" class="form-control" placeholder="Masukan Alamat yang Valid"></textarea>
                        </div>
                    </div>
                    <div class="from-group">
                        <button class="btn btn-success">Save</button>
                    </div>
                <?php echo Form::close(); ?>

            </div>
        </div>
        <div class="col-md-7 pull-right">
            <div class="block full">
                <div class="table-responsive">
                <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center" style="font-size: 12px;">ID</th>
                        <th class="text-center" style="font-size: 12px;">Nama SKPD</th>
                        <th class="text-center" style="font-size: 12px;">Alamat</th>
                        <th class="text-center" style="font-size: 12px;">Opsi</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $skpd; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $skpds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="font-size: 12px;"><?php echo e($skpds->id); ?></td>
                            <td style="font-size: 12px;"><?php echo e($skpds->nama_skpd); ?></td>
                            <td style="font-size: 12px;"><?php echo e($skpds->alamat_skpd); ?></td>
                            <td class="text-center" style="font-size: 12px;">
                                <a href="#" data-toggle="modal" data-target="#myModal" class="btnEdit">
                                    <i class="fa fa-pencil"></i></a> | 
                                <a href="#" data-toggle="modal" data-target="#modal-delete" class="modal-delete">
                                    <i class="fa fa-trash"></i></a> 
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <?php echo Form::open(array('action' => 'SkpdController@update', 'method' => 'POST', 'role' => 'form')); ?>

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <label class="col-md-3 control-label" for="example-text-input">Nama SKPD</label>
                <input type="text" id="editRoleName" name="role_name" class="form-control" placeholder="Nama Role">
                <input type="hidden" id="idRoleedit" name="idRoleedit">
                <span class="help-block"></span>
                <label class="col-md-3 control-label" for="example-text-input">Alamat SKPD</label>
                <textarea id="edit-desc" name="descript" rows="9" class="form-control" placeholder=""></textarea>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" id="bnt-update">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
<div id="modal-delete" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirm Delete</h4>
      </div>
      <?php echo Form::open(array('action' => 'SkpdController@delete', 'method' => 'POST', 'role' => 'form')); ?>  
      <div class="modal-body">
      <input type="hidden" name="id" id="id-delete">
        <p>Delete this user?</p>
      </div>
      <div class="modal-footer">
      <button class="btn btn-success" id="bnt-update">Delete</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        </div>
      <?php echo Form::close(); ?>

    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
    $("body").on("click",".btnEdit",function(){
        var id = $(this).parent("td").prev("td").prev("td").prev("td").text();
        var nama = $(this).parent("td").prev("td").prev("td").text();
        var alamat = $(this).parent("td").prev("td").text();
        $('#editRoleName').val(nama);
        $('#edit-desc').val(alamat);
        $('#idRoleedit').val(id);
//            $('#edit-desc').val(desc);
    });
    $("body").on("click",".modal-delete",function(){
        var id = $(this).parent("td").prev("td").prev("td").prev("td").text();
        $('#id-delete').val(id);
    });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>