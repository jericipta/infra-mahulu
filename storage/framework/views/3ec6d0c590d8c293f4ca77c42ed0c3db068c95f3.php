<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li>Kirim Data</li>
    <li><a href="">Profile Infrastruktur</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="block full">
   <h1>Resize Image Uploading Demo</h1>
<?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>


<?php if($message = Session::get('success')): ?>
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
    <strong><?php echo e($message); ?></strong>
</div>
<div class="row">
    <div class="col-md-4">
        <strong>Original Image:</strong>
        <br/>
        <img src="/images/<?php echo e(Session::get('imageName')); ?>" />
    </div>
    <div class="col-md-4">
        <strong>Thumbnail Image:</strong>
        <br/>
        <img src="/thumbnail/<?php echo e(Session::get('imageName')); ?>" />
    </div>
</div>
<?php endif; ?>


<?php echo Form::open(array('route' => 'resizeImagePost','enctype' => 'multipart/form-data')); ?>

    <div class="row">
        <div class="col-md-4">
            <br/>
            <?php echo Form::text('title', null,array('class' => 'form-control','placeholder'=>'Add Title')); ?>

        </div>
        <div class="col-md-12">
            <br/>
            <?php echo Form::file('image', array('class' => 'image')); ?>

        </div>
        <div class="col-md-12">
            <br/>
            <button type="submit" class="btn btn-success">Upload Image</button>
        </div>
    </div>
<?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>