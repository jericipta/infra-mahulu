<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li> Users List</li>
    <li><a href="/users">Users</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="block full">
    <div class="block-title">
         <div class="block-options pull-right">
            <a href="<?php echo e(URL::to( asset('/users/deleted'))); ?>" class="btn btn-sm btn-danger" data-toggle="tooltip" title="" data-original-title="List User Deleted"><i class="fa fa-trash"></i></a>
        </div>
        <div class="block-options pull-right">
            <a href="<?php echo e(URL::to( asset('/users/create'))); ?>" class="btn btn-sm btn-success" data-toggle="tooltip" title="" data-original-title="Tambah Data User"><i class="fa fa-plus-square"></i></a>
        </div>
        <h2><strong>List User</strong></h2>
    </div>
    <div class="table-responsive">
            <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                <thead>
                    <tr>
                        <th style="font-size: 14px;">ID</th>
                        <th style="font-size: 14px;">Username</th>
                        <th style="font-size: 14px;">Email</th>
                        <th style="font-size: 14px;">First Name</th>
                        <th style="font-size: 14px;">Last Name</th>
                        <th style="font-size: 14px;">Role</th>
                        <th style="font-size: 14px;">Created</th>
                        <th style="font-size: 14px;">Updated</th>
                        <th style="font-size: 14px;">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="font-size: 12px;"><?php echo e($user->id); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->name); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->email); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->first_name); ?></td>
                            <td style="font-size: 12px;"><?php echo e($user->last_name); ?></td>
                            <td style="font-size: 12px;">
                                <?php $__currentLoopData = $user->roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user_role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($user_role->name == 'User'): ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-user';
                                            $levelName        = 'User';
                                            $levelBgClass     = 'mdl-color--blue-200';
                                            $leveIconlBgClass = 'mdl-color--blue-500';
                                         ?>
                                    <?php elseif($user_role->name == 'Admin'): ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-users';
                                            $levelName        = 'Admin';
                                            $levelBgClass     = 'mdl-color--red-200';
                                            $leveIconlBgClass = 'mdl-color--red-500';
                                         ?>
                                    <?php elseif($user_role->name == 'Unverified'): ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-user-secret';
                                            $levelName        = 'Unverified';
                                            $levelBgClass     = 'mdl-color--orange-200';
                                            $leveIconlBgClass = 'mdl-color--orange-500';
                                         ?>
                                    <?php else: ?>
                                        <?php 
                                            $levelIcon        = 'fa fa-user-secret';
                                            $levelName        = 'Unverified';
                                            $levelBgClass     = 'mdl-color--orange-200';
                                            $leveIconlBgClass = 'mdl-color--orange-500';
                                         ?>
                                    <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <span><i class="<?php echo e($levelIcon); ?>"></i> <?php echo e($levelName); ?></span>
                            </td>
                            <td class="mdl-data-table__cell--non-numeric mdl-layout--large-screen-only"><?php echo e($user->created_at); ?></td>
                            <td class="mdl-data-table__cell--non-numeric mdl-layout--large-screen-only"><?php echo e($user->updated_at); ?></td>
                            <td class="mdl-data-table__cell--non-numeric">
                                <div class="btn-group btn-group-xs">
                                    <a href="<?php echo e(URL::to('users/' . $user->id . '/edit')); ?>" class="btn btn-primary" data-toggle="tooltip" title="Edit Selected">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    <a href="/profile/<?php echo e($user->name); ?>" class="btn btn-primary" data-toggle="tooltip" title="View Selected"><i class="fa fa-search"></i></a>
                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" class="modal-delete" data-id="<?php echo e($user->id); ?>"><i class="fa fa-trash"></i></a> 
                            </td>
                        </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                  </tbody>
            </table>
    </div>
</div>

<div id="modal-delete" class="modal fade" role="dialog">
    <?php echo Form::open(array('action' => 'UsersManagementController@delete', 'method' => 'POST', 'role' => 'form')); ?>

    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Delete Confirm</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id2" name="id">
                <span class="help-block"></span>
            </div>
            <div class="modal-footer">
                <?php echo Form::button('<i class="fa fa-fw fa-close" aria-hidden="true"></i> Cancel', array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )); ?>

                <?php echo Form::button('<i class="fa fa-fw fa-trash-o" aria-hidden="true"></i> Confirm Delete', array('class' => 'btn btn-danger pull-right btn-flat', 'type' => 'submit', 'id' => 'confirm' )); ?>

            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
    <script>
        $("body").on("click",".modal-delete",function(){
            $('#id2').val($(this).attr('data-id'));
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>