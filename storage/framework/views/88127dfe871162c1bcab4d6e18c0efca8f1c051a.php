<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li>Users List</li>
    <li><a href="">Create New User</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
  <div class="block full">

    <div class="row">
       <?php echo Form::open(array('action' => 'UsersManagementController@store', 'method' => 'POST', 'role' => 'form',
          'class' => 'form-horizontal ui-formwizard')); ?>

       <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
               <?php echo e(Form::label('unit', 'UNIT', array('class' => 'col-md-4 control-label'))); ?>

              <div class="col-md-8">
                <?php echo e(Form::text('unit', Input::old('unit'),array('class' => 'form-control'))); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
               <?php echo Form::label('nip', 'NIP', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
                <?php echo Form::text('nip', NULL, array('id' => 'nip', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('name', trans('auth.name') , array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
                <?php echo Form::text('name', NULL, array('id' => 'name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z,0-9]*')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('phone', 'Phone', array('class' => 'col-md-4 control-label')); ?>

              <div class="col-md-8">
                <?php echo Form::text('phone', NULL, array('id' => 'phone', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('email', trans('auth.email') , array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
                <?php echo Form::email('email', NULL, array('id' => 'email', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('first_name', 'First Name', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
                <?php echo Form::text('first_name', NULL, array('id' => 'first_name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z]*')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('last_name', 'Last Name', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
                <?php echo Form::text('last_name', NULL, array('id' => 'last_name', 'class' => 'form-control', 'pattern' => '[A-Z,a-z]*')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('tanggal_lahir', 'Tanggal Lahir', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
                <?php echo Form::text('tanggal_lahir', NULL, array('id' => 'datepicker', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('id_skpd', 'SKPD Name', array('class' => 'col-md-4 control-label')); ?>

              <div class="col-md-8">
                <select class="select-chosen" data-placeholder="Select SKPD" name="skpd" id="skpd">
                    <option value="" disabled="true"></option>
                    <?php if($skpd->count()): ?>
                      <?php $__currentLoopData = $skpd; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $skpds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <option value="<?php echo e($skpds->id); ?>"><?php echo e($skpds->nama_skpd); ?></option>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </select>
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('bagian', 'Bagian', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
              <?php echo Form::text('bagian', NULL, array('id' => 'bagian', 'class' => 'form-control' )); ?> 
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('role', trans('forms.label-userrole_id'), array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
                <select class="select-chosen" data-placeholder="Select SKPD" name="role" id="role">
                    <option value="" disabled="true"></option>
                    <?php if($roles->count()): ?>
                          <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($role->id); ?>"><?php echo e($role->name); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                </select>
              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('password', 'Password', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
              <?php echo Form::password('password', array('id' => 'password', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('password_confirmation', 'Confirm Password', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
              <?php echo Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('jabatan', 'Jabatan', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
              <?php echo Form::text('jabatan', NULL, array('id' => 'jabatan', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
          <div class="form-group">
              <?php echo Form::label('bio', 'Bio', array('class' => 'col-md-4 control-label'));; ?>

              <div class="col-md-8">
              <?php echo Form::textarea('bio',  NULL, array('id' => 'bio', 'class' => 'form-control')); ?>

              </div>
          </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-8 col-md-offset-4  pull-left">
                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Submit</button>
                <a href="<?php echo e(url('/users/')); ?>" class="btn btn-sm btn-warning"><i class="fa fa-reply"></i> Back</a>
            </div>
        </div>
       <?php echo Form::close(); ?>

    </div>
  </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>

  <?php echo $__env->make('scripts.mdl-required-input-fix', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('scripts.gmaps-address-lookup-api3', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <script>
      $(function() {
          $( "#datepicker" ).datepicker();
      });
  </script>
  <script type="text/javascript">
    mdl_dialog('.dialog-button-save');
    mdl_dialog('.dialog-button-icon-save');
  </script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>