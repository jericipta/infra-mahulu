<div id="map-canvas-<?php echo $id; ?>" style="height: 100%; margin: 0; padding: 0;"></div>

<script type="text/javascript">

	function initialize_<?php echo $id; ?>() {
		var bounds = new google.maps.LatLngBounds();
		var position = new google.maps.LatLng(<?php echo $options['latitude']; ?>, <?php echo $options['longitude']; ?>);

		var mapOptions = {
			<?php if($options['center']): ?>
				center: position,
			<?php endif; ?>
			zoom: <?php echo $options['zoom']; ?>,
			mapTypeId: google.maps.MapTypeId.<?php echo $options['type']; ?>,
			disableDefaultUI: <?php if(!$options['ui']): ?> true <?php else: ?> false <?php endif; ?>
		};

		var map = new google.maps.Map(document.getElementById('map-canvas-<?php echo $id; ?>'), mapOptions);

		var panoramaOptions = {
			position: position,
			pov: {
				heading: <?php echo $options['heading']; ?>,
				pitch: <?php echo $options['pitch']; ?>

			}
		};

		var panorama = new google.maps.StreetViewPanorama(document.getElementById('map-canvas-<?php echo $id; ?>'), panoramaOptions);

		map.setStreetView(panorama);
	}

    <?php if(!$options['async']): ?>

        google.maps.event.addDomListener(window, 'load', initialize_<?php echo $id; ?>);

    <?php endif; ?>

</script>