<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<div class="content-header">
    <div class="header-section" style="padding: 20px 10px;">
        <h1 class="text-center">
            DATA INFRASTRUKTUR (SARANA DAN PRASARANA) KAMPUNG
        </h1>
        <h1 class="text-center">
            DI KABUPATEN MAHAKAM HULU
        </h1>
    </div>
</div>
<ul class="breadcrumb breadcrumb-top">
    <li> Data Infrastruktur</li>
    <li><a href="/users">Air Bersih</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="block full">
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
          <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Lokasi</th>
                <th class="text-center" style="font-size: 12px;">Bidang Urusan</th>
                <th class="text-center" style="font-size: 12px;">Jumlah Penduduk / Jumlah KK</th>
                <th class="text-center" style="font-size: 12px;">SAB</th>
                <th class="text-center" style="font-size: 12px;">KapasitasLT/DK</th>
                <th class="text-center" style="font-size: 12px;">Sungai</th>
                <th class="text-center" style="font-size: 12px;">Keterangan</th>
            </tr>
          </thead>
          <tbody>

                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $v): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>

                    <td class="text-left" style="font-size: 12px;"><?php echo e($v[0]); ?></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($v[1]); ?></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($v[2]); ?></td>
                    <td class="text-center" style="font-size: 12px;"><i class="<?php echo e($v[3] === true ? "fa fa-check" : ""); ?>"></i></td>
                    <td class="text-center" style="font-size: 12px;"><i class="<?php echo e($v[4] === true ? "fa fa-check" : ""); ?>"></i></td>
                    <td class="text-center" style="font-size: 12px;"><i class="<?php echo e($v[5] === true  ? "fa fa-check" : ""); ?>"></i></td>
                    <td class="text-center" style="font-size: 12px;"><?php echo e($v[6]); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          </tbody>
        </table>
    </div>
</div>
    
<?php $__env->stopSection(); ?>
<?php $__env->startSection('footer_scripts'); ?>
    <script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
    <script>$(function(){ TablesDatatables.init(); });</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>