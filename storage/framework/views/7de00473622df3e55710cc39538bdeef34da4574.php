<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Insert</li>
    <li><a href="">Jalan Daerah / Kota</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="block full">
     <?php echo Form::open(array('action' => 'FormDataSKPDController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-bordered')); ?>

        <div class="form-group">
            <?php echo Form::Label('id-skpd', 'Nama SKPD'); ?>

            <?php echo Form::select('id-skpd', $skpd, null, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Skpd']); ?>

        </div>
        <div class="form-group">
            <?php echo Form::Label('nama-skpd', 'Nama Form SKPD'); ?>

            <input type="text" id="nama-skpd" name="nama-skpd" class="form-control" placeholder="Nama Form SKPD">
        </div>
        <div class="form-group">
            <?php echo Form::label('File Form'); ?>

            <?php echo Form::file('file-form', null); ?>

        </div>
        <div class="form-group">
            <?php echo Form::Label('table-name', 'Table Form'); ?>

            <input type="text" id="table-name" name="table-name" class="form-control" placeholder="Table Form">
        </div>
        <div class="form-group">
            <?php echo Form::label('Keterangan'); ?>

            <?php echo Form::textarea('keterangan', '', ['class' => 'form-control']); ?>

        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="<?php echo e(url('/form-data-skpd')); ?>" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    <?php echo Form::close(); ?>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>