<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header-breadcrumbs'); ?>
    <ul class="breadcrumb breadcrumb-top">
        <li> Data Jalan</li>
        <li><a href="/users">Jalan PU</a></li>
    </ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
            <div class="block full">
                <div class="table-responsive">
                <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
                    <thead>
                    <tr>
                        <th class="text-center" style="font-size: 12px;">ID</th>
                        <th class="text-center" style="font-size: 12px;">Dinas</th>
                        <th class="text-center" style="font-size: 12px;">Tanggal Proses</th>
                        <th class="text-center" style="font-size: 12px;">Status</th>
                        <th class="text-center" style="font-size: 12px;">Lokasi File</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data_pu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td style="font-size: 12px;"><?php echo e($data_pu->id); ?></td>
                            <td style="font-size: 12px;"><?php echo e($data_pu->dinas); ?></td>
                            <td style="font-size: 12px;"><?php echo e($data_pu->status); ?></td>
                            <td style="font-size: 12px;"><?php echo e($data_pu->lokasi_file); ?></td>
                            <td class="text-center" style="font-size: 12px;">
                                <a href="#" data-toggle="modal" data-target="#myModal" class="btnEdit">
                                    Lihat</a> 
                            </td>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>
<script src="<?php echo e(asset('ProUI/js/pages/tablesDatatables.js')); ?>"></script>
<script>$(function(){ TablesDatatables.init(); });</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>