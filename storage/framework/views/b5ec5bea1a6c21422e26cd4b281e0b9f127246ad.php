<?php $__env->startSection('template_title'); ?>
    Welcome <?php echo e(Auth::user()->name); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
    <?php echo e(trans('auth.loggedIn', ['name' => Auth::user()->name])); ?>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('header-breadcrumbs'); ?>
<ul class="breadcrumb breadcrumb-top">
    <li>Form Data</li>
    <li>Edit</li>
    <li><a href="">Infrastruktur</a></li>
</ul>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<style type="text/css">
    input[type=file]{
      display: inline;
    }
    #image_preview{
      border: 1px solid black;
      padding: 10px;
    }
    #image_preview img{
      width: 200px;
      padding: 5px;
    }
  </style>
<div class="block full">
     <?php echo Form::open(array('action' => 'ProfileInfraController@store', 'method' => 'POST', 'files' => true, 'role' => 'form', 'class' => 'form-horizontal  form-bordered')); ?>

        <div class="form-group col-sm-12">
            <?php echo Form::Label('id-kategori', 'Infrastruktur', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <?php echo Form::select('id-kategori', $kategori, $profile->id_kategori, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Infrastruktur']); ?>

            </div>
            <?php echo Form::Label('id-jenis', 'Jenis', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <?php echo Form::select('id-jenis', $jenis, $profile->id_jenis, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Jenis']); ?>

            </div>
        </div>
        <div class="form-group col-sm-12">
            <?php echo Form::Label('nama', 'Nama', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <input type="text" id="nama" name="nama" value="<?php echo e($profile->nama); ?>" class="form-control" placeholder="Nama">
            </div>
            <?php echo Form::Label('alamat', 'Alamat', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <input type="text" id="alamat" name="alamat" value="<?php echo e($profile->alamat); ?>" class="form-control" placeholder="Alamat">
            </div>
        </div>
         <div class="form-group col-sm-12">
            <?php echo Form::Label('id-kecamatan', 'Kecamatan', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <?php echo Form::select('id-kecamatan', $kecamatan, $profile->id_kecamatan, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Kecamatan']); ?>

            </div>
            <?php echo Form::Label('id-desa', 'Desa', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <?php echo Form::select('id-desa', $desa, $profile->id_desa, ['class' => 'select-chosen', 'data-placeholder'=>'Pilih Nama Kecamatan']); ?>

            </div>
        </div>
        <div class="form-group col-sm-12">
            <?php echo Form::Label('id-tahun', 'Tahun', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <?php echo Form::select('tahun', $tahun, $profile->tahun, ['class' => 'select-chosen']); ?>

            </div>
            <?php echo Form::Label('nilai', 'Nilai Anggaran', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <input type="text" id="nilai" name="nilai" value="<?php echo e($profile->nilai); ?>" class="form-control" placeholder="Nilai Anggaran">
            </div>
        </div>
        <div class="form-group col-sm-12">
            <?php echo Form::Label('catatan', 'Catatan', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <input type="text" id="catatan" name="catatan" value="<?php echo e($profile->catatan); ?>" class="form-control" placeholder="Catatan Kondisi">
            </div>
            <?php echo Form::Label('tgl-proses', 'Tanggal Proses', ['class' => 'col-md-2 control-label']); ?>

            <div class="col-md-4">
                <input type="text" id="tgl-proses" name="tgl-proses" value="<?php echo e($profile->tgl_proses); ?>" class="form-control input-datepicker" data-date-format="dd-mm-yy" placeholder="dd-MM-yy">
            </div>
        </div>
        <div class="form-group col-sm-12">
            <?php echo Form::Label('uploadFile', 'Foto', ['class' => 'col-md-3 control-label']); ?>

            <div class="row push col-sm-9">
                <div class="col-sm-4 col-md-4">
                    <a href="/images/<?php echo e($profile->image_1); ?>" data-toggle="lightbox-image">
                        <img src="/thumbnail/<?php echo e($profile->image_1); ?>" alt="image">
                    </a>
                </div>
                <div class="col-sm-4 col-md-4">
                    <a href="/images/<?php echo e($profile->image_2); ?>" data-toggle="lightbox-image">
                        <img src="/thumbnail/<?php echo e($profile->image_2); ?>" alt="image">
                    </a>
                </div>
                <div class="col-sm-4 col-md-4">
                    <a href="/images/<?php echo e($profile->image_3); ?>" data-toggle="lightbox-image">
                        <img src="/thumbnail/<?php echo e($profile->image_3); ?>" alt="image">
                    </a>
                </div>
            </div>
        </div>
        <div class="form-group form-actions">
            <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-check"></i> Save</button>
            <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-repeat"></i> Reset</button>
            <a href="<?php echo e(url('/infra-form-infrastruktur')); ?>" type="reset" class="btn btn-sm btn-danger"><i class="fa fa-reply"></i> Cancel</a>
        </div>
    <?php echo Form::close(); ?>

</div>

<div class="block full">
    <div class="block-title">
        <div class="block-options pull-right">
            <a href="#" class="btn btn-sm btn-success" id="add" data-toggle="modal" data-target="#myModal" title="" data-original-title="Tambah Data"><i class="fa fa-plus-square"></i></a>
        </div>
        <h2><strong>Form Data - Detail Profile</strong></h2>
    </div>
    <div class="table-responsive">
        <table border="1" id="example-datatable" class="table table-vcenter table-condensed table-bordered">
            <thead>
            <tr>
                <th class="text-center" style="font-size: 12px;">Tanggal</th>
                <th class="text-center" style="font-size: 12px;">Kegiatan</th>
                <th style="font-size: 12px;">Hasil</th>
                <th style="font-size: 12px;">Foto</th>
            </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $pDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <tr data-id="<?php echo e($value->id); ?>">
                        <td><?php echo e($value->tanggal); ?></td>
                        <td><?php echo e($value->kegiatan); ?></td>
                        <td><?php echo e($value->hasil); ?></td>
                        <td>
                            <a href="/images/profile-infra/<?php echo e($value->image_1); ?>" data-toggle="lightbox-image">
                                <img src="/thumbnail/profile-infra/<?php echo e($value->image_1); ?>" alt="image">
                            </a>
                            <a href="/images/profile-infra/<?php echo e($value->image_2); ?>" data-toggle="lightbox-image">
                                <img src="/thumbnail/profile-infra/<?php echo e($value->image_2); ?>" alt="image">
                            </a>
                            <a href="/images/profile-infra/<?php echo e($value->image_3); ?>" data-toggle="lightbox-image">
                                <img src="/thumbnail/profile-infra/<?php echo e($value->image_3); ?>" alt="image">
                            </a>
                        </td>
                    </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <?php echo Form::open(array('action' => 'ProfileInfraDetailController@store', 'method' => 'POST', 'files' => true, 'role' => 'form')); ?>

    <input type="hidden" name="id-parent" value="<?php echo e($profile->id); ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Profile Infrastruktur Detail</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <?php echo Form::Label('tanggal', 'Tanggal', ['class' => 'control-label']); ?>

                    <input type="text" id="tgl-proses" name="tgl-proses" class="form-control input-datepicker" data-date-format="dd-mm-yy" placeholder="dd-MM-yy">
                </div>
                <div class="form-group">
                    <?php echo Form::Label('kegiatan', 'Kegiatan', ['class' => 'control-label']); ?>

                    <input type="text" id="kegiatan" name="kegiatan" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group">
                    <?php echo Form::Label('hasil', 'Hasil', ['class' => 'control-label']); ?>

                    <input type="text" id="hasil" name="hasil" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_1" name="image_1" multiple/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_1_name" name="image_1_name" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_2" name="image_2" multiple/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_2_name" name="image_2_name" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_3" name="image_3" multiple/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_3_name" name="image_3_name" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group col-sm-6">
                    <input type="file" id="image_4" name="image_4"/>
                </div>
                 <div class="form-group col-sm-6">
                    <input type="text" id="image_4_name" name="image_4_name" class="form-control" placeholder="Nama">
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" id="bnt-update">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer_scripts'); ?>

<script>
    $('#id-kategori').change(function(){
        $.get("<?php echo e(url('infra-master-jenis/jenis')); ?>", 
            { option: $(this).val() }, 
            function(data) {
                var model = $('#id-jenis');
                model.empty();

                $.each(data, function(index, element) {
                    model.append("<option value='"+ index +"'>" + element + "</option>");
                });
            });
    });

    $('#id-kecamatan').change(function(){
        $.get("<?php echo e(url('infra-master-desa/desa')); ?>", 
            { option: $(this).val() }, 
            function(data) {
                var model = $('#id-desa');
                model.empty();

                $.each(data, function(index, element) {
                    model.append("<option value='"+ index +"'>" + element + "</option>");
                });
            });
    });

    function storeData() {
        $.ajax({
            url: "/infra-form-infrastruktur-d",
            type: "POST",
            data: $('#frmData').serialize(),
            beforeSend: function () {
                $(".loader-container").show();
            },
            success: function (data) {
                console.log(data);
                if (data.message == 'Success') {
                    $(".loader-container").hide();
                    swal('Berhasil!', 'Simpan Data Berhasil', 'success');
                    var id = 'headTabCppt';
                } else {
                    $(".loader-container").hide();
                    swal('Gagal!', 'Simpan Data Gagal', 'warning');
                }
            }
        });
    }
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('infra.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>