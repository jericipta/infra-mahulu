<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
| Middleware options can be located in `app/Http/Kernel.php`
|
 */

// Homepage Route
//Route::get('/', 'WelcomeController@welcome')->name('welcome');

// Authentication Routes
Auth::routes();

// Public Resource Route
Route::get('material.min.css.template', 'ThemesManagementController@template');

// Public Routes
Route::group(['middleware' => 'web'], function ()
{
	// Activation Routes
	Route::get('/activate', ['as' => 'activate', 'uses' => 'Auth\ActivateController@initial']);

	Route::get('/activate/{token}', ['as' => 'authenticated.activate', 'uses' => 'Auth\ActivateController@activate']);
	Route::get('/activation', ['as'       => 'authenticated.activation-resend', 'uses'       => 'Auth\ActivateController@resend']);
	Route::get('/exceeded', ['as'         => 'exceeded', 'uses'         => 'Auth\ActivateController@exceeded']);

	// Socialite Register Routes
	Route::get('/social/redirect/{provider}', ['as' => 'social.redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
	Route::get('/social/handle/{provider}', ['as'   => 'social.handle', 'uses'   => 'Auth\SocialController@getSocialHandle']);

	// Route to for user to reactivate their user deleted account.
	Route::get('/re-activate/{token}', ['as' => 'user.reactivate', 'uses' => 'RestoreUserController@userReActivate']);
});

// Registered and Activated User Routes
Route::group(['middleware' => ['auth', 'activated']], function ()
{
	// Homepage Route
	Route::get('/', ['as' => 'public.home', 'uses' => 'HomeController@home']);
	Route::get('images-upload', 'HomeController@imagesUpload');
	Route::post('images-upload', 'HomeController@imagesUploadPost')->name('images.upload');
	
	// Activation Routes
	Route::get('/activation-required', ['uses' => 'Auth\ActivateController@activationRequired'])->name('activation-required');
	Route::get('/logout', ['uses'              => 'Auth\LoginController@logout'])->name('logout');

	//  Homepage Route - Redirect based on user role is in controller.
	// Route::get('/home', ['as' => 'public.home', 'uses' => 'UserController@index']);
	Route::get('/home', ['as' => 'public.home', 'uses' => 'HomeController@home']);
	Route::get('/home/map/{id}', ['as' => 'public.home-map', 'uses' => 'HomeController@homeMap']);
	Route::get('/home/detail/{id}', ['as' => 'public.home-map', 'uses' => 'HomeController@detail']);
	// Route::get('/home', ['as' => 'public.home', 'uses' => 'HomeController@home']);
	// Show users profile - viewable by other users.
	Route::get('profile/{username}', [
		'as'   => '{username}',
		'uses' => 'ProfilesController@show'
	]);

	// Route to show user avatar
	Route::get('images/profile/{id}/avatar/{image}', [
		'uses' => 'ProfilesController@userProfileAvatar'
	]);

	// Route for user profile background image
	Route::get('images/profile/{id}/background/{image}', [
		'uses' 		=> 'ProfilesController@userProfileBackgroundImage'
	]);
});

// Registered, activated, and is current user routes.
Route::group(['middleware' => ['auth', 'activated', 'currentUser']], function ()
{
	// User Profile and Account Routes
	Route::resource(
		'profile',
		'ProfilesController', [
			'only' => [
				'account',
				'show',
				'edit',
				'update',
				'create'
			]
		]
	);
	Route::put('profile/{username}/updateUserAccount', [
		'as'   => '{username}',
		'uses' => 'ProfilesController@updateUserAccount'
	]);
	Route::put('profile/{username}/updateUserPassword', [
		'as'   => '{username}',
		'uses' => 'ProfilesController@updateUserPassword'
	]);
	Route::delete('profile/{username}/deleteUserAccount', [
		'as'   => '{username}',
		'uses' => 'ProfilesController@deleteUserAccount'
	]);

	// Route for user profile background image
	Route::get('account', [
		'as'   	=> '{username}',
		'uses' 	=> 'ProfilesController@account'
	]);

	// Update User Profile Ajax Route
	Route::post('profile/{username}/updateAjax', [
		'as'   => '{username}',
		'uses' => 'ProfilesController@update'
	]);

	// Route to upload user avatar.
	Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'ProfilesController@upload']);

	// Route to uplaod user background image
	Route::post('background/upload', ['as' => 'background.upload', 'uses' => 'ProfilesController@uploadBackground']);

	// User Tasks Routes
	Route::resource('/tasks', 'TasksController');
});
Route::get('/role',['as'=>'role','uses'=>'RoleController@index']);
Route::post('/new-role',['as'=>'new-role','uses'=>'RoleController@store']);
Route::post('/update/role',['as'=>'update-role','uses'=>'RoleController@update']);
Route::post('/delete/role',['as'=>'delete-role','uses'=>'RoleController@delete']);
Route::get('/get-desa', 'InfraListrikController@desa');

Route::post('/update/skpd',['as'=>'update-skpd','uses'=>'SkpdController@update']);
Route::get('/statistik',['as'=>'statistik','uses'=>'ChartController@index']);
Route::get('/maps',['as'=>'maps','uses'=>'ChartController@maps']);
Route::get('/skpd', ['as'=>'skpd','uses'=>'SkpdController@index']);
Route::post('/new-skpd',['as'=>'new-skpd','uses'=>'SkpdController@store']);
Route::post('/delete/skpd',['as'=>'delete-skpd','uses'=>'SkpdController@delete']);

Route::get('/get-desa-air', 'InfraAirBersihController@desa');

Route::resource('akses', 'AksesController');
Route::post('/update/akses',['as'=>'update-akses','uses'=>'AksesController@update']);
Route::post('/delete/akses',['as'=>'delete-akses','uses'=>'AksesController@delete']);
Route::post('/savedb','HomeController@import');

Route::resource('/form-data-jalan-kota', 'FormDataJDaerahKotaController');
Route::post('/delete/form-data-jalan-kota',['as'=>'delete-form-data-jalan-kota','uses'=>'FormDataJDaerahKotaController@delete']);
Route::resource('/form-data-skpd', 'FormDataSKPDController');
Route::post('/delete/form-data-skpd',['as'=>'delete-form-data-skpd','uses'=>'FormDataSKPDController@delete']);

Route::get('/form-master-dinas',['as'=>'dinas','uses'=>'MasterDinasController@index']);
Route::post('/form-master-dinas',['as'=>'create-dinas','uses'=>'MasterDinasController@store']);
Route::post('/delete/form-master-dinas',['as'=>'delete-dinas','uses'=>'MasterDinasController@delete']);
Route::post('/update/form-master-dinas',['as'=>'update-dinas','uses'=>'MasterDinasController@update']);
// Registered, activated, and is admin routes.


Route::resource('/infra-kirim-fasum', 'KirimFasumController');
Route::get('/infra-kirim-jalan', ['as' => 'public.infra-kirim-jalan', 'uses' => 'HomeController@home']);
Route::resource('/infra-kirim-jembatan', 'KirimJembatanController');
Route::resource('/infra-kirim-pengairan', 'KirimPengairanController');

Route::get('infra-master-jenis/jenis', ['as' => 'infra-master-jenis.jenis', 'uses' => 'TMInfraJenisController@jenis']);
Route::resource('infra-master-jenis', 'TMInfraJenisController');

Route::get('infra-master-desa/desa', ['as' => 'infra-master-desa.desa', 'uses' => 'TMDesaController@desa']);
Route::resource('infra-master-desa', 'TMDesaController');

Route::get('infra-master-kategori/combo', ['as' => 'infra-master-kategori.combo', 'uses' => 'TMInfraKategoriController@combo']);
Route::resource('infra-master-kategori', 'TMInfraKategoriController');


Route::get('/infra-form-infrastruktur/map/{id}', ['as' => 'public.infra-form-infrastruktur', 'uses' => 'ProfileInfraController@profileMap']);
Route::post('/delete/infra-form-infrastruktur',['as'=>'delete-form-infrastruktur','uses'=>'ProfileInfraController@delete']);
Route::resource('/infra-form-infrastruktur', 'ProfileInfraController');
Route::resource('/infra-form-infrastruktur-d', 'ProfileInfraDetailController');

Route::get('api/dropdown', function(){
	$input = Input::get('option');
	$kategori = TMInfraKategori::find($input);
	$jenis = $kategori->jenis();
	return Response::eloquent($jenis->get(['id','nama']));
});

Route::get('/get-desa-air', 'InfraAirBersihController@desa');

Route::group(['middleware' => ['auth', 'activated', 'role:admin']], function ()
{
	Route::resource('/users/deleted', 'SoftDeletesController', [
		'only' => [
			'index', 'show', 'update', 'destroy',
		]
	]);
	Route::resource('users', 'UsersManagementController', [
		'names'    => [
			'index'   => 'users',
			'create'  => 'create',
			'store' => 'save',
			'destroy' => 'user.destroy'
		],
	]);
	Route::post('/delete/users',['as'=>'delete-users','uses'=>'UsersManagementController@delete']);
});

Route::post('/infra-listrik/post', 'InfraListrikController@post');

Route::group(['middleware' => ['auth', 'activated']], function ()
{
	Route::resource('themes', 'ThemesManagementController', [
		'names'    => [
			'index'   => 'themes',
			'destroy' => 'themes.destroy'
		]
	]);
	Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
	Route::get('php', 'AdminDetailsController@listPHPInfo');
	Route::get('routes', 'AdminDetailsController@listRoutes');

	
	Route::resource('infra-listrik', 'InfraListrikController', [
		'names'    => [
			'index'   => 'infra-listrik',
			'create'  => 'create',
			'destroy' => 'listrik.destroy'
		],
		'except' => [
			'deleted'
		]
	]);

	Route::post('/infra-air-bersih/post', 'InfraAirBersihController@post');
	Route::resource('infra-air-bersih', 'InfraAirBersihController', [
		'names'    => [
			'index'   => 'infra-air-bersih',
			'create'  => 'create',
			'destroy' => 'listrik.destroy'
		],
		'except' => [
			'deleted'
		]
	]);

	Route::get('/get-desa-telekom', 'InfraTelekomunikasiController@desa');
	Route::post('infra-telekomunikasi/post', 'InfraTelekomunikasiController@post');
	Route::resource('infra-telekomunikasi', 'InfraTelekomunikasiController', [
		'names'    => [
			'index'   => 'infra-telekomunikasi',
			'create'  => 'create',
			'destroy' => 'listrik.destroy'
		],
		'except' => [
			'deleted'
		]
	]);

	Route::resource('infra-jalan', 'InfraJalanController', [
		'names'    => [
			'index'   => 'infra-jalan',
			'create'  => 'create',
			'destroy' => 'listrik.destroy'
		],
		'except' => [
			'deleted'
		]
	]);

	Route::resource('infra-fasum-fasos', 'InfraFasumFasosController', [
		'names'    => [
			'index'   => 'infra-fasum-fasos',
			'create'  => 'create',
			'destroy' => 'listrik.destroy'
		],
		'except' => [
			'deleted'
		]
	]);

	Route::resource('map', 'MapController');
});

Route::get('resizeImage', 'ImageController@resizeImage');
Route::post('resizeImagePost',['as'=>'resizeImagePost','uses'=>'ImageController@resizeImagePost']);





